package teammates.unittest;

import org.junit.After;
import org.junit.Before;

import teammates.pojo.Account;
import teammates.pojo.Coordinator;
import teammates.pojo.Course;
import teammates.pojo.CourseStudent;
import teammates.pojo.CourseTeam;
import teammates.pojo.Evaluation;
import teammates.pojo.Student;
import teammates.pojo.Submit;
import teammates.pojo.SubmitEntry;

import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.googlecode.objectify.ObjectifyService;

/**
 * Code extracted from
 * 
 * https://groups.google.com/forum/#!topic/google-appengine-java/E3RC4efxxvM
 * 
 * 
 */
public class BaseTest {

	static {
		ObjectifyService.register(Account.class);
		ObjectifyService.register(Coordinator.class);
		ObjectifyService.register(Student.class);
		ObjectifyService.register(CourseStudent.class);
		ObjectifyService.register(Course.class);
		ObjectifyService.register(Evaluation.class);
		ObjectifyService.register(CourseTeam.class);
		ObjectifyService.register(Submit.class);
		ObjectifyService.register(SubmitEntry.class);
	}


	private final LocalServiceTestHelper helper = new LocalServiceTestHelper(
		new LocalDatastoreServiceTestConfig());

	@Before
	public void setUp() {
		helper.setUp();
	}

	/*
	 * Properties newProperties = new Properties();
	 * newProperties.put("javax.jdo.PersistenceManagerFactoryClass",
	 * "org.datanucleus.store.appengine.jdo." +
	 * "DatastoreJDOPersistenceManagerFactory");
	 * newProperties.put("javax.jdo.option.ConnectionURL", "appengine");
	 * newProperties.put("javax.jdo.option.NontransactionalRead", "true");
	 * newProperties.put("javax.jdo.option.NontransactionalWrite", "true");
	 * newProperties.put("javax.jdo.option.RetainValues", "true");
	 * newProperties.put("datanucleus.appengine.autoCreateDatastoreTxns", "true");
	 * newProperties.put("datanucleus.appengine.autoCreateDatastoreTxns", "true");
	 * pmf = JDOHelper.getPersistenceManagerFactory(newProperties); pm =
	 * pmf.getPersistenceManager();
	 * 
	 * ApiProxyLocalFactory factory = new ApiProxyLocalFactory(); ApiProxyLocal
	 * proxy = factory.create(new LocalServerEnvironment() { public void
	 * waitForServerToStart() throws InterruptedException { }
	 * 
	 * public int getPort() { return 0; }
	 * 
	 * public File getAppDir() { return new File("."); }
	 * 
	 * public String getAddress() { return null; }
	 * 
	 * @Override public boolean enforceApiDeadlines() { return false; }
	 * 
	 * @Override public String getHostName() { return null; }
	 * 
	 * @Override public boolean simulateProductionLatencies() { return false; }
	 * });
	 * 
	 * proxy.setProperty( // LocalDatastoreService.BACKING_STORE_PROPERTY,
	 * LocalDatastoreService.NO_STORAGE_PROPERTY, Boolean.TRUE.toString());
	 * ApiProxy.setDelegate(proxy);
	 * 
	 * }
	 */

	@After
	public void tearDown() {
		helper.tearDown();
	}

}