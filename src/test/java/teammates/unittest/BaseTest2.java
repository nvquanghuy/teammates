package teammates.unittest;

import static org.junit.Assert.assertArrayEquals;

import javax.persistence.Id;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.ObjectifyService;

class FakeEntity {
  @Id public Long id;
  public boolean boolProp;
  public boolean equals(Object other) {
    return other != null &&
           other instanceof FakeEntity &&
           ((FakeEntity)other).id == this.id &&
           ((FakeEntity)other).boolProp == this.boolProp; 
  }
}


public class BaseTest2 {
  private final LocalServiceTestHelper helper =
    new LocalServiceTestHelper(new LocalDatastoreServiceTestConfig());

  @Before
  public void setUp() {
    helper.setUp();
  }
  @After
  public void tearDown() {
    helper.tearDown();
  }
  
  @Test
  public void testBoolQuery() {
    ObjectifyFactory objectifyFactory = ObjectifyService.factory();
    objectifyFactory.register(FakeEntity.class);
    Objectify objectify = objectifyFactory.begin();
    FakeEntity entityFalse = new FakeEntity();
    FakeEntity entityTrue = new FakeEntity();
    entityTrue.boolProp = true;
    objectifyFactory.begin().put(entityFalse);
    objectifyFactory.begin().put(entityTrue);

    assertArrayEquals(
        new FakeEntity[] {entityFalse},
        objectify.query(FakeEntity.class)
        .filter("boolProp", false).list().toArray());
    assertArrayEquals(
        new FakeEntity[] {entityTrue},
        objectify.query(FakeEntity.class)
        .filter("boolProp", true).list().toArray());
    assertArrayEquals(
        new FakeEntity[] {entityTrue},
        objectify.query(FakeEntity.class)
        .filter("boolProp", true).list().toArray());
    assertArrayEquals(
        new FakeEntity[] {entityTrue},
        objectify.query(FakeEntity.class)
        .filter("boolProp", Boolean.TRUE).list().toArray());
    // Filtering on integers and strings WON'T work:
    assertArrayEquals(
        new FakeEntity[] {},
        objectify.query(FakeEntity.class)
        .filter("boolProp", "true").list().toArray());
    assertArrayEquals(
        new FakeEntity[] {},
        objectify.query(FakeEntity.class)
        .filter("boolProp", 0).list().toArray());
  }

  
}
