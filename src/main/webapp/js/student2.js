$(function() {
	
//	$("#form_join_course").validate();

	$("#form_join_course").submit( function() {
		//if (!$(this).valid()) return;
		loadingStart(this);
		var that = this;
		$.post($(this).attr('action'), $(this).serialize(), function(data) {
			loadingEnd(that);
			if (data) {
				flash(data);
				if (data.status == SUCCESS) {
					loadCourseList();
				}
			}
		} );
		return false;
	});

});


function loadCourseList() {
	$("#stu_course_list").html(LOADING);
	$.get("/app/student/course/list", function(data) {
		$("#stu_course_list").replaceWith(tm.student.course_list(data))
	});
}