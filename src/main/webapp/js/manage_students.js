$(function() {
});

function cs_create_form_submit() {
	var form = $('#cs_create_form');
	$.post(form.attr('action'), form.serialize(), function(data) {
		// Hide facebox
		jQuery(document).trigger('close.facebox');
		window.location.reload();
	} );
	return false;
}

function deleteCourseStudent(csid) {
	if (confirm('Are you sure?')) {
		var POST = { "csid": csid};
		$.post("/app/c/cs_delete", POST, function(data) {
			window.location.reload();
		});
	}
}