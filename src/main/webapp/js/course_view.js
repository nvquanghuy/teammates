function loadCSList() {
	$("#cs_list").html(LOADING);
	$.get("/app/coordinator/course/list_students?courseid=" + getParameterByName('courseid'), function(data) {
		$("#cs_list").replaceWith(tm.coordinator.cs_list(data));
	});
}

function deleteCourseStudent(csId) {
  if (confirm("Delete this student?")) {
    var POST = {"csid": csId};
    $.post("/app/coordinator/course/delete_student", POST, function(data) {
			flash(data);
			loadCSList();
    } );
	}
}

function deleteAllStudents(courseId) {
  if (confirm("Delete all students in this course?")) {
    var POST = {"courseid": courseId};
    $.post("/app/coordinator/course/delete_all_students", POST, function(data) {
			flash(data);
			renderCourseStudents();
    } );
	}
}

function remindAllStudents(courseId) {
	if (confirm('Are you sure you want to send registration keys to all the unregistered students for them to join your course?')) {
    var POST = {"courseid": courseId};
    $.post("/app/coordinator/course/remind_all_students", POST, function(data) {
			flash(data);
    } );
	}
}


function remindStudent(csId) {
  var POST = {"csid": csId};
  $.post("/app/coordinator/course/remind_student", POST, function(data) {
  	flash(data);
  } );
}
