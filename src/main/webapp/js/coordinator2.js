$(function() {
//	$("#form_addcourse").validate();
	/*$("#form_addcourse").submit( function() {
	//	if (!$(this).valid()) return;

		loadingStart(this);
		var that = this;

		$.post($(this).attr('action'), $(this).serialize(), function(data) {
			loadingEnd(that);
			if (data) {
				flash(data);
				if (data.status == SUCCESS) {
					loadCourseTable();
				}
			}
		} );
		return false;
	});*/
	
	$("#form_enrolstudents").submit( function() {
		// Just do some basic validation
		var POST = parseFormInputs($(this));
		var input = POST['input'];
		input = replaceAll(input,"|","\t").trim();

		// Remove trailing "\n"
		if (input.lastIndexOf("\n") == input.length-1) {
			input = input.substring(0, input.length-1);
		}
	 	if (isEnrollmentInputValid(input)) {
			showError("Please enter some students info.");
		}
		return false;
	} );

	//$('#add_eval_form').validate();
/*	$("#add_eval_form").submit( function() {
		//if (!$(this).valid()) return false;
		
		loadingStart(this); var that = this;
		
		$.post($(this).attr('action'), $(this).serialize(), function(data) {
			loadingEnd(that);
			if (data) {
				flash(data);
				if (data.status == SUCCESS) {
					loadEvaluationTable();
				}
			}
		} );
		return false;
	});*/
	
	//$("#form_edit_eval").validate();
	
});

function loadCourseTable() {
	$("#course_table").html(LOADING);
	$.get("/app/coordinator/course/list", function(data) {
		$("#course_table").replaceWith(tm.coordinator.course_list(data))
	});
}

function loadEvaluationTable() {
	$("#eval_table").html(LOADING);
	$.get("/app/coordinator/evaluation/list", function(data) {
		$("#eval_table").replaceWith(tm.coordinator.eval_list(data))
	});
}

// Load CourseStudent list
function loadCsList(courseId) {
	$(".cs_list").html(LOADING);
	$.get("/app/c/cs_list?courseid=" + courseId, function(data) {
		console.log(data);
		$(".cs_list").replaceWith(tm.coordinator.cs_list(data))
	});
}


function publishEval(evid) {
	if (!confirm('Publish this evaluation?')) {
		return false;
	}
	var POST = {"evid": evid};
	$.post('/app/coordinator/evaluation/ajax_publish', POST, function (data) {
		flash(data);
		loadEvaluationTable();
	});
}