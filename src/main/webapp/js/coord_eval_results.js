$(function() {
	
	$('.r').hide();
	$('.r_summary_reviewer').show();

	$('input:radio').click( function() {
		var name = 'r_' + $('input:radio[name=report_type]:checked').val()
			+ '_' + $('input:radio[name=review_type]:checked').val();
	
		$('.r').hide();
		$('.' + name).show();
	} );
});