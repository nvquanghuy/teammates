// This file was automatically generated from coord_home.soy.
// Please don't edit this file by hand.

if (typeof tm == 'undefined') { var tm = {}; }
if (typeof tm.coordinator == 'undefined') { tm.coordinator = {}; }


tm.coordinator.home = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('<div id=\'coord_wrapper\' class=\'form-wrapper shadow\'></div>');
  return opt_sb ? '' : output.toString();
};

;
// This file was automatically generated from coordinator.soy.
// Please don't edit this file by hand.

if (typeof tm == 'undefined') { var tm = {}; }
if (typeof tm.coordinator == 'undefined') { tm.coordinator = {}; }


tm.coordinator.course_header = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('<div class=\'page-header\'><div class=\'pull-right\' style=\'padding-top: 8px\'><a href=\'/app/coordinator/course\'><i class=\'icon-arrow-left\'></i> Back to main</a></div><h2>', soy.$$escapeHtml(opt_data.course.code), ' - ', soy.$$escapeHtml(opt_data.course.name), '<small> ', soy.$$escapeHtml(opt_data.course.term), '</small></h2><div class=\'clearfix\'></div></div>');
  return opt_sb ? '' : output.toString();
};


tm.coordinator.course_nav = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('<div class=\'span2\' style=\'margin-right: 20px\'><ul class="nav nav-pills nav-stack" id=\'course_nav\'><li ', (opt_data.selected == 'evals') ? 'class=\'active\'' : '', '><a href=\'/app/c/course?courseid=', soy.$$escapeHtml(opt_data.course.id), '\'><strong>Evaluations</strong></a></li><li ', (opt_data.selected == 'students') ? 'class=\'active\'' : '', '><a href=\'/app/c/manage_students?courseid=', soy.$$escapeHtml(opt_data.course.id), '\'>Manage Students</a></li><li ', (opt_data.selected == 'settings') ? 'class=\'active\'' : '', '><a href=\'/app/c/course_settings?courseid=', soy.$$escapeHtml(opt_data.course.id), '\'>Course Settings</a></li></ul></div>');
  return opt_sb ? '' : output.toString();
};


tm.coordinator.getTimeOptionString = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  for (var i34 = 1; i34 < 24; i34++) {
    output.append('<option ', (opt_data != null && opt_data.selected == i34) ? 'selected' : '', ' value="', soy.$$escapeHtml(i34), '">', soy.$$escapeHtml(i34), ':00</option>');
  }
  output.append('<option ', (opt_data != null && opt_data.selected == 24) ? 'selected' : '', ' value="24">23:59</option>');
  return opt_sb ? '' : output.toString();
};


tm.coordinator.getTimezoneOptionString = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('<option value="-12">UTC -12:00</option><option value="-11">UTC -11:00</option><option value="-10">UTC -10:00</option><option value="-9">UTC -09:00</option><option value="-8">UTC -08:00</option><option value="-7">UTC -07:00</option><option value="-6">UTC -06:00</option><option value="-5">UTC -05:00</option><option value="-4.5">UTC -04:30</option><option value="-4">UTC -04:00</option><option value="-3.5">UTC -03:30</option><option value="-3">UTC -03:00</option><option value="-2">UTC -02:00</option><option value="-1">UTC -01:00</option><option value="0">UTC </option><option value="1">UTC +01:00</option><option value="2">UTC +02:00</option><option value="3">UTC +03:00</option><option value="3.5">UTC +03:30</option><option value="4">UTC +04:00</option><option value="4.5">UTC +04:30</option><option value="5">UTC +05:00</option><option value="5.5">UTC +05:30</option><option value="5.75">UTC +05:45</option><option value="6">UTC +06:00</option><option value="6.5">UTC +06:30</option><option value="7">UTC +07:00</option><option value="8">UTC +08:00</option><option value="9">UTC +09:00</option><option value="9.5">UTC +09:30</option><option value="10">UTC +10:00</option><option value="11">UTC +11:00</option><option value="12">UTC +12:00</option><option value="13">UTC +13:00</option>');
  return opt_sb ? '' : output.toString();
};


tm.coordinator.getGracePeriodOptionString = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  for (var i52 = 5; i52 < 35; i52 += 5) {
    output.append('<option ', (opt_data != null && opt_data.selected == i52) ? 'selected' : '', ' value="', soy.$$escapeHtml(i52), '">', soy.$$escapeHtml(i52), ' min</option>');
  }
  return opt_sb ? '' : output.toString();
};

;
// This file was automatically generated from course_add.soy.
// Please don't edit this file by hand.

if (typeof tm == 'undefined') { var tm = {}; }
if (typeof tm.coordinator == 'undefined') { tm.coordinator = {}; }


tm.coordinator.course_add = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('<div class=\'form-wrapper center shadow\' style=\'background: #fff; width: 350px;\'><h3>Add New Course</h3><br/><p>To begin, create a new course</p><br/><form method="post" id="form_addcourse" name="form_addcourse"><div><label>Course code<br/><input type="text" name="courseid" ID="courseid" title=\'Enter the IDentifier of the course, e.g.CS3215Sem1.\' /></label></div><div><label>Course Name <br/><input type="text" name="coursename" ID="coursename" class=\'required\' title=\'Enter the name of the course, e.g. Software Engineering.\'/></label></div><div><label>Term <br/><input type="text" name="term" title=\'E.g. Fall 2012, 2011/2012 Sem 1, 2012 Special Sem, etc..\' /></label></div><div><input id=\'btnAddCourse\' type="submit" class="btn btn-primary" value="Add Course" /> &nbsp;<a class=\'btn\' href=\'/app/coordinator/course\'>Back</a></div></form>');
  tm.flash(null, output, opt_ijData);
  output.append('</div>');
  return opt_sb ? '' : output.toString();
};

;
// This file was automatically generated from course_create.soy.
// Please don't edit this file by hand.

if (typeof tm == 'undefined') { var tm = {}; }
if (typeof tm.coordinator == 'undefined') { tm.coordinator = {}; }


tm.coordinator.course_create = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('<div class=\'form-wrapper center shadow\' style=\'width: 350px;\'><h3>Create New Course</h3><br/><form method="post" action="/app/coordinator/course_create" id="form_addcourse"><div><label>Course code<br/><input type="text" name="courseid" ID="courseid" title=\'Enter the IDentifier of the course, e.g.CS3215Sem1.\' maxlength=50 class=\'required\' tabindex=1 /></label></div><div><label>Course Name <br/><input type="text" name="coursename" ID="coursename" class=\'required\' title=\'Enter the name of the course, e.g. Software Engineering.\' maxlength=50 tabindex=2 /></label></div><div><input id=\'btnAddCourse\' type="submit" class="button" value="Add Course" tabindex="3" /><span class=\'loading\'>Loading...</span></div></form></div>');
  return opt_sb ? '' : output.toString();
};

;
// This file was automatically generated from course_delete.soy.
// Please don't edit this file by hand.

if (typeof tm == 'undefined') { var tm = {}; }
if (typeof tm.coordinator == 'undefined') { tm.coordinator = {}; }


tm.coordinator.course_delete = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('<div class=\'form-wrapper center shadow\' style=\'width: 400px\'><h3>Deleting Course...</h3><br/><p>Are you sure you want to delete this course?<br/> <br/>This will remove all evaluations, student information etc that is related to this course. <br/><br/><em>You cannot undo this action.</em></p><br/><form method="post"><button type=\'submit\' class=\'button\'>Delete</button><a href=\'/app/coordinator/course/list\'>Cancel</a></form></div>');
  return opt_sb ? '' : output.toString();
};

;
// This file was automatically generated from course_enroll.soy.
// Please don't edit this file by hand.

if (typeof tm == 'undefined') { var tm = {}; }
if (typeof tm.coordinator == 'undefined') { tm.coordinator = {}; }


tm.coordinator.course_enroll = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  tm.flash(null, output, opt_ijData);
  output.append('<div class=\'container-wrapper shadow white-bg\'><div class=\'container\'><div class=\'row span12\'><br/><h3>Enroll Students for ', soy.$$escapeHtml(opt_data.course.name), '</h3><br/><ul class="nav nav-tabs"></ul><div class=\'tab-content\'><div class="tab-pane active" id="import"><div class=\'span12\'>To import your students, copy students data from spreadsheet to the box below.</div><div class=\'span12\' align=\'center\'><img src="/img/enroll.png" /></div><!--          <img src="/images/enrolInstructions.png" border="0" /> --><br /><form id="form_enrolstudents" method="post"><div align=\'center\'><textarea placeholder="Paste your spreadsheet here!" name="input" id=\'enrol_text\' style=\'width: 500px; height: 180px\' id="input"></textarea></div><div align=\'center\'><input type="submit" class="btn btn-primary" name="button_enrol" id="button_enrol" value="Enrol students" /> <a href="/app/c/course?courseid=', soy.$$escapeHtml(opt_data.course.id), '" class="btn t_back">Skip</a></div></form></div><div class="tab-pane" id="manual"></div></div></div></div></div>');
  return opt_sb ? '' : output.toString();
};

;
// This file was automatically generated from course_index.soy.
// Please don't edit this file by hand.

if (typeof tm == 'undefined') { var tm = {}; }
if (typeof tm.coordinator == 'undefined') { tm.coordinator = {}; }


tm.coordinator.course_index = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  tm.flash(null, output, opt_ijData);
  output.append('<div class=\'container-wrapper white-bg shadow\'><div class=\'container\'><div class=\'page-header\'><h2>Welcome to Teammates</h2></div><div class="row"><div class=\'span8 offset1\'><table class=\'table table-striped\'><thead><tr><th style=\'\'>Course</th><th style=\'\'>Created</th><th class=\'centeralign\'>Teams</th><th class=\'centeralign\'>Students</th></tr></thead><tbody>');
  var courseList107 = opt_data.courses;
  var courseListLen107 = courseList107.length;
  if (courseListLen107 > 0) {
    for (var courseIndex107 = 0; courseIndex107 < courseListLen107; courseIndex107++) {
      var courseData107 = courseList107[courseIndex107];
      output.append('<tr><td><a href="/app/c/course?courseid=', soy.$$escapeHtml(courseData107.id), '" class="t_course_view">', soy.$$escapeHtml(courseData107.code), ' - ', soy.$$escapeHtml(courseData107.name), '</a>&nbsp;', (courseData107.term != '') ? ' (' + soy.$$escapeHtml(courseData107.term) + ') ' : '', '</td><td class="">', soy.$$escapeHtml(courseData107.timeCreatedF), '</td><td class="t_course_teams">', soy.$$escapeHtml(courseData107.numTeams), '</td><td>', soy.$$escapeHtml(courseData107.numStudents), '</td></tr>');
    }
  } else {
    output.append('<tr><td colspan=\'3\'>There is no course.</td></tr>');
  }
  output.append('</tbody></table><br /></div></div><a class=\'btn\' href=\'/app/c/course_create\'>New course</a><br/> <br/></div></div>');
  return opt_sb ? '' : output.toString();
};

;
// This file was automatically generated from course_settings.soy.
// Please don't edit this file by hand.

if (typeof tm == 'undefined') { var tm = {}; }
if (typeof tm.coordinator == 'undefined') { tm.coordinator = {}; }


tm.coordinator.course_settings = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('<div class=\'container-wrapper white-bg shadow\'><div class="container">');
  tm.coordinator.course_header(opt_data, output, opt_ijData);
  output.append('<div class=\'row\'>');
  tm.coordinator.course_nav({course: opt_data.course, selected: 'settings'}, output, opt_ijData);
  output.append('<div class=\'span7\'><h3>Course settings</h3><br/><form method="post"><label>Code<br/><input type="text" name="code" value="', soy.$$escapeHtml(opt_data.course.code), '" /></label><label>Name <br/><input type="text" name="name" value="', soy.$$escapeHtml(opt_data.course.name), '" title=\'Enter the name of the course, e.g. Software Engineering.\' /></label><label>Term<br/><input type="text" name="term" title=\'Which term? E.g Fall 2012, 2011/2012 Sem 1\' value="', soy.$$escapeHtml(opt_data.course.term), '"  /></label><button type=\'submit\' class=\'btn btn-primary\'>Update</button></form>');
  tm.flash(null, output, opt_ijData);
  output.append('<hr/><p><form method=\'post\' action=\'/app/c/course_delete\' onsubmit=\'return confirm("Are you sure?");\'><input type=\'hidden\' name=\'courseid\' value=\'', soy.$$escapeHtml(opt_data.course.id), '\' /><button type=\'submit\' class=\'btn btn-danger\'>Delete Course</button>\tDelete the entire course. This is irreversible and not recommended.</form></p><br/></div></div></div> <!-- /container --></div>');
  return opt_sb ? '' : output.toString();
};

;
// This file was automatically generated from course_view.soy.
// Please don't edit this file by hand.

if (typeof tm == 'undefined') { var tm = {}; }
if (typeof tm.coordinator == 'undefined') { tm.coordinator = {}; }


tm.coordinator.course_view = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  tm.flash(null, output, opt_ijData);
  output.append('<div class=\'container-wrapper white-bg shadow\'><div class=\'container\'>');
  tm.coordinator.course_header({course: opt_data.course, selected: 'evals'}, output, opt_ijData);
  output.append('<div class=\'row\'>');
  tm.coordinator.course_nav({course: opt_data.course, selected: 'evals'}, output, opt_ijData);
  output.append('<div class=\'span7\'><h3>Evaluations</h3>');
  tm.coordinator.eval_list(opt_data, output, opt_ijData);
  output.append('<a class=\'btn\' href=\'/app/c/eval_create?courseid=', soy.$$escapeHtml(opt_data.course.id), '\'>Create Evaluation</a> <br/><br/></div></div></div></div>');
  return opt_sb ? '' : output.toString();
};

;
// This file was automatically generated from cs_create.soy.
// Please don't edit this file by hand.

if (typeof tm == 'undefined') { var tm = {}; }
if (typeof tm.coordinator == 'undefined') { tm.coordinator = {}; }


tm.coordinator.cs_create = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('<div style=\'display: block; width: 450px; min-height: 300px; padding-left: 20px; padding-top: 20px\'><h1>Create Student</h1><br/><form method="post" action="/app/c/cs_create" id="cs_create_form"><input type=\'hidden\' name=\'courseid\' value=\'', soy.$$escapeHtml(opt_data.course.id), '\' /><div><label for="">Student Name * <br/><input type="text" value="" name="name" /></label></div><div><label for="">Team * <br/><select name="teamid">');
  var teamList173 = opt_data.teams;
  var teamListLen173 = teamList173.length;
  for (var teamIndex173 = 0; teamIndex173 < teamListLen173; teamIndex173++) {
    var teamData173 = teamList173[teamIndex173];
    output.append('<option value="', soy.$$escapeHtml(teamData173.id), '">', soy.$$escapeHtml(teamData173.name), '</option>');
  }
  output.append('</select><input type=\'text\' name=\'newteamname\' placeholder=\'or enter new team name\' /></label></div><div><label for="">Email * <br/><input class="" type="text" value="" id="email" name="email"/></label></div><div><input type="submit" class="btn btn-primary" value="Add student" /></div></form></div>');
  return opt_sb ? '' : output.toString();
};

;
// This file was automatically generated from cs_edit.soy.
// Please don't edit this file by hand.

if (typeof tm == 'undefined') { var tm = {}; }
if (typeof tm.coordinator == 'undefined') { tm.coordinator = {}; }


tm.coordinator.cs_edit = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('<div class=\'form-wrapper shadow center white-bg\' style=\'width: 300px\'><h1>Edit Student</h1><br/><form method="post"><input type=\'hidden\' name=\'csid\' value=\'', soy.$$escapeHtml(opt_data.cs.id), '\' /><div><label for="">Student Name * <br/><input type="text" value="', soy.$$escapeHtml(opt_data.cs.name), '" name="name" id="editname"/></label></div><div><label for="">Team * <br/><select name="team">');
  var teamList188 = opt_data.teams;
  var teamListLen188 = teamList188.length;
  for (var teamIndex188 = 0; teamIndex188 < teamListLen188; teamIndex188++) {
    var teamData188 = teamList188[teamIndex188];
    output.append('<option value="', soy.$$escapeHtml(teamData188.id), '" ', (teamData188.id == opt_data.cs.team.id) ? 'selected=\'selected\'' : '', '>', soy.$$escapeHtml(teamData188.name), '</option>');
  }
  output.append('</select></label></div><div><label for="">Email * <br/><input class="" type="text" value="', soy.$$escapeHtml(opt_data.cs.email), '" id="email" name="email"/></label></div><div><label for="">Comments * <br/><textarea name="comments" id="editcomments" rows="6" cols="80">', soy.$$escapeHtml(opt_data.cs.comments), '</textarea></label></div><div><input type="submit" class="btn btn-primary" name="button_editstudent" id="button_editstudent" value="Save Changes" /> <a class=\'btn\' href=\'/app/c/manage_students?courseid=', soy.$$escapeHtml(opt_data.cs.course.id), '\'>Back</a></div></form></div>');
  return opt_sb ? '' : output.toString();
};

;
// This file was automatically generated from cs_list.soy.
// Please don't edit this file by hand.

if (typeof tm == 'undefined') { var tm = {}; }
if (typeof tm.coordinator == 'undefined') { tm.coordinator = {}; }


tm.coordinator.cs_list = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('<div class=\'cs_list\'><table class=\'table table-striped\'><thead><tr><th>Student Name</th><th>Team</th><th>Email</th><th>Actions</th></tr></thead><tbody>');
  var studentList209 = opt_data.students;
  var studentListLen209 = studentList209.length;
  if (studentListLen209 > 0) {
    for (var studentIndex209 = 0; studentIndex209 < studentListLen209; studentIndex209++) {
      var studentData209 = studentList209[studentIndex209];
      output.append('<tr><td>', soy.$$escapeHtml(studentData209.name), '</td><td>', soy.$$escapeHtml(studentData209.team.name), '</td><td>', soy.$$escapeHtml(studentData209.email), '</td><td><a class="t_student_edit" href="/app/c/cs_edit?csid=', soy.$$escapeHtml(studentData209.id), '">Edit</a>&nbsp;<a class="t_student_delete" href="#" onclick="deleteCourseStudent(', soy.$$escapeHtml(studentData209.id), ')">Delete</a></td></tr>');
    }
  } else {
    output.append('<tr><td colspan="4">There is no student enrolled.</td></tr>');
  }
  output.append('</tbody></table></div>');
  return opt_sb ? '' : output.toString();
};


tm.coordinator.eval_cs_list = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append(soy.$$escapeHtml(opt_data.teams), '<table class=\'table table-striped\' style=\'width: 80%\'><thead><tr><th>Student Name</th><th>Email</th>\b\t\t<th>Actions</th></tr></thead><tbody>');
  var teamList228 = opt_data.teams;
  var teamListLen228 = teamList228.length;
  for (var teamIndex228 = 0; teamIndex228 < teamListLen228; teamIndex228++) {
    var teamData228 = teamList228[teamIndex228];
    output.append('<tr><td colspan=\'3\'><strong>Team:</strong> ', soy.$$escapeHtml(teamData228.name), '</td></tr>');
    var studentList232 = opt_data.students;
    var studentListLen232 = studentList232.length;
    for (var studentIndex232 = 0; studentIndex232 < studentListLen232; studentIndex232++) {
      var studentData232 = studentList232[studentIndex232];
      output.append(' ', (studentData232.teamId == teamData228.id) ? '<tr><td><a href=\'#\'>' + soy.$$escapeHtml(studentData232.name) + '</a></td><td>' + soy.$$escapeHtml(studentData232.email) + '</td><td><a class="t_student_edit" href="/app/coordinator/course/edit_student?csid=' + soy.$$escapeHtml(studentData232.id) + '">Edit</a> <a class="t_student_delete" href="#" onclick="deleteCourseStudent(' + soy.$$escapeHtml(studentData232.id) + ')">Delete</a></td></tr>' : '');
    }
  }
  output.append('</tbody></table>');
  return opt_sb ? '' : output.toString();
};

;
// This file was automatically generated from cs_view.soy.
// Please don't edit this file by hand.

if (typeof tm == 'undefined') { var tm = {}; }
if (typeof tm.coordinator == 'undefined') { tm.coordinator = {}; }


tm.coordinator.cs_view = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('<h1>STUDENT DETAIL</h1><table><tr><td class="fieldname">Student Name:</td><td>', soy.$$escapeHtml(opt_data.cs.name), '</td></tr><tr><td class="fieldname">Team Name:</td><td>', soy.$$escapeHtml(opt_data.cs.team.name), '</td></tr><tr><td class="fieldname">E-mail Address:</td><td>', soy.$$escapeHtml(opt_data.cs.email), '</td></tr><tr><td class="fieldname">Comments:</td><td>', soy.$$escapeHtml(opt_data.cs.comments), '</td></div></td></tr></table><br /><br /><br /><div align=\'center\'><a class=\'btn\' href=\'/app/c/course?courseid=', soy.$$escapeHtml(opt_data.cs.course.id), '\'>Back</a></div>');
  return opt_sb ? '' : output.toString();
};

;
// This file was automatically generated from eval_create.soy.
// Please don't edit this file by hand.

if (typeof tm == 'undefined') { var tm = {}; }
if (typeof tm.coordinator == 'undefined') { tm.coordinator = {}; }


tm.coordinator.eval_create = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('<div class=\'form-wrapper center shadow white-bg\' style=\'width: 320px;\'><h1>New Evaluation</h1><br/><div class=\'alert alert-info\'>An evaluation is 1 round of peer feedback.</div><br/><form method="post" name="add_eval_form" id="add_eval_form"><div><label>Course <br/><strong>', soy.$$escapeHtml(opt_data.course.display), '</strong></label></div><div><label>Evaluation Name * <br/><input class=\'required\' style="width: 260px;" type="text" name="evaluationname" maxlength = 30><small>E.g. Midterm review, Final Review..</small></label></div><!--<div><label>Opening time <br/><input style="width: 100px; display: inline" title="When the evaluation automatically opens" class=\'date\' type="text" name="start_date"> @ <select style="width: 70px; display: inline" name="start_time">');
  tm.coordinator.getTimeOptionString(null, output, opt_ijData);
  output.append('</select></label></div>--><div><label>Deadline: * <br/><input style="width: 100px; display: inline" type="date" name="deadline_date"> @ <select style="width: 70px; display: inline" name="deadline_time" tabindex=6>');
  tm.coordinator.getTimeOptionString(null, output, opt_ijData);
  output.append('</select></label></div><!-- Disable grace period. --><div><label>Instructions to students <br/><textarea rows="4" cols="100" style="width: 100%" class="required" name="instr" tabindex=8>Please submit your peer evaluation based on the overall contribution of your teammates so far.</textarea></label></div><br/><div><input id=\'t_btnAddEvaluation\' value=\'Create\' type="submit" class="btn btn-primary" tabindex=\'9\' /> <a class=\'btn\' href=\'/app/c/course?courseid=', soy.$$escapeHtml(opt_data.course.id), '\'>Cancel</a></div>');
  tm.flash(null, output, opt_ijData);
  output.append('</form></div>\n<script language="javascript">\n$(function() {\n\t/*  var now = new Date();\n\t  var currentDate = convertDateToDDMMYYYY(now);\n\n\t  var hours = convertDateToHHMM(now).substring(0, 2);\n\t  var currentTime;\n\n\t  if (hours.substring(0, 1) == "0") {\n\t    currentTime = (parseInt(hours.substring(1, 2)) + 1) % 24;\n\t    } else {\n\t    currentTime = (parseInt(hours.substring(0, 2)) + 1) % 24;\n\t  }\n\n\t  var timeZone = -now.getTimezoneOffset() / 60;\n\n\t  $("{EVALUATION_START}").value = currentDate;\n\t  document.getElementById(EVALUATION_STARTTIME).value = currentTime;\n\t  document.getElementById(EVALUATION_TIMEZONE).value = timeZone; */\n});\n\n<\/script>\n');
  return opt_sb ? '' : output.toString();
};

;
// This file was automatically generated from eval_delete.soy.
// Please don't edit this file by hand.

if (typeof tm == 'undefined') { var tm = {}; }
if (typeof tm.coordinator == 'undefined') { tm.coordinator = {}; }


tm.coordinator.eval_delete = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('<div class=\'form-wrapper white-bg center shadow\' style=\'width: 400px\'><h3>Deleting Evaluation...</h3><br/><p>Are you sure you want to delete this evaluation?<br/> <br/><br/><em>You cannot undo this action.</em></p><br/><form method="post"><button type=\'submit\' class=\'btn btn-primary\'>Delete</button> <a class=\'btn\' href=\'/app/c/eval?evid=', soy.$$escapeHtml(opt_data.ev.id), '\'>Cancel</a></form></div>');
  return opt_sb ? '' : output.toString();
};

;
// This file was automatically generated from eval_edit.soy.
// Please don't edit this file by hand.

if (typeof tm == 'undefined') { var tm = {}; }
if (typeof tm.coordinator == 'undefined') { tm.coordinator = {}; }


tm.coordinator.eval_edit = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('<div class=\'form-wrapper center shadow white-bg\' style=\'width: 350px\'><h1>Edit Evaluation</h1><br/><form id="form_edit_eval" method="post"><div><label>Course <br/><strong>', soy.$$escapeHtml(opt_data.ev.courseDisplay), '</strong></label></div><div><label>Evaluation Name <br/><input type=\'text\' name=\'name\' value=\'', soy.$$escapeHtml(opt_data.ev.name), '\' /></label></div><!--\t<div><label>Opening Time <br/><input style="width: 100px; display: inline" type="date" name="start_date" id="start_date" class=\'date required\' value=\'', soy.$$escapeHtml(opt_data.ev.startDate), '\'/><select style="width: 70px; display: inline" name=\'start_time\'>');
  tm.coordinator.getTimeOptionString({selected: opt_data.ev.startTime}, output, opt_ijData);
  output.append('</select></label></div> --><div><label>Deadline <br/><input style="width: 100px; display: inline" class=\'\' type="date" name="deadline_date" value=\'', soy.$$escapeHtml(opt_data.ev.deadlineDate), '\' tabindex=3><select style="width: 70px; display: inline" name="deadline_time" tabindex=4>');
  tm.coordinator.getTimeOptionString({selected: opt_data.ev.deadlineTime}, output, opt_ijData);
  output.append('</select></label></div><div><label>Grace Period: <br/></label></div><div><label><select style="width: 70px;" name=\'graceperiod\' tabindex=5>');
  tm.coordinator.getGracePeriodOptionString({selected: opt_data.ev.gracePeriod}, output, opt_ijData);
  output.append('</select></label></div><div><label>Instructions: <br/><textarea rows="2" cols="80" class="required" type="text" name="instr" id="instr" tabindex=8>', soy.$$escapeHtml(opt_data.ev.instructions), '</textarea></label></div><div><input type="submit" class="btn btn-primary" name="button_editevaluation" id="button_editevaluation" value="Save Changes" /> &nbsp;<a href="/app/c/eval?evid=', soy.$$escapeHtml(opt_data.ev.id), '" class=\'btn\'>Back</a></div></form>');
  return opt_sb ? '' : output.toString();
};

;
// This file was automatically generated from eval_index.soy.
// Please don't edit this file by hand.

if (typeof tm == 'undefined') { var tm = {}; }
if (typeof tm.coordinator == 'undefined') { tm.coordinator = {}; }


tm.coordinator.eval_index = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('<div class=\'form-wrapper white-bg shadow center\' style=\'width: 800px\'><h1>Evaluations</H1>');
  tm.flash(null, output, opt_ijData);
  tm.coordinator.eval_list(opt_data, output, opt_ijData);
  output.append('<br/><br/><div style=\'width: 950px; margin: 0 auto; display: block\'><a class=\'button\' href=\'/app/coordinator/evaluation/create\'>Create new Evaluation</a></div></div>');
  return opt_sb ? '' : output.toString();
};

;
// This file was automatically generated from eval_list.soy.
// Please don't edit this file by hand.

if (typeof tm == 'undefined') { var tm = {}; }
if (typeof tm.coordinator == 'undefined') { tm.coordinator = {}; }


tm.coordinator.eval_list = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('<div id=\'eval_table\'><table class=\'table\'><thead><tr><th>Evaluation</th><th>Timing</th><th>Status</th><th>Response Rate</th></tr></thead>');
  var evalList318 = opt_data.evals;
  var evalListLen318 = evalList318.length;
  if (evalListLen318 > 0) {
    for (var evalIndex318 = 0; evalIndex318 < evalListLen318; evalIndex318++) {
      var evalData318 = evalList318[evalIndex318];
      output.append('<tr><td><a class=\'\' href=\'/app/coordinator/evaluation/view?evid=', soy.$$escapeHtml(evalData318.id), '\'>', soy.$$escapeHtml(evalData318.name), '</a></td><td>Deadline: ', soy.$$escapeHtml(evalData318.deadlineR), '</td><td>');
      tm.coordinator.evalStatus({status: evalData318.status}, output, opt_ijData);
      output.append('</td><td align=\'center\'>', soy.$$escapeHtml(evalData318.numSubmitted), ' / ', soy.$$escapeHtml(evalData318.numStudents), '</td></tr>');
    }
  } else {
    output.append('<tr><td colspan=\'6\'>There is no evaluation</td></tr>');
  }
  output.append('</table></div>');
  return opt_sb ? '' : output.toString();
};


tm.coordinator.evalStatus = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append((opt_data.status == 0) ? 'Awaiting' : (opt_data.status == 1) ? 'Open' : (opt_data.status == 2) ? 'Closed' : (opt_data.status == 3) ? 'Published' : 'Unknown');
  return opt_sb ? '' : output.toString();
};

;
// This file was automatically generated from eval_results.soy.
// Please don't edit this file by hand.

if (typeof tm == 'undefined') { var tm = {}; }
if (typeof tm.coordinator == 'undefined') { tm.coordinator = {}; }


tm.coordinator.eval_results = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('<table class="headerform"><tbody><tr><td class="fieldname">Course ID:</td><td>', soy.$$escapeHtml(opt_data.ev.courseDisplay), '</td></tr><tr><td class="fieldname">Evaluation name:</td><td>', soy.$$escapeHtml(opt_data.ev.name), '</td></tr><tr><td class  = "fieldname">Opening time:</td><td>', soy.$$escapeHtml(opt_data.ev.startR), '</td></tr><tr><td class = "fieldname">Closing time:</td><td>', soy.$$escapeHtml(opt_data.ev.deadlineR), '</td></tr></tbody></table><br /><div class=\'form-wrapper center shadow\' style=\'width: 400px\'><b>Report Type:</b> <br/><label><input type="radio" name="report_type" value="summary" checked="checked">Summary</label><label><input type="radio" name=\'report_type\' value="detail">Details</label><br/><br/><b>Review Type:</b> <br/><label for="radio_reviewer"><input type="radio" name="review_type" value="reviewer" checked="checked">By Reviewer </label><label><input disabled=\'disabled\' type="radio" name="review_type" value="reviewee">By Reviewee</label><br/></div><br/><table id=\'dataform\' class=\'r r_summary_reviewer\'><tbody><tr><th class="leftalign">TEAM</th><th class="leftalign">STUDENT</th><th class="centeralign">SUBMITTED</th><th class="centeralign">ACTION(S)</th></tr>');
  var submitList360 = opt_data.submits;
  var submitListLen360 = submitList360.length;
  for (var submitIndex360 = 0; submitIndex360 < submitListLen360; submitIndex360++) {
    var submitData360 = submitList360[submitIndex360];
    output.append('<tr><td>', soy.$$escapeHtml(submitData360.teamName), '</td><td>', soy.$$escapeHtml(submitData360.studentName), '</td><td class="centeralign">');
    tm.printYesNo({val: submitData360.submitted}, output, opt_ijData);
    output.append('</td><td class="centeralign"><a href=\'#\'>View</a></td></tr>');
  }
  output.append('</tbody></table><div id="detail" class=\'r r_detail_reviewer\'>');
  var teamList371 = opt_data.teams;
  var teamListLen371 = teamList371.length;
  for (var teamIndex371 = 0; teamIndex371 < teamListLen371; teamIndex371++) {
    var teamData371 = teamList371[teamIndex371];
    output.append('<div class="result_team"><strong>Team: </strong>', soy.$$escapeHtml(teamData371.name), '<br/><table class="result_table">');
    var submitList375 = opt_data.submits;
    var submitListLen375 = submitList375.length;
    for (var submitIndex375 = 0; submitIndex375 < submitListLen375; submitIndex375++) {
      var submitData375 = submitList375[submitIndex375];
      if (submitData375.teamId == teamData371.id) {
        output.append('<tr><td>Submitter: ', soy.$$escapeHtml(submitData375.studentName), '</td></tr><tr><td><table width=\'100%\'><tr><th>To Student</th><th>Comments</th></tr>');
        var entryList381 = opt_data.entries;
        var entryListLen381 = entryList381.length;
        for (var entryIndex381 = 0; entryIndex381 < entryListLen381; entryIndex381++) {
          var entryData381 = entryList381[entryIndex381];
          output.append((entryData381.submitId == submitData375.id) ? '<tr><td>' + soy.$$escapeHtml(entryData381.toStudentName) + '</td><td>' + soy.$$escapeHtml(entryData381.content) + '</td></tr>' : '');
        }
        output.append('</table></td></tr>');
      }
    }
    output.append('</table></div>');
  }
  output.append('</div><br/><div align=\'center\'><a class=\'\' href=\'/app/coordinator/evaluation\'>Back</a></div>');
  return opt_sb ? '' : output.toString();
};

;
// This file was automatically generated from eval_view.soy.
// Please don't edit this file by hand.

if (typeof tm == 'undefined') { var tm = {}; }
if (typeof tm.coordinator == 'undefined') { tm.coordinator = {}; }


tm.coordinator.eval_view = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('<script>var deadline_mils = ', soy.$$escapeHtml(opt_data.deadline_mils), ';\n\t$(function() {\n\t\tvar offset = new Date().getTimezoneOffset();\n\t\tvar d = new Date(deadline_mils);\n\t\t// Minus to timezone.\n\t\td.setMinutes(d.getMinutes() - offset);\n\t\t$(".countdown").countdown({\n\t\t\tuntil: d,\n\t\t\tcompact: true,\n\t\t\tformat: \'DHMS\'\n\t\t});\n\t});\n\t<\/script>');
  tm.flash(null, output, opt_ijData);
  output.append('<div class=\'container-wrapper white-bg shadow\'><div class=\'container\'><div class=\'page-header\'><div class=\'pull-right\' style=\'padding-top: 8px\'><a href=\'/app/c/course?courseid=', soy.$$escapeHtml(opt_data.course.id), '\'><i class=\'icon-arrow-left\'></i> Back to course</a></div><h2>', soy.$$escapeHtml(opt_data.course.code), ' - ', soy.$$escapeHtml(opt_data.course.name), '<small> ', soy.$$escapeHtml(opt_data.course.term), '</small></h2></div><div></div><div class=\'row\'><div class=\'span1\'>&nbsp;</div><div class=\'span8\'><div style=\'margin-bottom: 20px\'><div class=\'pull-right\' style=\'padding-top: 8px;\'><a class=\'_hover-item \' style=\'font-weight: normal;\' href=\'/app/c/eval_edit?evid=', soy.$$escapeHtml(opt_data.ev.id), '\'>Edit <i class=\'icon-edit\'></i></a> &nbsp;<a class=\'_hover-item \' style=\'font-weight: normal;\' href=\'/app/c/eval_delete?evid=', soy.$$escapeHtml(opt_data.ev.id), '\'>Delete <i class=\'icon-remove-sign\'></i></a></div><h3 class=\'hover-container\'><span class=\'muted\'>Evaluation: </span>', soy.$$escapeHtml(opt_data.ev.name), ' </h3></div>');
  if (opt_data.ev.status == 0) {
    output.append('<div class=\'eval-pbar\'><div class=\'box-eval shadow\'><ul class=\'clearfix\'><li class=\'step1 current\'>Launch Evaluation</li><li class=\'step2 notyet\'>Collect Submissions</li><li class=\'step3 notyet\'>Review &amp; Publish\b</li><li class=\'step4 notyet\'>Done</li></ul><hr style=\'margin: 0; margin-bottom: 20px\' /><p><strong>Instructions:</strong><br/>', soy.$$escapeHtml(opt_data.ev.instructions), '</p><p><strong>Deadline:</strong><br/>', soy.$$escapeHtml(opt_data.ev.deadline), '</p><br/><p><br/><form method=\'post\' action=\'/app/c/eval_open\'><input type=\'hidden\' name=\'evid\' value=\'', soy.$$escapeHtml(opt_data.ev.id), '\' /><button class=\'btn btn-large btn-primary\'>Launch Evaluation</button> <em>Make sure all the details are correct before launching the Evaluation. </em></form></p></div></div>');
  } else if (opt_data.ev.status == 1) {
    output.append('<div class=\'eval-pbar\'><div class=\'box-eval shadow\'><ul class=\'clearfix\'><li class=\'step1 done\'>Launch Evaluation</li><li class=\'step2 current\'>Collect Submissions</li><li class=\'step3 notyet\'>Review &amp; Publish</li><li class=\'step4 notyet\'>Done</li></ul><hr style=\'margin: 0; margin-bottom: 20px\' /><table class=\'table table-striped\' style=\'\'><thead><tr><th style=\'width: 40%\'>Name</th><th>Team</th><th>Submitted</th></tr></thead><tbody>');
    var submitList433 = opt_data.submits;
    var submitListLen433 = submitList433.length;
    for (var submitIndex433 = 0; submitIndex433 < submitListLen433; submitIndex433++) {
      var submitData433 = submitList433[submitIndex433];
      output.append('<tr ', (submitData433.submitted == true) ? 'class=\'row-submitted\'' : '', '><td>', soy.$$escapeHtml(submitData433.courseStudent.name), '<a href=\'/app/s/submit?evid=', soy.$$escapeHtml(opt_data.ev.id), '&csid=', soy.$$escapeHtml(submitData433.courseStudent.id), '&s=', soy.$$escapeHtml(submitData433.secret), '\'>[S]</a></td><td>', soy.$$escapeHtml(submitData433.courseStudent.team.name), '</td><td>', (submitData433.submitted) ? '<a href=\'/app/c/feedback_view_from?sid=' + soy.$$escapeHtml(submitData433.id) + '\' rel=\'facebox\'>Submitted</a>' : 'Not yet', '</td></tr>');
    }
    output.append('</tbody></table><p><strong>Deadline: </strong> ', soy.$$escapeHtml(opt_data.ev.deadline), ' (<span class=\'countdown\'></span>)</p><p><form method=\'post\' action=\'/app/c/eval_remind\'><input type=\'hidden\' name=\'evid\' value=\'', soy.$$escapeHtml(opt_data.ev.id), '\' /><button class=\'btn\'>Email Reminders</button> Send email reminders to those who haven\'t submitted their feedback.</form></p><p><form method=\'post\' action=\'/app/c/eval_close\'><input type=\'hidden\' name=\'evid\' value=\'', soy.$$escapeHtml(opt_data.ev.id), '\' /><button class=\'btn\'>End Evaluation Now</button> <br/>If you want to end the evaluation early. The evaluation will automatically end when deadline comes.</form></p></div></div>');
  } else if (opt_data.ev.status == 2) {
    output.append('<div class=\'eval-pbar\'><div class=\'box-eval shadow\'><ul class=\'clearfix\'><li class=\'step1 done\'>Launch Evaluation</li><li class=\'step2 done\'>Collect Submissions</li><li class=\'step3 current\'>Review &amp; Publish</li><li class=\'step4 notyet\'>Done</li></ul><hr style=\'margin: 0; margin-bottom: 20px\' /><div class=\'alert alert-info\'>Do review your students\' submissions below. Remember to publish the feedback.</div>');
    tm.coordinator.submits(opt_data, output, opt_ijData);
    output.append('<p><br/><form method=\'post\' action=\'/app/c/eval_publish\'><input type=\'hidden\' name=\'evid\' value=\'', soy.$$escapeHtml(opt_data.ev.id), '\' /><button class=\'btn btn-primary btn-large\'>Publish</button> <em>Send feedback to students.</em></form></p></div></div>');
  } else {
    output.append('<div class=\'eval-pbar\'><div class=\'box-eval shadow\'><ul class=\'clearfix\'><li class=\'step1 done\'>Launch Evaluation</li><li class=\'step2 done\'>Collect Submissions</li><li class=\'step3 done\'>Review &amp; Publish</li><li class=\'step4 current\'>Done</li></ul><hr style=\'margin: 0; margin-bottom: 20px\' /><div class=\'alert alert-info\'>The evaluation is over. Thank you for using Teammates!</div>');
    tm.coordinator.submits(opt_data, output, opt_ijData);
    output.append('</div></div>');
  }
  output.append('</div></div><br/></div></div>');
  return opt_sb ? '' : output.toString();
};


tm.coordinator.submits = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('<table class=\'table table-striped\' style=\'\'><thead><tr><th>Name</th><th>Team</th><th>Submitted</th></tr></thead><tbody>');
  var submitList482 = opt_data.submits;
  var submitListLen482 = submitList482.length;
  for (var submitIndex482 = 0; submitIndex482 < submitListLen482; submitIndex482++) {
    var submitData482 = submitList482[submitIndex482];
    output.append('<tr ', (submitData482.submitted == true) ? 'class=\'row-submitted\'' : '', '><td>', soy.$$escapeHtml(submitData482.courseStudent.name), '</td><td>', soy.$$escapeHtml(submitData482.courseStudent.team.name), '</td><td><a href=\'/app/c/feedback_view_from?sid=', soy.$$escapeHtml(submitData482.id), '\' rel=\'facebox\'>[From this]</a> &nbsp;<a href=\'/app/c/feedback_view_for?csid=', soy.$$escapeHtml(submitData482.courseStudent.id), '&amp;evid=', soy.$$escapeHtml(opt_data.ev.id), '\' rel=\'facebox\'>[For this]</a></td></tr>');
  }
  output.append('</tbody></table>');
  return opt_sb ? '' : output.toString();
};

;
// This file was automatically generated from manage_students.soy.
// Please don't edit this file by hand.

if (typeof tm == 'undefined') { var tm = {}; }
if (typeof tm.coordinator == 'undefined') { tm.coordinator = {}; }


tm.coordinator.manage_students = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('<script>var courseId = ', soy.$$escapeHtml(opt_data.course.id), ';<\/script><div class=\'container-wrapper white-bg shadow\'><div class=\'container\'>');
  tm.coordinator.course_header(opt_data, output, opt_ijData);
  output.append('<div class=\'row\'>');
  tm.coordinator.course_nav({course: opt_data.course, selected: 'students'}, output, opt_ijData);
  output.append('<div class=\'span7\'><h3>Manage Students</h3>');
  tm.coordinator.cs_list(opt_data, output, opt_ijData);
  output.append('<a class="btn" href="/app/c/cs_create?courseid=', soy.$$escapeHtml(opt_data.course.id), '" rel=\'facebox\'>New Student</a><br/> <br/></div></div></div></div><div class="modal hide" id="myModal"><form method="post" id="new_student_form" action="/app/c/cs_create"><input type="hidden" name="courseid" value="', soy.$$escapeHtml(opt_data.course.id), '" /><div class="modal-header"><a class="close" data-dismiss="modal">×</a><h3>New Student</h3></div><div class="modal-body"><div><label>Name: <br/><input placeholder="Student name" type="text" name="name" /></label></div><div><label>Email: <br/><input placeholder="Email address" type="text" name="email" /></label></div><div><label>Team: <br/></label><select name="team" id="ddlTeam" style="display: inline"><option value="0">[Select Team]</option></select> &nbsp; or &nbsp; <input type="text" style="width: 150px; display: inline" placeholder="New team" name="newteamname" /></div></div><div class="modal-footer"><a href="#" data-dismiss="modal" class="btn">Close</a><button type=\'submit\' class=\'btn btn-primary\'>Add Student</button></div><form></div>');
  return opt_sb ? '' : output.toString();
};

;
// This file was automatically generated from submit_view.soy.
// Please don't edit this file by hand.

if (typeof tm == 'undefined') { var tm = {}; }
if (typeof tm.coordinator == 'undefined') { tm.coordinator = {}; }


tm.coordinator.feedback_view_from = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('<div style=\'display: block; width: 500px; min-height: 300px; padding-left: 20px; padding-top: 20px\'><h3>Submission of ', soy.$$escapeHtml(opt_data.cs.name), '</h3><p>Below is what ', soy.$$escapeHtml(opt_data.cs.name), ' feedback to others:</p><br/>');
  var entryList526 = opt_data.submit.entries;
  var entryListLen526 = entryList526.length;
  for (var entryIndex526 = 0; entryIndex526 < entryListLen526; entryIndex526++) {
    var entryData526 = entryList526[entryIndex526];
    output.append(' ', (entryData526.toStudentId != opt_data.cs.id) ? '<div class=\'row\'><div class=\'span2\' style=\'text-align: right; padding-right: 10px; color: #7a7a7a\'>' + soy.$$escapeHtml(entryData526.toStudentName) + '</div><div class=\'span4\'><div class=\'alert alert-info\'>' + soy.$$escapeHtml(entryData526.content) + '</div></div></div>' : '');
  }
  output.append('</div>');
  return opt_sb ? '' : output.toString();
};


tm.coordinator.feedback_view_for = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('<div style=\'display: block; width: 500px; min-height: 300px; padding-left: 20px; padding-top: 20px\'><h3>Feedback for ', soy.$$escapeHtml(opt_data.cs.name), '</h3><p>Below is what others feedback for ', soy.$$escapeHtml(opt_data.cs.name), ':</p><br/>');
  var entryList543 = opt_data.entries;
  var entryListLen543 = entryList543.length;
  for (var entryIndex543 = 0; entryIndex543 < entryListLen543; entryIndex543++) {
    var entryData543 = entryList543[entryIndex543];
    output.append(' ', (entryData543.fromStudentId != opt_data.cs.id) ? '<div class=\'row\'><div class=\'span2\' style=\'text-align: right; padding-right: 10px; color: #7a7a7a\'>' + soy.$$escapeHtml(entryData543.fromStudentName) + '</div><div class=\'span4\'><div class=\'alert alert-info\'>' + soy.$$escapeHtml(entryData543.content) + '</div></div></div>' : '');
  }
  output.append('</div>');
  return opt_sb ? '' : output.toString();
};

;
// This file was automatically generated from stu_course_index.soy.
// Please don't edit this file by hand.

if (typeof tm == 'undefined') { var tm = {}; }
if (typeof tm.student == 'undefined') { tm.student = {}; }


tm.student.course_index = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  tm.student.course_join(null, output, opt_ijData);
  tm.flash(null, output, opt_ijData);
  tm.student.course_list(opt_data, output, opt_ijData);
  return opt_sb ? '' : output.toString();
};

;
// This file was automatically generated from stu_course_join.soy.
// Please don't edit this file by hand.

if (typeof tm == 'undefined') { var tm = {}; }
if (typeof tm.student == 'undefined') { tm.student = {}; }


tm.student.course_join = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('<div class=\'form-wrapper shadow center\' style=\'width:500px\'><h3>JOIN NEW COURSE</h3><br/><form method="post" id="form_join_course" action="/app/student/course/join"><div>Registration Key:</div><div><input type=\'text\' class=\'required\' name=\'regkey\' title=\'Enter your registration key for the course\' /></div><div><input type=\'submit\' value=\'Join Course\' /><span class=\'loading\'>Loading...</span></div></form></div>');
  return opt_sb ? '' : output.toString();
};

;
// This file was automatically generated from stu_course_list.soy.
// Please don't edit this file by hand.

if (typeof tm == 'undefined') { var tm = {}; }
if (typeof tm.student == 'undefined') { tm.student = {}; }


tm.student.course_list = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('<div id=\'stu_course_list\'><br /><br /><table id=\'dataform\' style=\'width: 600px\'><tr><th>COURSE</input></th><th class=\'centeralign\'>TEAMS</th><th class="centeralign">ACTION(S)</th></tr>');
  var csList567 = opt_data.cstudents;
  var csListLen567 = csList567.length;
  if (csListLen567 > 0) {
    for (var csIndex567 = 0; csIndex567 < csListLen567; csIndex567++) {
      var csData567 = csList567[csIndex567];
      output.append('<tr><td>', soy.$$escapeHtml(csData567.courseCode), ' - ', soy.$$escapeHtml(csData567.courseName), '</td><td>', soy.$$escapeHtml(csData567.teamName), '</td><td class="centeralign"><a href="/app/student/course/view?csid=', soy.$$escapeHtml(csData567.id), '">View</a></td></tr>');
    }
  } else {
    output.append('<tr><td colspan=\'6\'>You haven\'t joined any course</td></tr>');
  }
  output.append('</table></div>');
  return opt_sb ? '' : output.toString();
};

;
// This file was automatically generated from stu_course_view.soy.
// Please don't edit this file by hand.

if (typeof tm == 'undefined') { var tm = {}; }
if (typeof tm.student == 'undefined') { tm.student = {}; }


tm.student.course_view = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('<h1>COURSE DETAIL</h1><br /><br /><table width="600" class="detailform"><tr><td>Course</td><td>', soy.$$escapeHtml(opt_data.cs.courseCode), ' - ', soy.$$escapeHtml(opt_data.cs.courseName), '</td></tr><tr><td>Your Team</td><td>', soy.$$escapeHtml(opt_data.cs.teamName), ' <br/> <br/><ol>');
  var memberList590 = opt_data.members;
  var memberListLen590 = memberList590.length;
  for (var memberIndex590 = 0; memberIndex590 < memberListLen590; memberIndex590++) {
    var memberData590 = memberList590[memberIndex590];
    output.append('<li>', soy.$$escapeHtml(memberData590.name), '</li>');
  }
  output.append('</ol></td></tr><tr><td>Your Name</td><td>', soy.$$escapeHtml(opt_data.cs.name), '</td></tr><tr><td>Email:</td><td>', soy.$$escapeHtml(opt_data.cs.email), '</td></tr><tr><td>Coordinator:</td><td>', soy.$$escapeHtml(opt_data.coord_name), '</td></tr><tr><td colspan=\'2\'><a href=\'/app/student/course/list\'>Back</a></td></tr></table><br />');
  return opt_sb ? '' : output.toString();
};

;
// This file was automatically generated from stu_eval_list.soy.
// Please don't edit this file by hand.

if (typeof tm == 'undefined') { var tm = {}; }
if (typeof tm.student == 'undefined') { tm.student = {}; }


tm.student.eval_list = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('<h1>EVALUATIONS</h1><br /><br />');
  tm.flash(null, output, opt_ijData);
  output.append('<h2 align=\'center\'>Pending Evaluations</h2> <br/><table id=\'dataform\' style=\'width: 600px\'><tr><th>COURSE</input></th><th class=\'centeralign\'>Evaluation</th><th class="centeralign">Deadline</th><th class="centeralign">Action</th></tr>');
  var evList607 = opt_data.pending;
  var evListLen607 = evList607.length;
  if (evListLen607 > 0) {
    for (var evIndex607 = 0; evIndex607 < evListLen607; evIndex607++) {
      var evData607 = evList607[evIndex607];
      output.append('<tr><td>', soy.$$escapeHtml(evData607.courseCode), ' - ', soy.$$escapeHtml(evData607.courseName), '</td><td>', soy.$$escapeHtml(evData607.name), '</td><td>', soy.$$escapeHtml(evData607.deadlineR), '</td><td align="center"><a href="/app/student/evaluation/submit?evid=', soy.$$escapeHtml(evData607.id), '">Do</a></td></tr>');
    }
  } else {
    output.append('<tr><td colspan=\'4\'>No pending evaluation</td></tr>');
  }
  output.append('</table><br/><h2 align=\'center\'>Past Evaluations</h2> <br/><table id=\'dataform\' style=\'width: 600px\'><tr><th>COURSE</input></th><th class=\'centeralign\'>Evaluation</th><th class="centeralign">Deadline</th><th class="centeralign">Action</th></tr>');
  var evList623 = opt_data.past;
  var evListLen623 = evList623.length;
  if (evListLen623 > 0) {
    for (var evIndex623 = 0; evIndex623 < evListLen623; evIndex623++) {
      var evData623 = evList623[evIndex623];
      output.append('<tr><td>', soy.$$escapeHtml(evData623.courseCode), ' - ', soy.$$escapeHtml(evData623.courseName), '</td><td>', soy.$$escapeHtml(evData623.name), '</td><td>', soy.$$escapeHtml(evData623.deadlineR), '</td><td align="center">', (evData623.status == 3) ? '<a href="/app/student/evaluation/results?evid=' + soy.$$escapeHtml(evData623.id) + '">View Results</a>' : '', '</td></tr>');
    }
  } else {
    output.append('<tr><td colspan=\'4\'>No past evaluation</td></tr>');
  }
  output.append('</table><br /><br />');
  return opt_sb ? '' : output.toString();
};

;
// This file was automatically generated from stu_eval_results.soy.
// Please don't edit this file by hand.

if (typeof tm == 'undefined') { var tm = {}; }
if (typeof tm.student == 'undefined') { tm.student = {}; }


tm.student.eval_results = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('<h1>EVALUATION RESULTS</h1><br /><br /><table class="headerform"><tbody><tr><td class="fieldname">Course:</td><td>', soy.$$escapeHtml(opt_data.ev.courseDisplay), '</td></tr><tr><td class="fieldname">Evaluation name:</td><td>', soy.$$escapeHtml(opt_data.ev.name), '</td></tr><tr><td class="fieldname">Student Name:</td><td>', soy.$$escapeHtml(opt_data.cs.name), '</td></tr><tr><td class  = "fieldname">Opening time:</td><td>', soy.$$escapeHtml(opt_data.ev.startR), '</td></tr><tr><td class = "fieldname">Closing time:</td><td>', soy.$$escapeHtml(opt_data.ev.deadlineR), '</td></tr></tbody></table><br /><table id="dataform"><tr><th>Feedback from other students</th></tr>');
  var entryList656 = opt_data.entries;
  var entryListLen656 = entryList656.length;
  for (var entryIndex656 = 0; entryIndex656 < entryListLen656; entryIndex656++) {
    var entryData656 = entryList656[entryIndex656];
    output.append((entryData656.content != '') ? '<tr><td>' + soy.$$escapeHtml(entryData656.content) + '</td></tr>' : '');
  }
  output.append('</table><br/> <br/><div align=\'center\'><a href=\'/app/student/evaluation\' class=\'button\'>Back</a></div>');
  return opt_sb ? '' : output.toString();
};

;
// This file was automatically generated from stu_eval_submit.soy.
// Please don't edit this file by hand.

if (typeof tm == 'undefined') { var tm = {}; }
if (typeof tm.student == 'undefined') { tm.student = {}; }


tm.student.eval_submit = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('<h1>SUBMIT EVALUATION</h1><br /><br /><form method="post"><table class="headerform"><tbody><tr><td class="fieldname">Course ID:</td><td>', soy.$$escapeHtml(opt_data.ev.courseDisplay), '</td></tr><tr><td class="fieldname">Evaluation name:</td><td>', soy.$$escapeHtml(opt_data.ev.name), '</td></tr><tr><td class  = "fieldname">Opening time:</td><td>', soy.$$escapeHtml(opt_data.ev.startR), '</td></tr><tr><td class = "fieldname">Closing time:</td><td>', soy.$$escapeHtml(opt_data.ev.deadlineR), '</td></tr><tr><td class="fieldname">Instructions:</td><td>', soy.$$escapeHtml(opt_data.ev.instructions), '</td></tr></tbody></table><br /><br /><form id="form_submit_eval"><table class="headerform"><tbody><tr><td class="reportheader" colspan="2">Self evaluation in team: <strong>', soy.$$escapeHtml(opt_data.teamName), '</strong></td></tr><tr><td class="lhs">Comments about yourself:</td><td><textarea type="text" style=\'width: 400px; height: 80px\' name="content_', soy.$$escapeHtml(opt_data.self.id), '">', soy.$$escapeHtml(opt_data.self.content), '</textarea></td></tr>');
  var eList683 = opt_data.entries;
  var eListLen683 = eList683.length;
  for (var eIndex683 = 0; eIndex683 < eListLen683; eIndex683++) {
    var eData683 = eList683[eIndex683];
    output.append('<tr><td class="reportheader" colspan="2">Evaluation for ', soy.$$escapeHtml(eData683.toStudentName), '</td></tr><tr><td class="lhs">Comments about this teammate:</td><td><textarea class="" type="text" style=\'width: 400px; height: 80px\' name="content_', soy.$$escapeHtml(eData683.id), '">', soy.$$escapeHtml(eData683.content), '</textarea></td></tr>');
  }
  output.append('<tr><td colspan=\'2\'><div align=\'center\'><button type=\'submit\'>Submit</button></div></td></tr></tbody></table></form>');
  return opt_sb ? '' : output.toString();
};

;
// This file was automatically generated from stu_results.soy.
// Please don't edit this file by hand.

if (typeof tm == 'undefined') { var tm = {}; }
if (typeof tm.student == 'undefined') { tm.student = {}; }


tm.student.results = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('<div class=\'form-wrapper shadow white-bg center\' style=\'width: 700px\'><div class="header_box"><h1>Results of Peer-evaluation Exercise</h1><div class=\'subheader\'>', soy.$$escapeHtml(opt_data.ev.courseDisplay), '</div></div><br /><p>The results of your evaluation are now ready.</p><table class=\'table\'><tbody><tr><td class="fieldname">Evaluation:</td><td>', soy.$$escapeHtml(opt_data.ev.name), '</td></tr><tr><td class="fieldname">Student Name:</td><td>', soy.$$escapeHtml(opt_data.cs.name), '</td></tr><tr><td class  = "fieldname">Opening time:</td><td>', soy.$$escapeHtml(opt_data.ev.startR), '</td></tr><tr><td class = "fieldname">Closing time:</td><td>', soy.$$escapeHtml(opt_data.ev.deadlineR), '</td></tr></tbody></table><br /><p>Here are what your teammates wrote about you:</p><div id=\'results\'>');
  var entryList706 = opt_data.entries;
  var entryListLen706 = entryList706.length;
  for (var entryIndex706 = 0; entryIndex706 < entryListLen706; entryIndex706++) {
    var entryData706 = entryList706[entryIndex706];
    output.append((entryData706.content != '') ? '<div class=\'row\'>' + soy.$$escapeHtml(entryData706.content) + '</div></td></tr>' : '');
  }
  output.append('</div><br/> <br/></div>');
  return opt_sb ? '' : output.toString();
};

;
// This file was automatically generated from stu_submit.soy.
// Please don't edit this file by hand.

if (typeof tm == 'undefined') { var tm = {}; }
if (typeof tm.student == 'undefined') { tm.student = {}; }


tm.student.submit = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('<div id=\'submit-main\' class=\'form-wrapper shadow\'><div style=\'width: 150px; display: block; float: right\'>Time Left: <br/><span class=\'countdown\'></span></div><div class=\'header_box\'><h1 style=\'font-size: 20px\'>Teammates Peer-evaluation Exercise</h1><div class=\'subheader\' style=\'font-size: 16px\'>', soy.$$escapeHtml(opt_data.cs.course.code), ' - ', soy.$$escapeHtml(opt_data.cs.course.name), (opt_data.cs.course.term != '') ? '<span class=\'muted\'> (' + soy.$$escapeHtml(opt_data.cs.course.term) + ')</span>' : '', '</div></div><br/><div class=\'alert alert-info\'>', soy.$$escapeHtml(opt_data.cs.name), ', <br/><br/>Your course coordinator invited you to evaluate your teammates in module ', soy.$$escapeHtml(opt_data.cs.course.code), '. <br/><div class=\'note\'>', soy.$$escapeHtml(opt_data.ev.instructions), '</div></div><div class=\'info form-wrapper\' style=\'display: none\'><table><tr><td class=\'label\'>Evaluation:</td><td>', soy.$$escapeHtml(opt_data.ev.name), '</td></tr><tr><td class=\'label\'>Deadline:</td><td>', soy.$$escapeHtml(opt_data.ev.deadlineR), ' (<span class=\'countdown\'></span>)</td></tr></table></div><div style=\'line-height: 140%\'><strong>Team:</strong> ', soy.$$escapeHtml(opt_data.cs.team.name), ' <br/><ul>');
  var memberList738 = opt_data.members;
  var memberListLen738 = memberList738.length;
  for (var memberIndex738 = 0; memberIndex738 < memberListLen738; memberIndex738++) {
    var memberData738 = memberList738[memberIndex738];
    output.append('<li>', soy.$$escapeHtml(memberData738.name), ' ', (opt_data.cs.id == memberData738.id) ? '(you)' : '', '</li>');
  }
  output.append('</ul></div><br/><strong>Deadline: </strong> <br/>', soy.$$escapeHtml(opt_data.ev.deadlineR), '<br/><br/><div class=\'\'><form class="form-horizontal" method="post"><fieldset><legend>Please provide feedback for your team members</legend>');
  var eList757 = opt_data.entries;
  var eListLen757 = eList757.length;
  for (var eIndex757 = 0; eIndex757 < eListLen757; eIndex757++) {
    var eData757 = eList757[eIndex757];
    output.append('<div class="control-group"><label class="control-label">', soy.$$escapeHtml(eData757.toStudentName), '</label><div class="controls"><textarea class="" type="text" style=\'width: 350px; height: 80px\' name="content_', soy.$$escapeHtml(eData757.id), '">', soy.$$escapeHtml(eData757.content), '</textarea></div></div>');
  }
  output.append('<div class="form-actions"><button type=\'submit\' class=\'btn btn-success\'>Submit</button> &nbsp;<!--\t\t\t\t<button type=\'submit\' class=\'btn btn\'>Save as Draft</button> --></div></fieldset></form></div></div><!--<div id=\'connect_wrapper\' class=\'form-wrapper shadow\' style=\'background-color: #fff; width: 300px\'><h3 style=\'margin-bottom: 5px\'>Connect with your Google Account</h3><p>To manage your exercises easily</p><br/><p><a href=\'#\'><img class=\'google_connect\' src=\'/img/google_connect_button.png\' /></a></p><div class=\'clearfix\'></div></div>--><script>var deadline_mils = ', soy.$$escapeHtml(opt_data.deadline_mils), ';\n\t$(function() {\n\t\tvar offset = new Date().getTimezoneOffset();\n\t\tvar d = new Date(deadline_mils);\n\t\t// Minus to timezone.\n\t\td.setMinutes(d.getMinutes() - offset);\n\t\t$(".countdown").countdown({\n\t\t\tuntil: d,\n\t\t\tcompact: true,\n\t\t\tformat: \'DHMS\'\n\t\t});\n\t});\n\t<\/script>');
  return opt_sb ? '' : output.toString();
};

;
// This file was automatically generated from 404.soy.
// Please don't edit this file by hand.

if (typeof teammates == 'undefined') { var teammates = {}; }


teammates.e404 = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('Page not found!');
  return opt_sb ? '' : output.toString();
};

;
// This file was automatically generated from admin.soy.
// Please don't edit this file by hand.

if (typeof tm == 'undefined') { var tm = {}; }
if (typeof tm.admin == 'undefined') { tm.admin = {}; }


tm.admin.index = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('<h1>Administrator CP</h1>');
  tm.flash(null, output, opt_ijData);
  output.append('<h1>Coordinator List</h1><br/><table style=\'width: 500px\'><tr><th>Email</th><th>Coordinator</th><th> </th></tr>');
  var aList777 = opt_data.coords;
  var aListLen777 = aList777.length;
  for (var aIndex777 = 0; aIndex777 < aListLen777; aIndex777++) {
    var aData777 = aList777[aIndex777];
    output.append('<tr><td>', soy.$$escapeHtml(aData777.email), '</td><td><a href=\'/app/admin/remove_coordinator?cid=', soy.$$escapeHtml(aData777.id), '\'>Remove</button></td></tr>');
  }
  output.append('</table><br/><br/><div class=\'form-wrapper shadow center\' style=\'width: 400px\'><form method="post" action="/app/admin/make_coordinator"><h3>Make Coordinator</h3><br/><div><label>Email address: <br/><input type="text" name="email" size="25" /></label></div><div><button type="submit">Make Coordinator</button></div></form>');
  return opt_sb ? '' : output.toString();
};

;
// This file was automatically generated from email.soy.
// Please don't edit this file by hand.

if (typeof tm == 'undefined') { var tm = {}; }
if (typeof tm.email == 'undefined') { tm.email = {}; }


tm.email.invite_old = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('Dear ', soy.$$escapeHtml(opt_data.name), ',\n\nThe course ', soy.$$escapeHtml(opt_data.course), ' will be using Teammates Peer-Evaluation System for peer-evaluations. The coordinator has invited you to use the system to evaluate your team members. These are the steps to follow.\n\nLogin to the system:\n', soy.$$escapeHtml(opt_data.app_url), '\n\n* Login as "Student" using your Google ID. If you do not have a Google ID, please create one.\n\nJoin the course:\n* Enter this key to join ', soy.$$escapeHtml(opt_data.course), ': ', soy.$$escapeHtml(opt_data.key), '\n\n* Now, ', soy.$$escapeHtml(opt_data.course), ' should appear in the course list and you can see names of your teammates by clicking the View link corresponding to the course.\n\nSubmit pending evaluations:\n* Click "Evaluations" button at the top to check if there are any pending peer-evaluations.\n\nIn case of problems:\nIf team details are not correct, please contact us at ', soy.$$escapeHtml(opt_data.contact_email), '\n');
  return opt_sb ? '' : output.toString();
};


tm.email.informEvaluation_old = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('Dear ', soy.$$escapeHtml(opt_data.student_name), ',\n\nThe following evaluation is now open:\n\n', soy.$$escapeHtml(opt_data.course_display), ' - ', soy.$$escapeHtml(opt_data.eval_name), '\n\nPlease go to the site and complete your evaluation:\n\n', soy.$$escapeHtml(opt_data.app_url), '\n\nIn case of problems, please contact us at ', soy.$$escapeHtml(opt_data.contact_email));
  return opt_sb ? '' : output.toString();
};


tm.email.submit = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('Dear ', soy.$$escapeHtml(opt_data.student_name), ',\n\nYou receive this because you are taking the course: ', soy.$$escapeHtml(opt_data.course_display), '. \nYour course coordinator has invited you to evaluate your teammates. The evaluation name: ', soy.$$escapeHtml(opt_data.eval_name), '\n\nPlease follow the link below to submit your evaluation:\n\n', soy.$$escapeHtml(opt_data.url), '\n\nDo not share this link with anyone - it is a unique and private link generated for you only.\n\nIn case of problems, please contact us at ', soy.$$escapeHtml(opt_data.contact_email));
  return opt_sb ? '' : output.toString();
};


tm.email.results = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('Dear ', soy.$$escapeHtml(opt_data.student_name), ',\n\nThe evaluation results from your team are now ready.\nEvaluation: ', soy.$$escapeHtml(opt_data.eval_name), '\nCourse: ', soy.$$escapeHtml(opt_data.course_display), '\n\nFollow the link below to view: \n\n', soy.$$escapeHtml(opt_data.result_url), '\n\nDo not share this link with anyone - it is a unique and private link generated for you only.\n\nRegards,\n\nTeammates System.\n\n');
  return opt_sb ? '' : output.toString();
};


tm.email.evalPublished = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('Dear ', soy.$$escapeHtml(opt_data.student_name), ',\n\nThe results of the following evaluation is now ready:\n\nEvaluation: ', soy.$$escapeHtml(opt_data.eval_name), '\nCourse: ', soy.$$escapeHtml(opt_data.course_display), '\nClick here to view the results:\n\n', soy.$$escapeHtml(opt_data.result_url), '\n\nRegards,\n\nTeammates System.\n\n');
  return opt_sb ? '' : output.toString();
};

;
// This file was automatically generated from footer.soy.
// Please don't edit this file by hand.

if (typeof tm == 'undefined') { var tm = {}; }


tm.footer = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('<div id="footer-wrapper"><span>[TEAMMATES Version v0.1] <a class="footer" href="http://www.comp.nus.edu.sg/~teams/contact.html" target="_blank">Contact Us</a>&nbsp; &nbsp;</span></div>');
  return opt_sb ? '' : output.toString();
};

;
// This file was automatically generated from html.soy.
// Please don't edit this file by hand.

if (typeof tm == 'undefined') { var tm = {}; }


tm.requireJs = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('<script language="JavaScript" src="/js/jquery-1.7.1.min.js"><\/script><script language="JavaScript" src="/js/facebox.js"><\/script><script language="JavaScript" src="/js/bootstrap.min.js"><\/script><script src="/js/jquery.tools.form.min.js"><\/script><script src="/js/jquery.tools.tiny.min.js"><\/script><!-- <script src="/js/jquery.validate.min.js"><\/script> --><script language="JavaScript" src="/js/jquery.tipsy.js"><\/script><script language="JavaScript" src="/js/jquery-ui-1.8.18.custom.min.js"><\/script><!-- JqueryUI included: autocomplete  --><script language="JavaScript" src="/js/jquery.countdown.min.js"><\/script><script language="JavaScript" src="/js/soyutils.js"><\/script><script language="JavaScript" src="/js/templates.js"><\/script><script language="JavaScript" src="/js/common2.js"><\/script><script language="JavaScript" src="/js/app.js"><\/script>');
  var jsList854 = opt_ijData.jscripts;
  var jsListLen854 = jsList854.length;
  for (var jsIndex854 = 0; jsIndex854 < jsListLen854; jsIndex854++) {
    var jsData854 = jsList854[jsIndex854];
    output.append('<script language="JavaScript" src="', soy.$$escapeHtml(jsData854), '"><\/script>');
  }
  return opt_sb ? '' : output.toString();
};


tm.requireCss = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('\t<link rel=stylesheet href="/css/bootstrap.css" type="text/css" /><link rel=stylesheet href="/css/form.css" type="text/css" /><link rel=stylesheet href="/css/globals.css" type="text/css" /><link rel=\'stylesheet\' href="/css/main.css" type="text/css" /><link rel=stylesheet href="/css/tipsy.css" type="text/css" /><link rel="stylesheet" href="/css/jquerytools.dateinput.css" type="text/css"><link rel="stylesheet" href="/css/facebox.css" type="text/css">');
  var cssList861 = opt_ijData.stylesheets;
  var cssListLen861 = cssList861.length;
  for (var cssIndex861 = 0; cssIndex861 < cssListLen861; cssIndex861++) {
    var cssData861 = cssList861[cssIndex861];
    output.append('<link rel=\'stylesheet\' href="', soy.$$escapeHtml(cssData861), '" type="text/css" />');
  }
  output.append('<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements --><!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"><\/script><![endif]-->');
  return opt_sb ? '' : output.toString();
};


tm.flash = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append((opt_ijData.flash) ? '<div id=\'flash\' class=\'alert fade in alert-' + soy.$$escapeHtml(opt_ijData.flash.status) + '\' style=\'display: block\'><a class="close" data-dismiss="alert" href="#">&times;</a>' + soy.$$escapeHtml(opt_ijData.flash.message) + '</div>' : '<div id=\'flash\'></div>');
  return opt_sb ? '' : output.toString();
};


tm.loading = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('<span class=\'loading\'>Loading...</span>');
  return opt_sb ? '' : output.toString();
};


tm.error = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('\t<div class=\'error white-bg form-wrapper shadow center\' style=\'width: 300px\'>', soy.$$escapeHtml(opt_data.message), '</div>');
  return opt_sb ? '' : output.toString();
};


tm.success = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('\t<br/><br/><div class=\'alert alert-block alert-success center\' style=\'width: 300px\'>', soy.$$escapeHtml(opt_data.message), (opt_data.next != null) ? '<br/> <br/><a href=\'' + soy.$$escapeHtml(opt_data.next) + '\'>Continue</a>' : '', '</div>');
  return opt_sb ? '' : output.toString();
};

;
// This file was automatically generated from master_admin.soy.
// Please don't edit this file by hand.

if (typeof tm == 'undefined') { var tm = {}; }


tm.master_admin = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append(opt_data.bodyHtml);
  return opt_sb ? '' : output.toString();
};

;
// This file was automatically generated from master_coordinator.soy.
// Please don't edit this file by hand.

if (typeof tm == 'undefined') { var tm = {}; }


tm.master_coordinator = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('<!DOCTYPE html><html><head><link rel="shortcut icon" href="/favicon.png" /><title>', opt_ijData.title, '</title>');
  tm.requireCss(null, output, opt_ijData);
  tm.requireJs(null, output, opt_ijData);
  output.append('<script language="JavaScript" src="/js/coordinator2.js"><\/script></head><body><div id="header-wrapper"><div id="header" class="container"><a href="/"><img id="logo" width=\'150px\' height=\'47px\' src="/images/teammateslogo.jpg" /></a><div id=\'topnav\' class=\'right\'><a class=\'navigation right\' href=\'/app/account/logout\'>Logout</a><a class=\'t_courses navigation right\' href=\'/app/coordinator/course\'><strong>Coordinator Home</strong></a></div></div></div><div id=\'page-wrapper\'>', opt_data.bodyHtml, '</div></div><div id="footer-wrapper"><span>[TEAMMATES Version v0.1] <a class="footer" href="http://www.comp.nus.edu.sg/~teams/contact.html" target="_blank">Contact Us</a>&nbsp; &nbsp;</span></div></body></html>');
  return opt_sb ? '' : output.toString();
};

;
// This file was automatically generated from master_default.soy.
// Please don't edit this file by hand.

if (typeof tm == 'undefined') { var tm = {}; }


tm.master_default = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('<!DOCTYPE html><html><head><link rel="shortcut icon" href="/favicon.png" /><title>', opt_ijData.title, '</title>');
  tm.requireCss(null, output, opt_ijData);
  tm.requireJs(null, output, opt_ijData);
  output.append('</head><body><div id="header-wrapper"><div id="header" class="container"><a href="/"><img id="logo" width=\'150px\' height=\'47px\' src="/images/teammateslogo.jpg" /></a></div></div><div id=\'page-wrapper\'>', opt_data.bodyHtml, '</div></div><div id="footer-wrapper"><span>[TEAMMATES Version v0.1] <a class="footer" href="http://www.comp.nus.edu.sg/~teams/contact.html" target="_blank">Contact Us</a>&nbsp; &nbsp;</span></div></body></html>');
  return opt_sb ? '' : output.toString();
};

;
// This file was automatically generated from master_none.soy.
// Please don't edit this file by hand.

if (typeof tm == 'undefined') { var tm = {}; }


tm.master_none = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append(opt_data.bodyHtml);
  return opt_sb ? '' : output.toString();
};

;
// This file was automatically generated from master_student.soy.
// Please don't edit this file by hand.

if (typeof tm == 'undefined') { var tm = {}; }


tm.master_student = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('<!DOCTYPE html><html><head><link rel="shortcut icon" href="/favicon.png" /><title>', opt_ijData.title, '</title>');
  tm.requireCss(null, output, opt_ijData);
  tm.requireJs(null, output, opt_ijData);
  output.append('<script language="JavaScript" src="/js/student2.js"><\/script></head><body><div id="header-wrapper"><div id="header" class="container"><a href="/"><img id="logo" width=\'150px\' height=\'47px\' src="/images/teammateslogo.jpg" /></a><div id=\'topnav\' class=\'right\'><a class=\'navigation right\' href=\'/app/account/logout\'>Logout</a><a class=\'t_evaluations navigation right\' href=\'/app/student/evaluation\'><strong>Evaluations</strong></a><a class=\'t_courses navigation right\' href=\'/app/student/course\'><strong>Courses</strong></a></div></div></div><div id=\'page-wrapper\'>', opt_data.bodyHtml, '</div><div id="footer-wrapper"><span>[TEAMMATES Version v0.1] <a class="footer" href="http://www.comp.nus.edu.sg/~teams/contact.html" target="_blank">Contact Us</a>&nbsp; &nbsp;</span></div></body></html>');
  return opt_sb ? '' : output.toString();
};

;
// This file was automatically generated from misc.soy.
// Please don't edit this file by hand.

if (typeof tm == 'undefined') { var tm = {}; }


tm.none = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  return opt_sb ? '' : output.toString();
};


tm.printYesNo = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append((opt_data.val == true) ? 'Yes' : 'No');
  return opt_sb ? '' : output.toString();
};

;
// This file was automatically generated from page_home.soy.
// Please don't edit this file by hand.

if (typeof tm == 'undefined') { var tm = {}; }


tm.home = function(opt_data, opt_sb, opt_ijData) {
  var output = opt_sb || new soy.StringBuilder();
  output.append('<!DOCTYPE html><html><head><link rel="shortcut icon" href="/favicon.png" /><title>Teammates - Online Peer Feedback System for Student Team Projects</title><link rel=stylesheet href="/css/bootstrap.css" type="text/css" /><link rel=stylesheet href="/css/form.css" type="text/css" /><link rel=stylesheet href="/css/globals.css" type="text/css" /><link rel=\'stylesheet\' href="/css/main.css" type="text/css" /><link rel=\'stylesheet\' href="/css/home.css" type="text/css" /></head><body><div id="header-wrapper"><div id="header" class=\'container\'><a href="/"><img id="logo" width=\'150px\' height=\'47px\' src="/images/teammateslogo.jpg" /></a></div></div><div id=\'page-wrapper\' style=\'padding-top: 5px\'><div style=\'margin: 0 auto;\'><div id="mainContent"> <!-- main --><div id="caption"><div id="captionImage"><img alt="Teammates logo" src="/img/splash/teammateslogo-black.png"></div><!-- end captionImage --><div id="captionText"><span>An online peer feedback tool for improving teamwork in student projects</span></div><!-- end captionText --></div><!-- end caption --><div style="margin: 0 auto; padding-bottom: 5px;"><img alt="Overview of Teammates" src="/img/splash/overview.png"></div><!-- end div --><div style="margin: 0 auto; margin-bottom: 10px;"><a href="http://www.comp.nus.edu.sg/~teams/request.html"><img alt="Request account" src="/img/splash/free-easy-request.png" style="border: 0;"></a></div><!-- end div --><div style=\'display: block; width: 100%\'><a href=\'/app/c\' class=\'btn btn-primary\' style=\'float: right;\'>Teacher Login</a></div></div></div></div><div id="footer-wrapper"><span>[TEAMMATES Version v0.1] <a class="footer" href="http://www.comp.nus.edu.sg/~teams/contact.html" target="_blank">Contact Us</a>&nbsp; &nbsp;</span></div></body></html>');
  return opt_sb ? '' : output.toString();
};
