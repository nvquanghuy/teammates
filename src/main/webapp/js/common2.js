function formError(form, message) {
	var div_message = $('.form-message');
	div_message.show().addClass('error').html(message);
}

function printMsg(st) {
	alert(st.message);
}


/**
 * Turn the form inputs into an JS array
 * @return JS array
 */
function parseFormInputs(jform) {
	var values = {};
	$.each(jform.serializeArray(), function(i, field) {
		values[field.name] = field.value;
	});
	return values;
}

function isAddEvaluationScheduleValid(start, startTime, deadline, deadlineTime) {
	var start = convertDateFromDDMMYYYYToMMDDYYYY(start);
	var deadline = convertDateFromDDMMYYYYToMMDDYYYY(deadline);
	
	var now = new Date();
	
	start = new Date(start);
	deadline = new Date(deadline);
	
	if(startTime != "24")
	{
		start.setHours(parseInt(startTime));
	}
	
	else
	{
		start.setHours(23);
		start.setMinutes(59);
	}
	
	if(deadlineTime != "24")
	{
		deadline.setHours(parseInt(deadlineTime));
	}
	
	else
	{
		deadline.setHours(23);
		deadline.setMinutes(59);
	}
	
	if(start > deadline)
	{
		return false;
	}
	
	else if(now > start)
	{
		return false;
	}

	else if(!(start > deadline || deadline > start)) 
	{
		if(parseInt(startTime) >= parseInt(deadlineTime))
		{
			return false;
		}
	}
	
	return true;
}

function isCourseIDValid(courseID)
{
	if(courseID.indexOf("\\") >= 0 || courseID.indexOf("'") >= 0 || courseID.indexOf("\"") >= 0)
	{
		return false;
	}
	
	if(courseID.match(/^[a-zA-Z0-9.-]*$/) == null)
	{
		return false;
	}
	
	if(courseID.length > 21)
	{
		return false;
	}
	
	return true;

}

function isCourseNameValid(courseName)
{
	if(courseName.length > 38)
	{
		return false;
	}
	
	return true;

}

function isEditEvaluationScheduleValid(start, startTime, deadline, deadlineTime, timeZone, activated, status)
{
	var startString = convertDateFromDDMMYYYYToMMDDYYYY(start);
	var deadlineString = convertDateFromDDMMYYYYToMMDDYYYY(deadline);
	
	var now = getDateWithTimeZoneOffset(timeZone);

	start = new Date(startString);
	deadline = new Date(deadlineString);

	if(startTime != "24")
	{
		start.setHours(parseInt(startTime));
	}
	
	else
	{
		start.setHours(23);
		start.setMinutes(59);
	}
	
	if(deadlineTime != "24")
	{
		deadline.setHours(parseInt(deadlineTime));
	}
	
	else
	{
		deadline.setHours(23);
		deadline.setMinutes(59);
	}
	
	if(start > deadline)
	{
		return false;
	}
	
	else if(status == "AWAITING"){
		// Open evaluation should be done by system only.
		// Thus, coordinator cannot change evaluation ststus from AWAITING to
		// OPEN
		if(start < now){
			return false;
		}
	}
	
// else if(now > deadline)
// {
// return false;
// }
//
// else if(!(start > deadline || deadline > start))
// {
// if(parseInt(startTime) >= parseInt(deadlineTime))
// {
// return false;
// }
// }
//	
// else if(!activated && start < now)
// {
// return false;
// }
//	
	return true;
}

function isEditStudentInputValid(editName, editTeamName, editEmail, editGoogleID)
{
	if(editName == "" || editTeamName == "" || editEmail == "")
	{
		return false;
	}
	
	if(!isStudentNameValid(editName))
	{
		return false;
	}
	
	else if(!isStudentEmailValid(editEmail))
	{
		return false;
	}
	
	else if(!isStudentTeamNameValid(editTeamName))
	{
		return false;
	}
	
	return true;
}

function isEnrollmentInputValid(input)
{
	var entries = input.split("\n");
	var fields;
	
	for(var x = 0; x < entries.length; x++)
	{
		if(entries[x] != ""){
			// Separate the fields
			fields = entries[x].split("\t");
			
			// Make sure that all fields are present
			if(fields.length < 3)
			{
				return false;
			}
			
			else if(fields.length > 4)
			{
				return false;
			}
			
			// Check that fields are correct
			if(!isStudentNameValid(trim(fields[1])))
			{
				return false;
			}
			
			else if(!isStudentEmailValid(trim(fields[2])))
			{
				return false;
			}
			
			else if(!isStudentTeamNameValid(trim(fields[0])))
			{
				return false;
			}
		}
	}
	
	return true;
}

function isEvaluationNameValid(name)
{
	if(name.indexOf("\\") >= 0 || name.indexOf("'") >= 0 || name.indexOf("\"") >= 0)
	{
		return false;
	}
	
	if(name.match(/^[a-zA-Z0-9 ]*$/) == null)
	{
		return false;
	}
	
	if(name.length > 22)
	{
		return false;
	}
	
	return true;
}

function isStudentEmailValid(email)
{
	if(email.match(/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i) != null && email.length <= 40)
	{
		return true;
	}

	return false;
}

function isStudentNameValid(name)
{
	if(name.indexOf("\\") >= 0 || name.indexOf("'") >= 0 || name.indexOf("\"") >= 0)
	{
		return false;
	}
	
	else if(name.match(/^.[^\t]*$/) == null)
	{
		return false;
	}
	
	else if(name.length > 40)
	{
		return false;
	}
	
	return true;
}

function isStudentTeamNameValid(teamName) {
	if(teamName.length > 24) {
		return false;
	}

	return true;
}