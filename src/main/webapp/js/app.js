var SUCCESS = 'success';
var LOADING = 'Loading...';

$(function() {
	$("input:date").dateinput({
		format: "dd/mm/yyyy",
    min: -1
	});

	$('input[title], a[title], select[title], textarea[title]').tipsy({html: true, gravity: 's'});
	
	$('#enrol_text').keydown(function (e) {
		if (e.which == 9) {
			$(this).val( $(this).val() + '\t' );
			e.preventDefault();
		}
	});
	
  $('a[rel*=facebox]').facebox({
    loadingImage : '/img/facebox/loading.gif',
    closeImage   : '/img/facebox/closelabel.png'
  });

	/*$(".hover-container").hover(
		function() { $(this).children('.hover-item').show(); } ,
		function() { $(this).children('.hover-item').hide(); }
		); */

  $(".modal").modal( {
    show: false
  } );

	$(".alert").alert();

});

function flash(data) {
	$('#flash').css('display', 'block').removeClass().addClass(data.status).html(data.message);
}

function flashOff() {
	$("#flash").css('display', 'none');
}

function loadingStart(form) {
	flashOff();
	$('.loading', form).show();
}

function loadingEnd(form) {
	$('.loading', form).hide();
}

function getParameterByName(name) {
	var match = RegExp('[?&]' + name + '=([^&]*)') .exec(window.location.search);
  return match ?  decodeURIComponent(match[1].replace(/\+/g, ' ')) : null;
}
