package teammates;

public class Lang {
	public static final String MSG_COURSE_ADDED = "Course added. Select 'Enrol' to start adding students for this course";
	public static final String MSG_COURSE_EXISTS = "Course Code already exists.";
	public static final String MSG_COURSE_NOTEAMS = "Course has no teams";
	public static final String MSG_EVALUATION_ADDED = "Evaluation added";
	public static final String MSG_EVALUATION_DEADLINEPASSED = "Evaluation deadline passed";
	public static final String MSG_EVALUATION_EDITED = "Evaluation edited";
	public static final String MSG_EVALUATION_EXISTS = "Evaluation exists";
	public static final String MSG_EVALUATION_UNABLETOCHANGETEAMS = "evaluation ongoing unable to change teams";
	public static final String MSG_EVALUATION_REMAINED = "evaluation remained";
	public static final String MSG_STATUS_OPENING = "<status>";
	public static final String MSG_STATUS_CLOSING = "</status>";
	public static final String MSG_STUDENT_COURSEJOINED = "course joined";
	public static final String MSG_STUDENT_GOOGLEIDEXISTSINCOURSE = "googleid exists in course";
	public static final String MSG_STUDENT_REGISTRATIONKEYINVALID = "registration key invalid";
	public static final String MSG_STUDENT_REGISTRATIONKEYTAKEN = "registration key taken";
	public static final String COURSE_NO_PERMISSION = "You don't have permission to access this course.";
	public static final String COURSE_NOT_FOUND = "Course not found";
	public static final String COURSE_DELETED = "Course deleted";
	public static final String INVALID_COURSE = "Invalid course";
}
