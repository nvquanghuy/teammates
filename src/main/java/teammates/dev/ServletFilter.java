package teammates.dev;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * Servlet Filter
 * 
 * @author huy
 * 
 */
public final class ServletFilter implements javax.servlet.Filter {

	@Override
	public void init(FilterConfig config) {
	}

	public void doFilter(ServletRequest request, ServletResponse response,
		FilterChain chain) throws IOException, ServletException {
		try {
			chain.doFilter(request, response);
		} finally {
			RequestSession.finishRequest();
		}
	}

	@Override
	public void destroy() {
	}
}