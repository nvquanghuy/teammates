package teammates.dev;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import teammates.config.Config;
import teammates.pojo.Course;
import teammates.pojo.CourseStudent;
import teammates.pojo.Evaluation;
import teammates.pojo.Submit;
import teammates.template.ViewMap;

import com.google.template.soy.SoyFileSet;
import com.google.template.soy.SoyFileSet.Builder;
import com.google.template.soy.tofu.SoyTofu;
import com.google.template.soy.tofu.SoyTofu.Renderer;

public class Emails {
	private static Properties props;

	static {
		props = new Properties();
	}

	public static void publishEvaluation(Evaluation ev, List<CourseStudent> cstudents) {
		try {
			Session session = Session.getDefaultInstance(props, null);

			Course course = ev.loadCourse();

			for (CourseStudent cs : cstudents) {
				MimeMessage message = new MimeMessage(session);

				message.addRecipient(
					Message.RecipientType.TO,
					new InternetAddress(cs.getEmail())
					);

				message.setFrom(new InternetAddress(Config.TEAMMATES_APP_EMAIL));

				Submit submit = Submit.byEvalAndCourseStudent(ev.getId(), cs.getId());

				String resultUrl = Config.TEAMMATES_APP_URL + "/app/s/results?evid="
					+ ev.getId() + "&csid=" + cs.getId() + "&s=" + submit.getSecret();

				cs.loadCourse();
				message.setSubject(String.format("Teammates - Results of Peer-Evaluation Exercise - "
					+ course.getCode()));
				ViewMap vm = new ViewMap();
				vm.put("student_name", cs.getName());
				vm.put("eval_name", ev.getName());
				vm.put("course_display", course.getCode() + " - " + course.getName());
				vm.put("result_url", resultUrl);

				message.setText(renderEmailTemplate("results", vm));

				Transport.send(message);
			}
		}

		catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Send email reminders to un-submitted students about this evaluation
	 * 
	 */
	public static void remindEvaluation(Evaluation ev, List<CourseStudent> cstudents) {
		// It will just be like informStudentsEvaluation, except only send for those who
		// haven't submitted.
		informStudentsEvaluation(ev, cstudents);
	}

	public static void informStudentsEvaluation(Evaluation ev, List<CourseStudent> cstudents) {
		Course course = Course.get(ev.getCourseKey().getId());
//		List<CourseStudent> cstudents = CourseStudent.listFromCourse(course.getId());

		try {
			Session session = Session.getDefaultInstance(props, null);

			for (CourseStudent cs : cstudents) {
				MimeMessage message = new MimeMessage(session);
				message.setFrom(new InternetAddress(Config.TEAMMATES_APP_EMAIL));

				message.addRecipient(
					Message.RecipientType.TO,
					new InternetAddress(cs.getEmail()));

				Submit submit = Submit.byEvalAndCourseStudent(ev.getId(), cs.getId());

				String subject = String.format("Invitation to Peer-evaluation Execise - %s", course.getCode());
				String url = Config.TEAMMATES_APP_URL + "/app/s/submit?evid=" + ev.getId() + "&csid="
					+ cs.getId() + "&s=" + submit.getSecret();

				message.setSubject(subject);

				ViewMap vm = new ViewMap();
				vm.put("student_name", cs.getName());
				vm.put("course_display", course.getCode() + " - " + course.getName());
				vm.put("eval_name", ev.getName());
				vm.put("url", url);
				vm.put("contact_email", Config.TEAMMATES_APP_EMAIL);

				message.setText(renderEmailTemplate("submit", vm));

				Transport.send(message);
			}
		}

		catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	/*
	 * public static void remindStudent(CourseStudent cs) { try { Session session
	 * = Session.getDefaultInstance(props, null); MimeMessage message = new
	 * MimeMessage(session);
	 * 
	 * message.addRecipient( Message.RecipientType.TO, new
	 * InternetAddress(cs.getEmail()) ); message.setFrom(new
	 * InternetAddress("nvquanghuy@gmail.com"));
	 * 
	 * message.setSubject(String.format("Invitation to Teammates")); ViewMap vm =
	 * new ViewMap(); vm.put("name", cs.getName()); vm.put("course",
	 * cs.getCourse().getCode() + " - " + cs.getCourse().getName());
	 * vm.put("app_url", Config.TEAMMATES_APP_URL); vm.put("key", cs.getRegKey());
	 * vm.put("contact_email", "nvquanghuy@gmail.com");
	 * 
	 * message.setText(renderEmailTemplate("invite", vm));
	 * 
	 * Transport.send(message); }
	 * 
	 * catch (MessagingException e) { e.printStackTrace(); } }
	 */

	private static String renderEmailTemplate(String templateName, ViewMap vm) {
		Builder b = new SoyFileSet.Builder();
		b.add(new File("WEB-INF/templates/email.soy"));
		try {
			b.setCompileTimeGlobals(new File("WEB-INF/templates/globals.txt"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		SoyFileSet sfs = b.build();

		// Compile the template into a SoyTofu object.
		// SoyTofu's newRenderer method returns an object that can render any
		// template in file set.
		SoyTofu tofu = sfs.compileToTofu();

		Renderer renderer = tofu.newRenderer("tm.email." + templateName).setData(vm);
		return renderer.render();
	}

}
