package teammates.dev;

import java.util.ArrayList;
import java.util.List;

import teammates.pojo.Account;
import teammates.pojo.Coordinator;
import teammates.pojo.Student;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.googlecode.objectify.Key;

/**
 * Represent a request-scoped Teammates session
 * 
 * This object stores the application state within the time where the request is
 * made until the request is terminated
 * 
 * It's different from Session (HttpSession).
 * 
 * It's implemented using the idea of ThreadLocal.
 * 
 * @author huy
 * 
 */
public class RequestSession {
	private static final ThreadLocal<RequestSession> PER_THREAD_REQUESTSCOPED = new ThreadLocal<RequestSession>();

	/**
	 * (request-scoped) Return the current Session object
	 * 
	 * @return
	 */
	public static RequestSession instance() {
		RequestSession ss = PER_THREAD_REQUESTSCOPED.get();
		if (ss == null) {
			ss = new RequestSession();
			PER_THREAD_REQUESTSCOPED.set(ss);
		}
		return PER_THREAD_REQUESTSCOPED.get();
	}
	
	public static void finishRequest() {
		PER_THREAD_REQUESTSCOPED.remove();
	}

	
	private static UserService userService = UserServiceFactory.getUserService();

	private Account account = null;
	private Coordinator coordinator = null;
	private Student student = null;

	private List<String> jsList = new ArrayList<String>();
	private List<String> cssList = new ArrayList<String>();
	String pageTitle = "";
	String masterTemplate = "";

	/**
	 * @return Coordinator object, null if account is not coordinator
	 */
	public Coordinator getCoordinator() {
		if (account.isCoordinator()) {
			coordinator = Coordinator.fromAccount(account);
			if (coordinator == null) {
				// First time
				coordinator = new Coordinator(new Key<Account>(Account.class,
					account.getId()));
				DAO.db().put(coordinator);
			}
			return coordinator;
		}
		return null;
	}

	public Student getStudent() {
		if (student == null) {
			// Grab student object from datastore
			student = Student.fromAccount(account.getId());
			if (student == null) {
				// Create student account
				student = new Student();
				student.account = new Key<Account>(Account.class, account.getId());
				DAO.db().put(student);
			}
		}
		return student;
	}

	public Account getAccount() {
		return account;
	}

	public RequestSession() {
		User user = getUser();
		if (user == null) {
			return;
		}
		account = Account.findByEmail(user.getEmail());

		if (account == null) {
			// Sign in for the first time, setting up some database for them
			account = new Account(user.getEmail(), false);
			DAO.db().put(account);
		}
	}

	public User getUser() {
		return userService.getCurrentUser();
	}

	public void requireJs(String src) {
		jsList.add(src);
	}
	
	public void requireCss(String src) {
		cssList.add(src);
	}

	public List<String> getJsList() {
		return jsList;
	}
	
	public List<String> getCssList() {
		return cssList;
	}

	public String getPageTitle() {
		if (pageTitle.isEmpty()) {
			return "Teammates";
		}
		return pageTitle + " - Teammates";
	}
	
	public void setPageTitle(String title) {
		this.pageTitle = title;
	}
	
	/**
	 * Can only be admin/coordinator/student
	 * @param master
	 */
	public void setMasterTemplate(String master) {
		masterTemplate = master;
	}
	
	public String getMasterTemplate() {
		if (masterTemplate.isEmpty()) {
			throw new RuntimeException("MasterTemplate is not set.");
		}
		return masterTemplate;
	}

}
