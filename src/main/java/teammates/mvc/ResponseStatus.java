package teammates.mvc;

import java.io.Serializable;

/**
 *
 */
public class ResponseStatus implements Serializable {
	private static final long serialVersionUID = -9073714732021972279L;

	public static String SUCCESS = "success", ERROR = "error";

	private String message;
	private String status;

	public ResponseStatus(String status, String message) {
		this.message = message;
		this.setStatus(status);
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the messsage to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

}
