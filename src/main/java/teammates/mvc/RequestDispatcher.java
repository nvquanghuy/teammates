package teammates.mvc;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import teammates.exception.NoPermissionException;
import teammates.exception.NotLoggedInException;
import teammates.mvc.view.ErrorView;
import teammates.mvc.view.RedirectView;
import teammates.mvc.view.View;

import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

/**
 * Served as a RequestDispatcher. This is the starting point of all requests
 * coming to the app
 * 
 * @author Huy Nguyen
 * 
 */
@SuppressWarnings("serial")
public class RequestDispatcher extends HttpServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
		throws IOException {
		doRequest(req, resp);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse resp)
		throws IOException {
		doRequest(req, resp);
	}

	/**
	 * Main Handler. Parse the URL and navigate the right controller to invoke
	 * 
	 * @throws IOException
	 */
	private void doRequest(HttpServletRequest req, HttpServletResponse resp)
		throws IOException {

		// Parse the URL

		URIParser parser = new URIParser(req.getRequestURI());
		Class<? extends Controller> clazz = parser.getControllerClass();
		String action = parser.getAction();

		Controller c = Controller.create(clazz, req, resp);
		if (c == null) {
			throw new RuntimeException("Controller class not found: "
				+ clazz.getName());
		}

		Object ret = null;
		try {
			c.beforeHook();
			ret = c.invoke(action);
			
		} catch (NoPermissionException e) {
			ret = new ErrorView(e.getMessage());
		} catch (NotLoggedInException e) {
			// Redirect to Login page
			UserService userService = UserServiceFactory.getUserService();
			ret = new RedirectView(userService.createLoginURL(req.getRequestURI()));
		}
		if (ret != null) {
			View v = (View) ret;
			v.execute(req, resp);
		}
	}
}
