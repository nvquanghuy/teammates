package teammates.mvc;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import teammates.dev.RequestSession;
import teammates.exception.NoPermissionException;
import teammates.exception.NotLoggedInException;
import teammates.mvc.view.ModelAndView;
import teammates.mvc.view.View;
import teammates.pojo.Account;
import teammates.pojo.Coordinator;
import teammates.pojo.Course;
import teammates.pojo.CourseStudent;
import teammates.pojo.CourseTeam;
import teammates.pojo.Evaluation;
import teammates.pojo.Student;
import teammates.pojo.Submit;
import teammates.pojo.SubmitEntry;
import teammates.template.ViewMap;

import com.googlecode.objectify.ObjectifyService;

/**
 * Base Controller class. To be extended by other controller class
 * 
 * @author huy
 * 
 */
public class Controller {

	static {
		ObjectifyService.register(Account.class);
		ObjectifyService.register(Coordinator.class);
		ObjectifyService.register(Student.class);
		ObjectifyService.register(CourseStudent.class);
		ObjectifyService.register(Course.class);
		ObjectifyService.register(Evaluation.class);
		ObjectifyService.register(CourseTeam.class);
		ObjectifyService.register(Submit.class);
		ObjectifyService.register(SubmitEntry.class);
	}

	private HttpServletRequest req;
	private HttpServletResponse resp;
	private PageRequest pageRequest;

	private void initialize() {
		pageRequest = new PageRequest(req);
	}

	/**
	 * Set a flash message to render, either in this page-render or the following
	 * one.
	 * 
	 * We store the message in HttpSession
	 * 
	 * @param message
	 */
	protected void setFlash(String status, String message) {
		req.getSession().setAttribute("flash", new ResponseStatus(status, message));
	}

	protected void setFlash(String message) {
		setFlash("success", message);
	}
	
	protected void setPageTitle(String title) {
		requestSession().setPageTitle(title);
	}

	public PageRequest getRequest() {
		return pageRequest;
	}

	public HttpServletResponse getServletResponse() {
		return resp;
	}

	protected RequestSession requestSession() {
		return RequestSession.instance();
	}

	public Controller() {
	}

	/**
	 * To be over-riden
	 */
	protected void beforeHook() {
	}

	protected View theRest() {
		return new ModelAndView("teammates.404");
	}

	protected void requireJs(String src) {
		RequestSession.instance().requireJs(src);
	}

	protected void requireCss(String src) {
		RequestSession.instance().requireCss(src);
	}

	/**
	 * @throw NotLoggedInException
	 */
	protected void requireLogin() {
		if (RequestSession.instance().getUser() == null) {
			throw new NotLoggedInException();
		}
	}

	/**
	 * @throw NoPermissionException if current user is not coordinator
	 */
	protected void requireCoordinator() {
		requireLogin();
		if (RequestSession.instance().getCoordinator() == null) {
			throw new NoPermissionException("Coordinator privilige is required.");
		}
	}

	protected void setMasterTemplate(String template) {
		RequestSession.instance().setMasterTemplate(template);
	}

	public static Controller create(Class<? extends Controller> clz,
		HttpServletRequest req, HttpServletResponse resp) {

		Controller controller = null;
		try {
			Constructor<?> ctor = clz.getConstructor();
			controller = (Controller) ctor.newInstance(new Object[] {});
			controller.req = req;
			controller.resp = resp;
			controller.initialize();

		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
			throw new RuntimeException("No default constructor found for class "
				+ clz.getName());
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}

		return controller;
	}

	/**
	 * Call the appropriate action-method.
	 * 
	 * @param action
	 * @return
	 * @throws Throwable
	 */
	public Object invoke(String action) {
		// Default action points to method `index`
		if (action.isEmpty()) {
			action = "index";
		}

		if (methodExists(((Object) this), action)) {
			// Invoke that method to return an Object
			try {
				Method method = this.getClass().getMethod(action);
				Object ret = method.invoke(this);
				return ret;

			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				if (e.getCause() instanceof NoPermissionException) {
					throw (NoPermissionException) e.getCause();
				} else if (e.getCause() instanceof NotLoggedInException) {
					throw (NotLoggedInException) e.getCause();
				} else {
					e.printStackTrace();
					try {
						resp.getWriter().println(e.getCause().getMessage());
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			}
		}

		return null;
	}

	/**
	 * Check if a method exists in an object
	 * 
	 * @return
	 */
	public static boolean methodExists(Object obj, String methodName) {
		try {
			Method[] ms = obj.getClass().getMethods();
			for (Method m : ms) {
				if (m.getName().equals(methodName)) {
					return true;
				}
			}
			obj.getClass().getMethod(methodName);
			return true;
		} catch (SecurityException e) {
			return false;
		} catch (NoSuchMethodException e) {
			System.err.println("Cannot find method " + methodName + " in " + obj.getClass().toString());
			return false;
		}
	}

}
