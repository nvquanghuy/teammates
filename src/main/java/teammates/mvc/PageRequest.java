package teammates.mvc;

import javax.servlet.http.HttpServletRequest;

/**
 * A simplified wrapper of HttpServletRequest
 * 
 * Part of the MVC. Accessible from the controller.
 * 
 */
public class PageRequest {

	public HttpServletRequest req;

	/**
	 * Make sure all the fields listed here are non-empty from the requests
	 * 
	 * @param fields
	 * @return
	 */
	public boolean validateRequired(String... fields) {
		for (String field : fields) {
			String s = getParam(field);
			if (s == null) {
				return false;
			}
			if (s.isEmpty())
				return false;
		}
		return true;
	}

	public PageRequest(HttpServletRequest r) {
		req = r;
	}

	public boolean isAjax() {
		return "XMLHttpRequest".equals(req.getHeader("X-Requested-With"));
	}

	public boolean isPost() {
		return req.getMethod().toLowerCase().equals("post");
	}

	public boolean isGet() {
		return req.getMethod().toLowerCase().equals("get");
	}

	public String getParam(String key) {
		return req.getParameter(key);
	}

	public String getParameter(String key) {
		return this.getParam(key);
	}

	/**
	 * 
	 * @param key
	 * @return -1 if not valid number, otherwise the number
	 */
	public long getParamLong(String key) {
		String s = getParam(key);
		if (s != null) {
			try {
				Long l = Long.parseLong(s);
				return l;
			} catch (NumberFormatException e) {
				return -1;
			}
		}
		return -1;
	}

	public int getParamInt(String key) {
		String s = getParam(key);
		if (s != null) {
			try {
				Integer l = Integer.parseInt(s);
				return l;
			} catch (NumberFormatException e) {
				return -1;
			}
		}
		return -1;
	}

}
