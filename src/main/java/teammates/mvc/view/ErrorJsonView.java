package teammates.mvc.view;

import teammates.mvc.ResponseStatus;

import com.google.gson.Gson;

public class ErrorJsonView extends JsonView {
	public ErrorJsonView(String message) {
		Gson gson = new Gson();
		ResponseStatus st = new ResponseStatus(ResponseStatus.ERROR, message);
		json = gson.toJson(st);
	}

}
