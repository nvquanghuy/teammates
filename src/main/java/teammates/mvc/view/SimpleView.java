package teammates.mvc.view;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SimpleView implements View {
	String msg;
	
	public SimpleView(String html) {
		this.msg = html;
	}

	@Override
	public void execute(HttpServletRequest req, HttpServletResponse resp)
		throws IOException {
		
		resp.getWriter().println(msg);
	}

}
