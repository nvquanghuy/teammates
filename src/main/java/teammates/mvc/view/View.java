package teammates.mvc.view;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

// View.java
public interface View {
	public void execute(HttpServletRequest req, HttpServletResponse resp) throws IOException;
}
