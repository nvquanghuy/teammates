package teammates.mvc.view;

import teammates.dev.RequestSession;
import teammates.template.MasterTemplate;
import teammates.template.ViewMap;

public class ErrorView extends ModelAndView {

	public ErrorView(String message) {
		RequestSession.instance().setMasterTemplate(MasterTemplate.DEFAULT);
		this.template = "tm.error";
		params = ViewMap.of("message", message);
	}

}
