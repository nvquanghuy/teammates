package teammates.mvc.view;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import teammates.mvc.ResponseStatus;
import teammates.template.ViewMap;

import com.google.gson.Gson;

/**
 * Return a View that contains a JSON object
 * 
 * @author huy
 * 
 */
public class JsonView implements View {
	protected String json = "";

	public JsonView() {
	}

	public JsonView(ResponseStatus rs) {
		Gson gson = new Gson();
		json = gson.toJson(rs);
	}

	public <K, V> JsonView(ViewMap vm) {
		Gson gson = new Gson();
		json = gson.toJson(vm);
		// System.out.println(json);
	}

	public String getJson() {
		return json;
	}

	public String toString() {
		return getJson();
	}

	@Override
	public void execute(HttpServletRequest req, HttpServletResponse resp)
		throws IOException {
		resp.setContentType("application/json");
		resp.getWriter().write(getJson());
	}

}
