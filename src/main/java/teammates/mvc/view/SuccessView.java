package teammates.mvc.view;

import teammates.dev.RequestSession;
import teammates.template.MasterTemplate;
import teammates.template.ViewMap;

public class SuccessView extends ModelAndView {

	SuccessView() {
	}

	public SuccessView(String message) {
		this.template = "tm.success";
		this.params = ViewMap.of("message", message, "next", null);
		RequestSession.instance().setMasterTemplate(MasterTemplate.DEFAULT);
	}

	public SuccessView(String message, String redirect) {
		this.template = "tm.success";
		this.params = ViewMap.of("message", message, "next", redirect);
		RequestSession.instance().setMasterTemplate(MasterTemplate.DEFAULT);
	}

}
