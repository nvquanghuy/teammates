package teammates.mvc.view;

import teammates.mvc.ResponseStatus;

import com.google.gson.Gson;

public class SuccessJsonView extends JsonView {
	public SuccessJsonView(String message) {
		Gson gson = new Gson();
		ResponseStatus st = new ResponseStatus(ResponseStatus.SUCCESS, message);
		json = gson.toJson(st);
	}

}
