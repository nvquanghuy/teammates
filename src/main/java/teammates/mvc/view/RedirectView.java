package teammates.mvc.view;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Redirect the page to somewhere else
 * 
 */
public class RedirectView implements View {
	private String url;
	public RedirectView(String url) {
		this.url = url;
	}
	@Override
	public void execute(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.sendRedirect(url);
		resp.flushBuffer();
	}
}
