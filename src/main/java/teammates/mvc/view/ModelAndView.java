package teammates.mvc.view;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.RegexFileFilter;

import teammates.config.Config;
import teammates.dev.RequestSession;
import teammates.mvc.ResponseStatus;
import teammates.template.Soy;
import teammates.template.ViewMap;

import com.google.common.collect.ImmutableMap;
import com.google.template.soy.SoyFileSet;
import com.google.template.soy.SoyFileSet.Builder;
import com.google.template.soy.data.SoyData;
import com.google.template.soy.data.SoyMapData;
import com.google.template.soy.tofu.SoyTofu;
import com.google.template.soy.tofu.SoyTofu.Renderer;

public class ModelAndView implements View {

	private static SoyTofu tofu = null;

	private static SoyTofu makeTofu() throws IOException {
		Builder b = new SoyFileSet.Builder();
		b.setCompileTimeGlobals(new File("WEB-INF/templates/globals.txt"));

		File dir = new File("WEB-INF/templates");

		Collection<File> files = org.apache.commons.io.FileUtils.listFiles(
			dir,
			new RegexFileFilter("^(.*\\.soy?)"),
			DirectoryFileFilter.DIRECTORY
			);
		
		for (File f : files) {
			b.add(f);
		}
		SoyFileSet sfs = b.build();

		// Compile the template into a SoyTofu object.
		// SoyTofu's newRenderer method returns an object that can render any
		// template in file set.
		return sfs.compileToTofu();
	}

	private static SoyTofu getSoyTofu() throws IOException {
		// If production mode we'll build only once at startup time (cold-start)
		// If we're in development mode, we rebuild every time to avoid restarting
		// app
		if (Config.IS_PRODUCTION) {
			if (tofu == null) {
				tofu = makeTofu();
			}
			return tofu;
		} else {
			return makeTofu();
		}
	}

	// template name, the view
	protected String template;

	// data, the model
	protected ViewMap params;

	public ModelAndView() {
	}

	public ModelAndView(String template, ViewMap params) {
		this.template = template;
		this.params = params;
	}
	
	public ModelAndView(String template) {
		this.template = template;
		this.params = new ViewMap();
	}

	public ModelAndView(String string, SoyData data2) {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void execute(HttpServletRequest req, HttpServletResponse resp)
		throws IOException {
		resp.setContentType("text/html");

		Renderer renderer = getSoyTofu().newRenderer(this.template).setData(params);

		// Pop out flash message
		SoyData flash = null;
		if (req.getSession().getAttribute("flash") != null) {
			ResponseStatus rs = (ResponseStatus) req.getSession().getAttribute(
				"flash");
			flash = new SoyMapData(
				"status", rs.getStatus(),
				"message", rs.getMessage()
			);
			req.getSession().removeAttribute("flash");
		}

		if (flash == null) {
			renderer = renderer.setIjData(ViewMap.of("flash", ""));
		} else {
			renderer = renderer.setIjData(ViewMap.of("flash", flash));
		}

		String bodyHtml = renderer.render();

		RequestSession rc = RequestSession.instance();

		String templateName = "tm." + rc.getMasterTemplate();

		renderer = getSoyTofu().newRenderer(templateName).setData(
			ImmutableMap.<String, Object> of("bodyHtml", bodyHtml));

		renderer = renderer.setIjData(ViewMap.of(
			"jscripts", rc.getJsList(),
			"stylesheets", rc.getCssList(),
			"title", rc.getPageTitle())
			);

		resp.getWriter().write(renderer.render());
		resp.getWriter().flush();
	}
}
