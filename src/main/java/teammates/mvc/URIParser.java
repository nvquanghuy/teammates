package teammates.mvc;

import org.apache.commons.lang3.StringUtils;

import teammates.config.URLMappings;

public class URIParser {

	private Class<? extends Controller> controllerClass;
	private String action;

	public URIParser(String uri) {
		String origUri = uri;
		String mappedKey = null;
		for (String key : URLMappings.MAP.keySet()) {
			if (uri.startsWith(key)) {
				mappedKey = key;
				break;
			}
		}
		if (mappedKey == null) {
			throw new IllegalStateException("Cannot map to Controller");
		}

		controllerClass = URLMappings.MAP.get(mappedKey);
		String uri2 = uri.substring(mappedKey.length()).intern();
		uri2 = StringUtils.strip(uri2, "/");

		if (uri2.contains("/")) {
			throw new IllegalStateException("Unrecognizable: " + origUri);
		}

		this.action = uri2;
	}

	/**
	 * @return the controllerClass
	 */
	public Class<? extends Controller> getControllerClass() {
		return controllerClass;
	}

	/**
	 * @return the action
	 */
	public String getAction() {
		return action;
	}
}
