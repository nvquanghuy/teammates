package teammates.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import teammates.dev.DAO;
import teammates.dev.RequestSession;
import teammates.mvc.Controller;
import teammates.mvc.view.ErrorView;
import teammates.mvc.view.ModelAndView;
import teammates.mvc.view.RedirectView;
import teammates.mvc.view.View;
import teammates.pojo.Course;
import teammates.pojo.CourseStudent;
import teammates.pojo.Evaluation;
import teammates.pojo.EvaluationR;
import teammates.pojo.Student;
import teammates.pojo.Submit;
import teammates.pojo.SubmitEntry;
import teammates.template.MasterTemplate;
import teammates.template.Soy;
import teammates.template.ViewMap;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;

public class StudentEvaluationController extends Controller {

	@Override
	public void beforeHook() {
		requireLogin();
		setMasterTemplate(MasterTemplate.STUDENT);
	}

	public View index() {
		return list();
	}

	public View list() {
		Student student = RequestSession.instance().getStudent();

		// Get list of evaluations that this student can participate
		List<CourseStudent> cstudents = CourseStudent.listByStudent(student.getId());
		Set<Key<Course>> courseKeys = new HashSet<Key<Course>>();
		for (CourseStudent cs : cstudents) {
			courseKeys.add(cs.getCourseKey());
		}
		if (courseKeys.size() == 0) {
			courseKeys.add(null);
		}

		List<Evaluation> pending = DAO.db().query(Evaluation.class).filter(
			"course in", courseKeys).filter("status", Evaluation.OPEN).list();

		List<EvaluationR> pendingR = EvaluationR.fromList(pending);

		List<Evaluation> past = DAO.db().query(Evaluation.class).filter(
			"course in", courseKeys).filter("status >=", Evaluation.CLOSED).list();

		List<EvaluationR> pastR = EvaluationR.fromList(past);

		ViewMap params = new ViewMap();
		params.put("pending", Soy.make(pendingR));
		params.put("past", Soy.make(pastR));

		return new ModelAndView("tm.student.eval_list", params);
	}

	/**
	 * Submit evaluation
	 * 
	 * @return
	 */
	public View submit() {
		Student student = RequestSession.instance().getStudent();

		Long evid = this.getRequest().getParamLong("evid");
		Evaluation ev = Evaluation.get(evid);
		if (ev == null)
			return null;

		if (ev.getStatus() != Evaluation.OPEN) {
			return new ErrorView("You cannot submit feedback.");
		}

		CourseStudent cs = CourseStudent.byCourseAndStudent(
			ev.getCourseKey().getId(), student.getId());
		if (cs == null)
			return null;

		ViewMap params = new ViewMap();

		EvaluationR evr = new EvaluationR(ev);

		Submit submit = Submit.byEvalAndCourseStudent(ev.getId(), cs.getId());
		List<SubmitEntry> entries = SubmitEntry.bySubmit(submit.getId());

		if (this.getRequest().isPost()) {

			// Collect data and save them

			Objectify db = DAO.db();

			// Save the entries
			for (SubmitEntry entry : entries) {
				String content = this.getRequest().getParam("content_" + entry.getId());
				entry.setContent(content);
			}

			db.put(entries);

			// Save the submit object
			submit.justSubmitted();
			db.put(submit);

			setFlash("success", "Your submission has been recorded. Thank you!");
			return new RedirectView("/app/student/evaluation");

		} else {
			// Render submit form

			// Separate self entry from the list
			SubmitEntry self = null;
			for (SubmitEntry entry : entries) {
				// System.out.println(entry.getToStudentId());
				if (entry.getToStudentId() == cs.getId()) {
					self = entry;
					entries.remove(entry);
					break;
				}
			}

			// Push params
			cs.loadTeam();
			params.put("teamName", cs.getTeam().getName());
			params.put("ev", Soy.make(evr));
			params.put("self", Soy.make(self));
			params.put("entries", Soy.make(entries));

			return new ModelAndView("tm.student.eval_submit", params);

		}
	}

	public View results() {
		Student student = RequestSession.instance().getStudent();
		long evid = this.getRequest().getParamLong("evid");
		Evaluation ev = Evaluation.get(evid);

		long courseId = ev.getCourseKey().getId();
		CourseStudent cs = CourseStudent.byCourseAndStudent(courseId,
			student.getId());

		// Permission check
		if (ev.getStatus() != Evaluation.PUBLISHED) {
			return new ErrorView("Evaluation hasn't been published");
		}

		List<SubmitEntry> entries = SubmitEntry.listStudentResultsEntries(evid,
			cs.getId());

		EvaluationR evr = new EvaluationR(ev);

		ViewMap params = ViewMap.of(
			"entries", Soy.make(entries),
			"cs", Soy.make(cs),
			"ev", Soy.make(evr));

		return new ModelAndView("tm.student.eval_results", params);
	}

}
