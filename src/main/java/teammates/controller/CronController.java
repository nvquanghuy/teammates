package teammates.controller;

import java.util.List;

import teammates.mvc.Controller;
import teammates.mvc.view.JsonView;
import teammates.mvc.view.View;
import teammates.pojo.Evaluation;
import teammates.template.Soy;
import teammates.template.ViewMap;

public class CronController extends Controller {

	/**
	 * Go and activate list of AWAITING evaluations
	 * 
	 * Go and close list of OPEN evaluations
	 * 
	 * @return
	 */
	public View index() {
		System.out.println("Cron running...");

		List<Evaluation> closedEvals = Evaluation.closeEvaluations();

		ViewMap vm = ViewMap.of("closed", Soy.make(closedEvals));

		System.out.println("Cron ended. Output:");
		System.out.println(vm.toJson());

		// Return for debug purposes.
		return new JsonView(vm);
	}

	public View test() {
		return null;
	}

}
