package teammates.controller;

import teammates.mvc.Controller;
import teammates.mvc.view.ModelAndView;
import teammates.mvc.view.View;
import teammates.template.MasterTemplate;

public class HomeController extends Controller {

	@Override
	protected void beforeHook() {
	}
	
	public View index() {
		requireCss("/css/home.css");
		setMasterTemplate(MasterTemplate.NONE);
		requestSession().setPageTitle("");
		return new ModelAndView("tm.home");
	}
	
}
