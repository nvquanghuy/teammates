package teammates.controller;

import java.util.List;

import teammates.dev.DAO;
import teammates.exception.NoPermissionException;
import teammates.mvc.Controller;
import teammates.mvc.view.ModelAndView;
import teammates.mvc.view.RedirectView;
import teammates.mvc.view.View;
import teammates.pojo.Account;
import teammates.template.MasterTemplate;
import teammates.template.Soy;
import teammates.template.ViewMap;

import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

public class AdminController extends Controller {

	@Override
	public void beforeHook() {
		setMasterTemplate(MasterTemplate.DEFAULT);

		// Authentication
		requireLogin();

		UserService us = UserServiceFactory.getUserService();
		if (!us.isUserAdmin()) {
			throw new NoPermissionException("Administration privilege is required.");
		}

	}

	public View index() {

		ViewMap params = new ViewMap();

		List<Account> accounts =
			DAO.db().query(Account.class)
				.filter("isCoordinator", true).list();

		params.put("coords", Soy.make(accounts));

		return new ModelAndView("tm.admin.index", params);

	}

	/**
	 * Create new coordinator account
	 * 
	 * @return
	 */
	public View make_coordinator() {
		// Create new account
		String email = this.getRequest().getParam("email");

		Account.makeCoordinator(email);

		setFlash("success", "Account " + email + " has been given coordinator access.");
		
		return new RedirectView("/app/admin");
	}

	/**
	 * Create new coordinator account
	 * 
	 * @return
	 */
	public View remove_coordinator() {
		// Create new account
		long cid = this.getRequest().getParamLong("cid");

		Account.removeCoordinator(cid);

		this.setFlash("success", "Account coordinator access has been removed.");
		return new RedirectView("/app/admin");
	}

}
