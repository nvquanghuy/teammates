package teammates.controller;

import java.util.Date;
import java.util.List;

import teammates.Lang;
import teammates.dev.DAO;
import teammates.dev.RequestSession;
import teammates.dev.Util;
import teammates.mvc.Controller;
import teammates.mvc.view.ErrorJsonView;
import teammates.mvc.view.ErrorView;
import teammates.mvc.view.JsonView;
import teammates.mvc.view.ModelAndView;
import teammates.mvc.view.RedirectView;
import teammates.mvc.view.SuccessJsonView;
import teammates.mvc.view.View;
import teammates.pojo.Coordinator;
import teammates.pojo.Course;
import teammates.pojo.CourseStudent;
import teammates.pojo.CourseTeam;
import teammates.pojo.Evaluation;
import teammates.pojo.EvaluationR;
import teammates.pojo.Submit;
import teammates.pojo.SubmitEntry;
import teammates.template.MasterTemplate;
import teammates.template.Soy;
import teammates.template.ViewMap;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;

public class CoordinatorController extends Controller {

	@Override
	public void beforeHook() {
		setMasterTemplate(MasterTemplate.COORDINATOR);
		requireCoordinator();
	}

	public View index() {
		setPageTitle("Managing Courses");
		Coordinator coord = RequestSession.instance().getCoordinator();

		List<Course> courses =
			DAO.db().query(Course.class)
				.filter("coordinator", Key.create(Coordinator.class, coord.getId()))
				.order("-id")
				.list();

		ViewMap params = ViewMap.of("courses", Soy.make(courses));

		if (this.getRequest().isGet() && this.getRequest().isAjax()) {
			return new JsonView(params);
		} else {
			return new ModelAndView("tm.coordinator.course_index", params);
		}
	}

	public View course() {
		Coordinator coord = RequestSession.instance().getCoordinator();
		requireJs("/js/course_view.js");

		long courseId = this.getRequest().getParamLong("courseid");
		Course course = Course.get(courseId);
		if (course == null) {
			return new ErrorView(Lang.COURSE_NOT_FOUND);
		}
		// Check coordinator has permission to edit this course
		if (coord.getId() != course.getCoordinatorId()) {
			return new ErrorView(Lang.COURSE_NO_PERMISSION);
		}
		
		setPageTitle(course.getCode() + " - " + course.getName());

		List<CourseStudent> courseStudents = CourseStudent.listFromCourse(courseId);

		// Get list of evaluations of this course
		List<EvaluationR> evals = EvaluationR.fromList(
			Evaluation.listByCourse(courseId)
			);

		return new ModelAndView(
			"tm.coordinator.course_view",
			ViewMap.of(
				"course", Soy.make(course),
				"students", Soy.make(courseStudents),
				"evals", Soy.make(evals)
				));
		
		
	}

	/**
	 * Edit Course information
	 * 
	 * @return
	 */
	public View course_settings() {
		Coordinator coord = RequestSession.instance().getCoordinator();

		long courseId = this.getRequest().getParamLong("courseid");
		Course course = Course.get(courseId);
		if (course == null) {
			return new ErrorView(Lang.COURSE_NOT_FOUND);
		}
		
		// Check coordinator has permission to edit this course
		if (coord.getId() != course.getCoordinatorId()) {
			return new ErrorView(Lang.COURSE_NO_PERMISSION);
		}

		// TODO: Validation

		if (this.getRequest().isPost()) {
			String code = this.getRequest().getParam("code");
			String name = this.getRequest().getParam("name");
			String term = this.getRequest().getParam("term");

			course.setCode(code);
			course.setName(name);
			course.setTerm(term);

			DAO.db().put(course);

			setFlash("success", "Course has been updated.");
		}

		return new ModelAndView("tm.coordinator.course_settings",
			ViewMap.of("course", Soy.make(course)));
	}

	public View manage_students() {
		Coordinator coord = RequestSession.instance().getCoordinator();
		requireJs("/js/manage_students.js");

		long courseId = this.getRequest().getParamLong("courseid");
		Course course = Course.get(courseId);
		if (course == null) {
			return new ErrorView(Lang.COURSE_NOT_FOUND);
		}
		// Check coordinator has permission to edit this course
		if (coord.getId() != course.getCoordinatorId()) {
			return new ErrorView(Lang.COURSE_NO_PERMISSION);
		}

		List<CourseStudent> courseStudents = CourseStudent.listFromCourse(courseId);
		for (CourseStudent cs : courseStudents) {
			cs.loadTeam();
		}

		// Prepare for presentation
		return new ModelAndView(
			"tm.coordinator.manage_students",
			ViewMap.of(
				"course", Soy.make(course),
				"students", Soy.make(courseStudents)
				));
	}


	public View course_create() {
		if (this.getRequest().isPost()) {
			Coordinator coord = RequestSession.instance().getCoordinator();

			if (!this.getRequest().validateRequired("courseid", "coursename", "term")) {
				setFlash("error", "Fill in relevant fields.");
			} else {
				Course course = new Course(coord);
				course.setName(this.getRequest().getParam("coursename"));
				course.setCode(this.getRequest().getParam("courseid"));
				course.setTerm(this.getRequest().getParam("term"));
				Course.create(course);

				setFlash("Course created! Now enrolling students for your course. ");
				return new RedirectView("/app/c/course_enroll?w=1&courseid=" + course.getId());
			}
		}
		// GET
		return new ModelAndView("tm.coordinator.course_create");
	}

	public View eval_remind() {
		long evid = this.getRequest().getParamLong("evid");
		Evaluation ev = Evaluation.get(evid);
		if (ev == null) {
			return new ErrorView("No evaluation found.");
		}

		ev.remind();

		setFlash("Reminder emails have been sent to students who haven't submitted their feedback!");
		return new RedirectView("/app/c/eval?evid=" + evid);
	}

	public View eval_open() {
		long evid = this.getRequest().getParamLong("evid");

		Evaluation ev = Evaluation.get(evid);
		if (ev.open()) {
			setFlash("Evaluation launched!");
			return new RedirectView("/app/c/eval?evid=" + evid);
			
		} else {
			return new ErrorView("Error launching evaluation.");
		}
	}

	public View eval_close() {
		long evid = this.getRequest().getParamLong("evid");
		Evaluation ev = Evaluation.get(evid);
		if (ev.close()) {
			setFlash("Evaluation has been ended!");
			return new RedirectView("/app/c/eval?evid=" + evid);
		} else {
			return new ErrorJsonView("Error ending evaluation.");
		}
	}

	public View eval_edit() {
		RequestSession.instance().setPageTitle("Edit Evaluation");
		long evId = this.getRequest().getParamLong("evid");
		Evaluation ev = Evaluation.get(evId);
		if (ev == null) {
			return new ErrorView("No evaluation found.");
		}

		EvaluationR evr = new EvaluationR(ev);

		if (this.getRequest().isGet()) {
			// Retrieve the object and render
			return new ModelAndView("tm.coordinator.eval_edit", ViewMap.of(
				"ev", Soy.make(evr)
				));

		} else if (this.getRequest().isPost()) {
			if (!this.getRequest().validateRequired(
				"deadline_date", "deadline_time", "graceperiod")) {
				return new ErrorView("Enter all required fields");
			}

			ev.setName(this.getRequest().getParam("name"));

			ev.setInstructions(this.getRequest().getParam("instr"));

			String deadlineDate = this.getRequest().getParameter("deadline_date");
			Integer deadlineTime = this.getRequest().getParamInt("deadline_time");
			Date deadline = Util.convertToDate(deadlineDate, deadlineTime);
			ev.setDeadline(deadline);

			ev.setGracePeriod(Integer.parseInt(this.getRequest().getParameter(
				"graceperiod")));

			DAO.db().put(ev);
			setFlash("Evaluation updated.");
			return new RedirectView("/app/c/eval?evid=" + ev.getId());
		}

		return null;
	}

	/**
	 * View all feedback this student sends
	 * 
	 * @param submitid
	 * @return
	 */
	public View feedback_view_from() {
		setMasterTemplate(MasterTemplate.NONE);

		long submitId = this.getRequest().getParamLong("sid");
		Submit submit = Submit.get(submitId);
		submit.loadEntries();

		CourseStudent cs = CourseStudent.get(submit.getKeyCourseStudent());

		return new ModelAndView("tm.coordinator.feedback_view_from", ViewMap.of(
			"submit", Soy.make(submit),
			"cs", Soy.make(cs)
			));
	}

	/**
	 * View all feedback others write for this student
	 * 
	 * @param csid
	 * @param evid
	 * @return
	 */
	public View feedback_view_for() {
		setMasterTemplate(MasterTemplate.NONE);

		long evid = this.getRequest().getParamLong("evid");
		long csid = this.getRequest().getParamLong("csid");

		List<SubmitEntry> entries = SubmitEntry.listStudentResultsEntries(evid, csid);
		CourseStudent cs = CourseStudent.get(csid);

		return new ModelAndView("tm.coordinator.feedback_view_for", ViewMap.of(
			"entries", Soy.make(entries),
			"cs", Soy.make(cs)
			));
	}

	public View eval_delete() {

		long evid = this.getRequest().getParamLong("evid");
		Evaluation ev = Evaluation.get(evid);
		if (ev == null) {
			// Not found
			return new ErrorView("No evaluation found.");
		}

		if (this.getRequest().isPost()) {

			Coordinator coord = RequestSession.instance().getCoordinator();

			if (DAO.db().get(ev.getCourseKey()).getCoordinatorId() != coord.getId()) {
				// No permission
				return new ErrorView("You don't have permission to perform this action.");
			}

			long courseId = ev.getCourseId();
			Evaluation.delete(evid);

			setFlash("success", "Evalution deleted");
			return new RedirectView("/app/c/course?courseid=" + courseId);
		}

		return new ModelAndView("tm.coordinator.eval_delete", ViewMap.of(
			"ev", Soy.make(ev)
			));

	}

	public View course_delete() {
		Coordinator coord = RequestSession.instance().getCoordinator();
		long courseId = this.getRequest().getParamLong("courseid");
		Course course = Course.get(courseId);
		if (course == null) {
			return new ErrorView(Lang.COURSE_NOT_FOUND);
		}
		// Check coordinator has permission to edit this course
		if (coord.getId() != course.getCoordinatorId()) {
			return new ErrorView(Lang.COURSE_NO_PERMISSION);
		}

		if (this.getRequest().isPost()) {
			String courseR = course.getDisplay();

			// Perform the actual delete
			boolean success = Course.delete(courseId);

			if (success) {
				setFlash("Course " + courseR + " has been deleted successfully.");
				return new RedirectView("/app/c/");
			} else {
				return new ErrorView("Error deleting the course");
			}
		}
		return new ErrorView("Something is wrong");
	}

	// View an evaluation
	public View eval() {
		long evId = this.getRequest().getParamLong("evid");
		Evaluation ev = Evaluation.get(evId);
		if (ev == null) {
			return new ErrorView("No evaluation found.");
		}
		
		EvaluationR evr = new EvaluationR(ev);
		Course course = Course.get(ev.getCourseId());

		setPageTitle(ev.getName() + " - " + course.getCode() );
		
		// Summary
		List<Submit> submits = Submit.listByEvaluation(ev.getId());
		for (Submit s : submits) {
			s.loadCourseStudent().loadTeam();
		}

		// We store as String.
		String mils = String.valueOf(evr.getDeadline().getTime());

		// Retrieve the object and render
		return new ModelAndView(
			"tm.coordinator.eval_view",
			ViewMap.of(
				"ev", Soy.make(evr),
				"course", Soy.make(course),
				"submits", Soy.make(submits),
				"deadline_mils", Soy.make(mils)
				));
	}

	public View eval_create() {
		
		setPageTitle("New Evaluation");
		
		Coordinator coord = RequestSession.instance().getCoordinator();
		long courseId = this.getRequest().getParamLong("courseid");
		Course course = Course.get(courseId);
		if (course == null) {
			return new ErrorView("Course is required.");
		}
		if (coord.getId() != course.getCoordinatorId()) {
			// Defensive check
			return new ErrorView(Lang.COURSE_NO_PERMISSION);
		}

		if (this.getRequest().isPost()) {
			Evaluation ev = new Evaluation();

			// Validation
			if (!this.getRequest().validateRequired("evaluationname", "instr", "deadline_date", "deadline_time")) {
				setFlash("error", "Enter all required fields");
			} else {
				ev.setCourseKey(new Key<Course>(Course.class, courseId));
				ev.setCoordinatorKey(Key.create(Coordinator.class, coord.getId()));

				// Collecting POST data

				ev.setName(this.getRequest().getParam("evaluationname"));

				ev.setInstructions(this.getRequest().getParam("instr"));

				String deadlineDate = this.getRequest().getParameter("deadline_date");

				Integer deadlineTime = this.getRequest().getParamInt("deadline_time");
				Date deadline = Util.convertToDate(deadlineDate, deadlineTime);
				ev.setDeadline(deadline);

				// ev.setTimeZone(Float.parseFloat(this.getRequest().getParameter("timezone")));

				// ev.setGracePeriod(Integer.parseInt(this.getRequest().getParameter("graceperiod")));

				Evaluation.create(ev);

				setFlash("success", Lang.MSG_EVALUATION_ADDED);
				return new RedirectView("/app/c/eval?evid=" + ev.getId());
			}
		}

		ViewMap vm = ViewMap.of(
			"course", Soy.make(course)
			);

		return new ModelAndView("tm.coordinator.eval_create", vm);
	}

	public View eval_publish() {
		long evid = this.getRequest().getParamLong("evid");
		Evaluation ev = Evaluation.get(evid);
		if (ev == null) {
			return new ErrorView("Evaluation not found.");
		}
		// TODO: Check permission

		ev.publish();

		setFlash("Evaluation published! The results have been emailed to all students.");
		return new RedirectView("/app/c/eval?evid=" + evid);

	}

	public View course_enroll() {

		long courseId = this.getRequest().getParamLong("courseid");
		Course course = Course.get(courseId);
		if (course == null) {
			return new ErrorView(Lang.INVALID_COURSE);
		}

		if (this.getRequest().isGet()) {

			return new ModelAndView("tm.coordinator.course_enroll",
				ViewMap.of("course", Soy.make(course)));

		} else if (this.getRequest().isPost()) {

			if (!this.getRequest().validateRequired("input")) {
				return new ErrorView("Please enter some students information");
			}

			String input = this.getRequest().getParam("input");

			course.enrollStudents(input);

			if (this.getRequest().getParam("w") != null) {
				setFlash("Students enrolled. Go ahead and create your first evaluation.");
				return new RedirectView("/app/c/course?courseid=" + course.getId());
			} else {
				setFlash("Students enrolled.");
				return new RedirectView("/app/c/manage_students?courseid=" + course.getId());
			}
		}
		return null;
	}

	public View cs_edit() {
		long csId = this.getRequest().getParamLong("csid");
		CourseStudent cs = CourseStudent.get(csId);

		cs.loadCourse();
		cs.loadTeam();

		Course course = cs.getCourse();

		if (this.getRequest().isPost()) {
			if (this.getRequest().validateRequired("name", "email")) {

				// Update data
				cs.setName(this.getRequest().getParam("name"));
				cs.setEmail(this.getRequest().getParam("email"));

				long newTeamId = this.getRequest().getParamLong("team");
				if (cs.getTeamId() != newTeamId) {
					cs.setTeamKey(new Key<CourseTeam>(CourseTeam.class, newTeamId));
				}
				cs.comments = this.getRequest().getParam("comments");

				DAO.db().put(cs);

				return new RedirectView("/app/c/course?courseid="
					+ course.getId());

			} else {
				System.out.println("need to fill.");
				// TODO: tell them they need to fill more information
				return null;
			}

		} else {
			List<CourseTeam> cteams = CourseTeam.listByCourse(cs.getCourseKey());

			ViewMap params = ViewMap.of(
				"cs", Soy.make(cs),
				"teams", Soy.make(cteams));

			return new ModelAndView("tm.coordinator.cs_edit", params);
		}
	}

	public View cs_delete() {
		long csId = this.getRequest().getParamLong("csid");
		DAO.db().delete(Key.create(CourseStudent.class, csId));
		return new SuccessJsonView("CS deleted.");
	}

	public View cs_create() {
		if (this.getRequest().isGet()) {
			setMasterTemplate(MasterTemplate.NONE);

			long courseid = this.getRequest().getParamLong("courseid");
			Course course = Course.get(courseid);
			List<CourseTeam> teams = CourseTeam.listByCourse(courseid);

			return new ModelAndView("tm.coordinator.cs_create", ViewMap.of(
				"course", Soy.make(course),
				"teams", Soy.make(teams)
				));

		} else { // POST
			Objectify db = DAO.db();

			long courseId = this.getRequest().getParamLong("courseid");
			Course c = Course.get(courseId);
			if (c == null) {
				return null;
			}
			CourseStudent cs = new CourseStudent();
			cs.setCourseKey(Key.create(Course.class, c.getId()));
			cs.setName(this.getRequest().getParam("name"));
			cs.setEmail(this.getRequest().getParam("email"));

			int teamId = this.getRequest().getParamInt("teamid");
			String newTeamName = this.getRequest().getParam("newteamname");

			if (teamId > 0) {
				cs.setTeamKey(Key.create(CourseTeam.class, teamId));
			} else {
				// Add new team
				CourseTeam cteam = CourseTeam.foundOrCreateByNameAndCourse(newTeamName, courseId);
				cs.setTeamKey(Key.create(CourseTeam.class, cteam.getId()));
			}
			db.put(cs);
		}

		return null;
	}

	public View cs_list() {
		long courseId = this.getRequest().getParamLong("courseid");
		List<CourseStudent> courseStudents = CourseStudent.listFromCourse(courseId);

		return new JsonView(ViewMap.of(
			"students", Soy.make(courseStudents)
			));
	}

}
