package teammates.controller;

import java.util.List;

import teammates.Lang;
import teammates.dev.DAO;
import teammates.dev.RequestSession;
import teammates.mvc.Controller;
import teammates.mvc.view.ErrorView;
import teammates.mvc.view.JsonView;
import teammates.mvc.view.ModelAndView;
import teammates.mvc.view.RedirectView;
import teammates.mvc.view.SuccessJsonView;
import teammates.mvc.view.View;
import teammates.pojo.Coordinator;
import teammates.pojo.Course;
import teammates.pojo.CourseStudent;
import teammates.pojo.CourseTeam;
import teammates.pojo.Evaluation;
import teammates.pojo.EvaluationR;
import teammates.template.MasterTemplate;
import teammates.template.Soy;
import teammates.template.ViewMap;

import com.googlecode.objectify.Key;

/**
 * 
 *
 */
// @Map: /coordinator/course
public class CoordCourseController extends Controller {

	@Override
	public void beforeHook() {
		requireLogin();
		requireCoordinator();
		requestSession().setMasterTemplate(MasterTemplate.COORDINATOR);
	}

	public CoordCourseController() {
	}

	public View view() {
		Coordinator coord = RequestSession.instance().getCoordinator();
		requireJs("/js/course_view.js");

		long courseId = this.getRequest().getParamLong("courseid");
		Course course = Course.get(courseId);
		if (course == null) {
			return new ErrorView(Lang.COURSE_NOT_FOUND);
		}
		// Check coordinator has permission to edit this course
		if (coord.getId() != course.getCoordinatorId()) {
			return new ErrorView(Lang.COURSE_NO_PERMISSION);
		}

		List<CourseStudent> courseStudents = CourseStudent.listFromCourse(courseId);

		// Get list of evaluations of this course
		List<EvaluationR> evals = EvaluationR.fromList(
			Evaluation.listByCourse(courseId)
			);

		// Prepare for presentation
		return new ModelAndView(
			"tm.coordinator.course_view",
			ViewMap.of(
				"course", Soy.make(course),
				"students", Soy.make(courseStudents),
				"evals", Soy.make(evals)
				));
	}

	/**
	 * index is the default behavior
	 */
	public View index() {
		return list();
	}

	public View list() {
		return new RedirectView("/app/c");
	}

	// /**
	// * AJAX. Remind all unregistered students
	// *
	// * @return
	// */
	// public View remind_all_students() {
	//
	// long courseId = this.getRequest().getParamLong("courseid");
	// Course course = Course.get(courseId);
	// if (course == null) {
	// return new ErrorView(Lang.INVALID_COURSE);
	// }
	//
	// course.sendKeysToUnjoined();
	//
	// return new SuccessJsonView("Emails have been sent");
	// }

	// /**
	// * /remind_student?csid=[course_student_id]
	// *
	// * @return
	// */
	// public View remind_student() {
	//
	// long csId = this.getRequest().getParamLong("csid");
	// CourseStudent cs = CourseStudent.get(csId);
	//
	// if (cs != null) {
	// Emails.remindStudent(cs);
	// }
	//
	// return new SuccessJsonView("Email has been sent to " + cs.getName());
	// }

	/**
	 * /delete_student?csid=[course_student_id]
	 * 
	 * @return
	 */
	public View delete_student() {

		long csId = this.getRequest().getParamLong("csid");

		CourseStudent cs = CourseStudent.get(csId);

		CourseStudent.delete(csId);
		CourseTeam.cleanupTeams(cs.getCourse().getId());

		return new SuccessJsonView("Student deleted.");
	}

	public View delete_all_students() {

		long courseId = this.getRequest().getParamLong("courseid");
		Course course = Course.get(courseId);
		if (course == null) {
			return new ErrorView(Lang.INVALID_COURSE);
		}

		CourseStudent.deleteCourseStudents(courseId);
		CourseTeam.cleanupTeams(courseId);

		return new SuccessJsonView("All students deleted");
	}

	public View view_student() {
		// Get student information
		long csId = this.getRequest().getParamLong("csid");
		CourseStudent cs = CourseStudent.get(csId);
		if (cs == null) {
			return new ErrorView("Student not found!");
		}
		cs.loadCourse();
		cs.loadTeam();
		ViewMap params = ViewMap.of("cs", Soy.make(cs));
		return new ModelAndView("tm.coordinator.cs_view", params);
	}

	public View edit_student() {
		long csId = this.getRequest().getParamLong("csid");
		CourseStudent cs = CourseStudent.get(csId);

		cs.loadCourse();
		cs.loadTeam();

		Course course = cs.getCourse();

		if (this.getRequest().isPost()) {
			if (this.getRequest().validateRequired("name", "email")) {

				// Update data
				cs.setName(this.getRequest().getParam("name"));
				cs.setEmail(this.getRequest().getParam("email"));

				long newTeamId = this.getRequest().getParamLong("team");
				if (cs.getTeamId() != newTeamId) {
					cs.setTeamKey(new Key<CourseTeam>(CourseTeam.class, newTeamId));
				}
				cs.comments = this.getRequest().getParam("comments");

				DAO.db().put(cs);

				return new RedirectView("/app/coordinator/course/view?courseid="
					+ course.getId());

			} else {
				System.out.println("need to fill.");
				// TODO: tell them they need to fill more information
				return null;
			}

		} else {
			List<CourseTeam> cteams = CourseTeam.listByCourse(cs.getCourseKey());

			ViewMap params = ViewMap.of(
				"cs", Soy.make(cs),
				"teams", Soy.make(cteams));

			return new ModelAndView("tm.coordinator.cs_edit", params);
		}
	}

	public View list_students() {
		long courseId = this.getRequest().getParamLong("courseid");
		Course course = Course.get(courseId);

		List<CourseStudent> courseStudents = CourseStudent.listFromCourse(courseId);

		// Prepare for presentation

		return new JsonView(ViewMap.of(
			"course", Soy.make(course),
			"students", Soy.make(courseStudents)
			));
	}

}