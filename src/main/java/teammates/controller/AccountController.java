package teammates.controller;

import java.io.IOException;

import teammates.dev.Accounts;
import teammates.mvc.Controller;
import teammates.mvc.view.RedirectView;
import teammates.mvc.view.View;
import teammates.pojo.Account;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

public class AccountController extends Controller {

	/**
	 * Handle /app/account/logout
	 */
	public View logout() {
		UserService us = UserServiceFactory.getUserService();
		return new RedirectView(us.createLogoutURL("/"));
	}
	

	/**
	 * Handle /app/account/coord_login
	 */
	public View coord_login() {
		Accounts accounts = Accounts.inst();

		User user = accounts.getUser();
		if (user == null) {
			try {
				this.getServletResponse().sendRedirect(
					accounts.getLoginPage("/app/account/coord_login"));
				return null;
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			Objectify ofy = ObjectifyService.begin();

			Account account = ofy.query(Account.class).filter("email",
				user.getEmail()).get();

			if (account == null) {
				// sign in for the first time
				account = new Account(user.getEmail(), false);
				ofy.put(account);
			} else {
			}
		}

		return null;
	}

}
