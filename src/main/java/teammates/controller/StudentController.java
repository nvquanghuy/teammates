package teammates.controller;

import java.util.List;

import teammates.dev.DAO;
import teammates.dev.RequestSession;
import teammates.mvc.Controller;
import teammates.mvc.view.ErrorView;
import teammates.mvc.view.ModelAndView;
import teammates.mvc.view.SuccessView;
import teammates.mvc.view.View;
import teammates.pojo.CourseStudent;
import teammates.pojo.Evaluation;
import teammates.pojo.EvaluationR;
import teammates.pojo.Submit;
import teammates.pojo.SubmitEntry;
import teammates.template.MasterTemplate;
import teammates.template.Soy;
import teammates.template.ViewMap;

import com.googlecode.objectify.Objectify;

public class StudentController extends Controller {

	@Override
	public void beforeHook() {
		setMasterTemplate(MasterTemplate.DEFAULT);
	}

	public View submit() {
		requireCss("/css/submit.css");
		RequestSession.instance().setPageTitle("Submit Evaluation");
		long evid = this.getRequest().getParamLong("evid");
		long csid = this.getRequest().getParamLong("csid");
		String hash = this.getRequest().getParam("s");

		Evaluation ev = Evaluation.get(evid);
		CourseStudent cs = CourseStudent.get(csid);
		if (cs == null || cs == null) {
			return new ErrorView("Invalid link.");
		}

		if (ev.getStatus() != Evaluation.OPEN) {
			return new ErrorView("The evaluation is not open.");
		}

		Submit submit = Submit.byEvalAndCourseStudent(evid, csid);
		// Checking the hash value
		if (!submit.getSecret().equals(hash)) {
			// We will just give vague response.
			return new ErrorView("Invalid link");
		}

		List<SubmitEntry> entries = SubmitEntry.bySubmit(submit.getId());

		EvaluationR evr = new EvaluationR(ev);

		if (this.getRequest().isPost()) {
			// Collect data and save them
			Objectify db = DAO.db();

			// Save the entries
			for (SubmitEntry entry : entries) {
				String content = this.getRequest().getParam("content_" + entry.getId());
				entry.setContent(content);
			}
			db.put(entries);

			// Save the submit object
			submit.justSubmitted();
			db.put(submit);

			return new SuccessView("Your submission has been recorded. Thank you!");
		} else { // GET
			// Render submit form

			// Separate self entry from the list
			SubmitEntry self = null;
			for (SubmitEntry entry : entries) {
				if (entry.getToStudentId() == cs.getId()) {
					self = entry;
					entries.remove(entry);
					break;
				}
			}

			// Push params
			cs.loadCourse();
			cs.loadTeam();

			List<CourseStudent> members = CourseStudent.listByTeam(cs.getTeamId());

			// We store as String.
			String deadline_mils = String.valueOf(evr.getDeadline().getTime());

			ViewMap params = new ViewMap();
			params.put("cs", Soy.make(cs));
			params.put("ev", Soy.make(evr));
			params.put("self", Soy.make(self));
			params.put("entries", Soy.make(entries));
			params.put("members", Soy.make(members));
			params.put("deadline_mils", Soy.make(deadline_mils));

			return new ModelAndView("tm.student.submit", params);
		}

	}

	/**
	 * app/s/results?evid=x&csid=x&s=x
	 * 
	 * @return
	 */
	public View results() {
		long evid = this.getRequest().getParamLong("evid");
		long csid = this.getRequest().getParamLong("csid");
		String hash = this.getRequest().getParam("s");
		Evaluation ev = Evaluation.get(evid);
		CourseStudent cs = CourseStudent.get(csid);

		if (ev == null || cs == null) {
			return new ErrorView("Invalid link");
		}

		Submit submit = Submit.byEvalAndCourseStudent(evid, csid);

		if (!submit.getSecret().equals(hash)) {
			return new ErrorView("Invalid link");
		}

		// Permission check
		if (ev.getStatus() != Evaluation.PUBLISHED) {
			return new ErrorView("Evaluation hasn't been published");
		}

		List<SubmitEntry> entries = SubmitEntry.listStudentResultsEntries(evid, csid);

		EvaluationR evr = new EvaluationR(ev);

		cs.loadTeam();

		ViewMap params = ViewMap.of(
			"entries", Soy.make(entries),
			"cs", Soy.make(cs),
			"ev", Soy.make(evr));

		return new ModelAndView("tm.student.results", params);

	}

}
