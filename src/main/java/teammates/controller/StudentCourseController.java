package teammates.controller;

import java.util.List;

import teammates.dev.RequestSession;
import teammates.mvc.Controller;
import teammates.mvc.view.JsonView;
import teammates.mvc.view.ModelAndView;
import teammates.mvc.view.View;
import teammates.pojo.Coordinator;
import teammates.pojo.Course;
import teammates.pojo.CourseStudent;
import teammates.pojo.Student;
import teammates.template.MasterTemplate;
import teammates.template.Soy;
import teammates.template.ViewMap;

public class StudentCourseController extends Controller {

	@Override
	public void beforeHook() {
		requireLogin();
		setMasterTemplate(MasterTemplate.STUDENT);
	}

	public View index() {
		return list();
	}

	public View list() {
		Student student = RequestSession.instance().getStudent();

		List<CourseStudent> cstudents = CourseStudent.listByStudent(student.getId());

		ViewMap params = ViewMap.of("cstudents", Soy.make(cstudents));

		if (this.getRequest().isGet() && this.getRequest().isAjax()) {
			return new JsonView(params);
		} else {
			return new ModelAndView("tm.student.course_index", params);
		}
	}

	// public View join() {
	// requireLogin();
	// Student student = RequestSession.instance().getStudent();
	//
	// String regkey = this.getRequest().getParam("regkey");
	// CourseStudent cs = CourseStudent.byRegKey(regkey);
	//
	// if (cs == null) {
	// return new ErrorJsonView("Invalid registration key");
	// }
	//
	// if (cs.getStudentKey() != null) {
	// return new ErrorJsonView("You are already registered in the course");
	// }
	//
	// cs.setStudentKey(new Key<Student>(Student.class, student.getId()));
	// //cs.setJoined(true);
	// DAO.db().put(cs);
	//
	// return new SuccessJsonView("Joined course successfully.");
	// }

	public View view() {
		// Student student = RequestScoped.instance().getStudent();

		long csid = this.getRequest().getParamLong("csid");
		CourseStudent cs = CourseStudent.get(csid);
		Course course = Course.get(cs.getCourseId());
		Coordinator coord = Coordinator.get(course.getCoordinatorId());
		List<CourseStudent> members = CourseStudent.listByTeam(cs.getTeamId());

		ViewMap params = new ViewMap();
		params.put("cs", Soy.make(cs));
		params.put("coord_name", coord.getAccount().getEmail()); // For now use
																															// account email
		params.put("members", Soy.make(members));

		return new ModelAndView("tm.student.course_view", params);

	}
}
