package teammates.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import teammates.Lang;
import teammates.dev.DAO;
import teammates.dev.RequestSession;
import teammates.dev.Util;
import teammates.mvc.Controller;
import teammates.mvc.view.ErrorJsonView;
import teammates.mvc.view.ErrorView;
import teammates.mvc.view.JsonView;
import teammates.mvc.view.ModelAndView;
import teammates.mvc.view.RedirectView;
import teammates.mvc.view.SuccessJsonView;
import teammates.mvc.view.View;
import teammates.pojo.Coordinator;
import teammates.pojo.Course;
import teammates.pojo.CourseTeam;
import teammates.pojo.Evaluation;
import teammates.pojo.EvaluationR;
import teammates.pojo.Submit;
import teammates.pojo.SubmitEntry;
import teammates.template.MasterTemplate;
import teammates.template.Soy;
import teammates.template.ViewMap;

import com.googlecode.objectify.Key;

public class CoordEvaluationController extends Controller {

	/**
	 * Run before any of these calls below are made
	 */
	@Override
	public void beforeHook() {
		requireLogin();
		requireCoordinator();
		setMasterTemplate(MasterTemplate.COORDINATOR);
	}

	public View index() {
		return list();
	}

	public View view() {
		long evId = this.getRequest().getParamLong("evid");
		return new RedirectView("/app/c/eval?evid=" + evId);
	}

	public View list() {
		Coordinator coord = RequestSession.instance().getCoordinator();
		List<EvaluationR> evals = EvaluationR.fromList(Evaluation.listByCoordinator(coord));

		ViewMap vm = ViewMap.of(
			"evals", Soy.make(evals)
			);

		if (this.getRequest().isGet() && this.getRequest().isAjax()) {
			return new JsonView(vm);
		} else {
			return new ModelAndView("tm.coordinator.eval_index", vm);
		}
	}

	public View edit() {
		RequestSession.instance().setPageTitle("Edit Evaluation");
		long evId = this.getRequest().getParamLong("eid");
		Evaluation ev = Evaluation.get(evId);
		if (ev == null) {
			return new ErrorView("No evaluation found.");
		}

		EvaluationR evr = new EvaluationR(ev);

		if (this.getRequest().isGet()) {
			// Retrieve the object and render
			return new ModelAndView("tm.coordinator.eval_edit", ViewMap.of(
				"ev", Soy.make(evr)
				));

		} else if (this.getRequest().isPost()) {
			if (!this.getRequest().validateRequired("start_date", "start_time",
				"deadline_date", "deadline_time", "graceperiod")) {
				return new ErrorView("Enter all required fields");
			}

			ev.setInstructions(this.getRequest().getParam("instr"));

			String startDate = this.getRequest().getParam("start_date");
			Integer startTime = Integer.parseInt(this.getRequest().getParameter(
				"start_time"));
			Date start = Util.convertToDate(startDate, startTime);
			ev.setStart(start);

			String deadlineDate = this.getRequest().getParameter("deadline_date");
			Integer deadlineTime = this.getRequest().getParamInt("deadline_time");
			Date deadline = Util.convertToDate(deadlineDate, deadlineTime);
			ev.setDeadline(deadline);

			ev.setGracePeriod(Integer.parseInt(this.getRequest().getParameter(
				"graceperiod")));

			DAO.db().put(ev);

			return new RedirectView("/app/coordinator/evaluation");
		}

		return null;

	}

	/**
	 * This is AJAX-posted
	 */
	public View create() {
		Coordinator coord = RequestSession.instance().getCoordinator();

		long courseId = this.getRequest().getParamLong("courseid");
		if (courseId == -1) {
			return new ErrorView("Course is required.");
		}

		if (this.getRequest().isPost()) {
			Evaluation ev = new Evaluation();

			Course course = Course.get(courseId);
			if (course == null || coord.getId() != course.getCoordinatorId()) {
				// Defensive check
				// return new ErrorJsonView(Lang.COURSE_NO_PERMISSION);
				setFlash("error", Lang.COURSE_NO_PERMISSION);
			} else if (!this.getRequest().validateRequired(
				"evaluationname", "instr", "start_date", "start_time",
				"deadline_date", "deadline_time", "graceperiod")) {

				setFlash("error", "Enter all required fields");
			} else {

				ev.setCourseKey(new Key<Course>(Course.class, courseId));
				ev.setCoordinatorKey(new Key<Coordinator>(Coordinator.class,
					coord.getId()));

				// Collecting POST data

				ev.setName(this.getRequest().getParam("evaluationname"));

				ev.setInstructions(this.getRequest().getParam("instr"));

				String startDate = this.getRequest().getParam("start_date");
				Integer startTime = Integer.parseInt(this.getRequest().getParameter(
					"start_time"));

				Date start = Util.convertToDate(startDate, startTime);
				ev.setStart(start);

				String deadlineDate = this.getRequest().getParameter("deadline_date");

				Integer deadlineTime = this.getRequest().getParamInt("deadline_time");
				Date deadline = Util.convertToDate(deadlineDate, deadlineTime);
				ev.setDeadline(deadline);

				// ev.setTimeZone(Float.parseFloat(this.getRequest().getParameter("timezone")));

				ev.setGracePeriod(Integer.parseInt(this.getRequest().getParameter(
					"graceperiod")));

				Evaluation.create(ev);

				setFlash("success", Lang.MSG_EVALUATION_ADDED);
				return new RedirectView("/app/coordinator/evaluation/view?evid=" + ev.getId());
			}
		}

		Course course = Course.get(courseId);
		ViewMap vm = ViewMap.of(
			"course", Soy.make(course)
			);

		return new ModelAndView("tm.coordinator.eval_create", vm);
	}

	public View results() {
		requireJs("/js/coord_eval_results.js");

		long evId = this.getRequest().getParamLong("eid");
		Evaluation ev = Evaluation.get(evId);
		if (ev == null) {
			return new ErrorView("No evaluation found.");
		}
		EvaluationR evr = new EvaluationR(ev);

		// Summary
		List<Submit> submits = Submit.listByEvaluation(ev.getId());

		// Details
		List<CourseTeam> teams = CourseTeam.listByCourse(ev.getCourseKey());

		List<Key<Submit>> submitKeys = new ArrayList<Key<Submit>>();
		for (Submit s : submits) {
			submitKeys.add(Key.create(Submit.class, s.getId()));
		}
		List<SubmitEntry> entries = DAO.db().query(SubmitEntry.class).filter(
			"submit in", submitKeys).list();

		return new ModelAndView("tm.coordinator.eval_results", ViewMap.of(
			"ev", Soy.make(evr),
			"submits", Soy.make(submits),
			"teams", Soy.make(teams),
			"entries", Soy.make(entries)
			));
	}

	public View open() {
		long evid = this.getRequest().getParamLong("evid");

		Evaluation ev = Evaluation.get(evid);
		if (ev.open()) {
			return new SuccessJsonView("Evaluation has been opened!");
		} else {
			return new ErrorJsonView("Error opening evaluation.");
		}
	}

	/**
	 * Publish the evaluation
	 * 
	 * @return
	 */
	public View ajax_publish() {
		long evid = this.getRequest().getParamLong("evid");
		Evaluation ev = Evaluation.get(evid);
		if (ev == null) {
			return new ErrorJsonView("Evaluation not found.");
		}
		// TODO: Check permission

		ev.publish();

		return new SuccessJsonView("Publish successfully.");

	}

	/**
	 * Publish the evaluation
	 * 
	 * @return
	 */
	public View ajax_unpublish() {
		long evid = this.getRequest().getParamLong("evid");
		Evaluation ev = Evaluation.get(evid);
		if (ev == null) {
			return new ErrorJsonView("Evaluation not found.");
		}
		// TODO: Check permission

		ev.unpublish();

		return new SuccessJsonView("Unpublish successfully.");

	}
}