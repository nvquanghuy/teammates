package teammates.pojo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import javax.persistence.PrePersist;

import teammates.dev.DAO;
import teammates.exception.ValidationError;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Unindexed;

@Cached
@Unindexed
public class Course extends DatastoreObject {
	String code;
	String term = "";
	String name;
	@Indexed
	Key<Coordinator> coordinator;
	@Indexed
	Date timeCreated = null;

	@PrePersist
	private void prepersist() {
		// Create for the first time
		if (timeCreated == null) {
			timeCreated = new Date();
		}
		// Data validation
		if (name.isEmpty() || term.isEmpty() || code.isEmpty() || coordinator == null) {
			throw new ValidationError("Fields are required.");
		}
	}

	public Date getTimeCreated() {
		return timeCreated;
	}

	public String getTimeCreatedF() {
		if (timeCreated != null) {
			return new SimpleDateFormat("MMM yyyy").format(timeCreated);
		}
		return "";
	}

	public Course() {
	}

	public Course(Coordinator coord) {
		this.coordinator = new Key<Coordinator>(Coordinator.class, coord.id);
	}

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public String getTerm() {
		return term;
	}

	public void setTerm(String v) {
		term = v;
	}

	public int getNumTeams() {
		return DAO.db().query(CourseTeam.class)
			.filter("course", new Key<Course>(Course.class, this.id))
			.count();
	}

	/**
	 * Get number of students who has yet to join the course
	 * 
	 * @return
	 */
	public int getUnregistered() {
		return DAO.db().query(CourseStudent.class).filter("course",
			new Key<Course>(Course.class, this.id)).filter("joined", false).count();
	}

	public int getNumStudents() {
		return DAO.db().query(CourseStudent.class).filter("course",
			new Key<Course>(Course.class, this.id)).count();
	}

	public long getCoordinatorId() {
		return coordinator.getId();
	}

	/**
	 * Get the course by specific id, return null if not found
	 * 
	 * @param id
	 * @return
	 */
	public static Course get(long id) {
		return DAO.db().find(Course.class, id);
	}

	public static Course create(Course course) {
		DAO.db().put(course);
		return course;
	}

	public static List<Course> listByCoordinator(Coordinator coord) {
		Objectify db = DAO.db();

		return db.query(Course.class)
			.filter("coordinator", Key.create(Coordinator.class, coord.id))
			.order("-id")
			.list();
	}

	/**
	 * Delete the course and all of its related data.
	 * 
	 * @param courseId
	 * @return true on successful, false otherwise
	 */
	public static boolean delete(long courseId) {
		// Delete
		Key<Course> courseKey = new Key<Course>(Course.class, courseId);
		Objectify db = DAO.db();

		Iterable<Key<CourseTeam>> teamKeys = db.query(CourseTeam.class).filter(
			"course", courseKey).fetchKeys();
		Iterable<Key<CourseStudent>> csKeys = db.query(CourseStudent.class).filter(
			"course", courseKey).fetchKeys();

		Iterable<Key<Evaluation>> eKeys = db.query(Evaluation.class).filter(
			"course", courseKey).fetchKeys();

		// Delete CourseTeam objects
		db.delete(teamKeys);

		// Delete CourseStudent objects
		db.delete(csKeys);

		// Delete Evaluation
		db.delete(eKeys);
		// for (long evid : eKeys)

		// Delete the course object itself
		db.delete(courseKey);

		return true;
	}

	/**
	 * Enroll list of students, if students are there we just modify their
	 * information
	 * 
	 * @param input
	 */
	public void enrollStudents(String input) {
		Objectify db = DAO.db();

		input = input.replaceAll("\r", "");
		String entries[] = input.split("\n");

		Hashtable<String, CourseTeam> teams = CourseTeam.listByCourseWithKeyName(this.getId());
		List<CourseStudent> justAddedStudents = new ArrayList<CourseStudent>();

		for (String entry : entries) {
			entry = entry.replace('|', '\t');
			String fields[] = entry.split("\t");
			String name = fields[1];
			String email = fields[2];
			String teamname = fields[0];

			CourseStudent cs = CourseStudent.fromCourseAndEmail(this.id, email);
			// Never enrol this student before
			if (cs == null) {
				cs = new CourseStudent();
				cs.course = new Key<Course>(Course.class, this.id);
				cs.email = email;

				justAddedStudents.add(cs);
			}
			// Otherwise we have enroled this students before, update their info

			// Do this when student join course.
			// cs.student = new Key<Student>(Student.class, student.id);

			cs.name = name;
			if (fields.length == 4) {
				cs.comments = fields[3];
			}

			// Team
			if (!teams.containsKey(teamname)) {
				CourseTeam team = new CourseTeam(teamname, this.id);

				db.put(team);
				// // Give team an id
				teams.put(teamname, team);

				cs.team = new Key<CourseTeam>(CourseTeam.class, team.id);

			} else {
				CourseTeam team = teams.get(teamname);
				cs.team = new Key<CourseTeam>(CourseTeam.class, team.id);
			}

			db.put(cs);

			// courseStudents.add(cs);
		}

		// Send emails to all students we just added

		// TODO: Clean up courseTeam
		// Since some teams get moved around and eventually will have zero students,
		// we need to delete these teams

		// // Saving to datastore
		// Objectify ofy = ObjectifyService.beginTransaction(); // instead of
		// begin()
		// try {
		// ofy.put(courseStudents);
		// ofy.put(teams);
		//
		// ofy.getTxn().commit();
		// } finally {
		// if (ofy.getTxn().isActive())
		// ofy.getTxn().rollback();
		// }
	}

	/**
	 */
	public void sendRegistrationKeys() {

	}

	// /**
	// * Send registration keys to all course_students that hasn't joined
	// */
	// public void sendKeysToUnjoined() {
	//
	// List<CourseStudent> cstudents = CourseStudent.listFromCourse(this.id);
	//
	// for (CourseStudent cs : cstudents) {
	// if (!cs.joined) {
	// Emails.remindStudent(cs);
	// }
	// }
	// }

	public void setCode(String code2) {
		code = code2;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDisplay() {
		return code + " - " + name;
	}

	public void setCoordinatorId(int coordId) {
		// TODO Auto-generated method stub
		
	}

}
