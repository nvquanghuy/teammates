package teammates.pojo;

import java.util.List;

import javax.persistence.Id;
import javax.persistence.PrePersist;

import teammates.dev.DAO;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.annotation.Cached;

@Cached
public class Coordinator {

	// Same as id in the corresponding Account object
	@Id
	Long id;
	Key<Account> account;

	Coordinator() {
	}
	
	@PrePersist
	private void prepersist() {
	}

	public Coordinator(Key<Account> account) {
		this.account = account;
	}

	public Account getAccount() {
		return service().get(account);
	}

	public long getAccountId() {
		return account.getId();
	}

	private static Objectify service() {
		return ObjectifyService.begin();
	}

	public static Coordinator fromAccount(Account account) {
		List<Coordinator> l = service().query(Coordinator.class).filter("account",
			new Key<Account>(Account.class, account.getId())).list();

		if (l.size() > 0) {
			return l.get(0);
		}

		return null;
	}

	public long getId() {
		return id;
	}

	public static Coordinator get(long coordinatorId) {
		return DAO.db().find(
			new Key<Coordinator>(Coordinator.class, coordinatorId));
	}

}
