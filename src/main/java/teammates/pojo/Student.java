package teammates.pojo;

import teammates.dev.DAO;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;

@Cached
public class Student extends DatastoreObject {

	public Key<Account> account;

	public Student() {
	}

	/**
	 * @return null if not found
	 */
	public static Student fromAccount(long accountId) {
		return DAO.db().query(Student.class)
			.filter("account", new Key<Account>(Account.class, accountId))
			.limit(1).get();
	}

	public static Student get(long id) {
		return DAO.db().find(new Key<Student>(Student.class, id));
	}

}
