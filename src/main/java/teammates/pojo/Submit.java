package teammates.pojo;

import java.util.Date;
import java.util.List;

import javax.persistence.PrePersist;

import teammates.dev.DAO;
import teammates.dev.Util;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.NotSaved;
import com.googlecode.objectify.annotation.Unindexed;

@Cached
public class Submit extends DatastoreObject {

	Key<Evaluation> evaluation;
	Key<CourseStudent> cstudent;
	Key<Course> course; // Denormalized
//	@Unindexed
	String secret = "";
	boolean submitted = false;
	Date timeOpened = null;
	Date timeSubmited = null;

	// Load on demand
	@NotSaved
	CourseStudent d_cstudent = null;
	@NotSaved
	List<SubmitEntry> d_entries = null;

	@PrePersist
	private void prePersist() {
		if (course == null) {
			course = Evaluation.get(evaluation.getId()).course;
			// throw new ConstraintException("Course must not be null");
		}
		if (secret.isEmpty()) {
			secret = Util.randomString(20);
		}
	}

	/**
	 * Create new if not created before
	 * 
	 * @return
	 */
	public static Submit byEvalAndCourseStudent(long evId, long csId) {
		Objectify db = DAO.db();
		Submit submit = db.query(Submit.class).filter("evaluation",
			new Key<Evaluation>(Evaluation.class, evId)).filter("cstudent",
			new Key<CourseStudent>(CourseStudent.class, csId)).limit(1).get();

		// If this is the first time, create the submit and respective SubmitEntries
		if (submit == null) {
			System.out.println("Creating submit object...");
			submit = new Submit();
			submit.evaluation = new Key<Evaluation>(Evaluation.class, evId);
			submit.cstudent = new Key<CourseStudent>(CourseStudent.class, csId);
			db.put(submit);
		}

		// // Create all the SubmitEntries if not already done so
		// CourseStudent fromCs = CourseStudent.get(csId);
		// List<CourseStudent> cstudents =
		// CourseStudent.listByTeam(fromCs.getTeamId());
		//
		// boolean changed = false; // if need to persist data
		// Map<Long, SubmitEntry> entries =
		// SubmitEntry.bySubmitMapToCourseStudent(submit.id);
		// for (CourseStudent toCs : cstudents) {
		// if (!entries.containsKey(toCs.id)) {
		// SubmitEntry enew = new SubmitEntry();
		// enew.fromStudent = new Key<CourseStudent>(CourseStudent.class,
		// fromCs.id);
		// enew.toStudent = new Key<CourseStudent>(CourseStudent.class, toCs.id);
		// enew.submit = new Key<Submit>(Submit.class, submit.id);
		// entries.put(toCs.id, enew);
		// changed = true;
		// }
		// }
		// if (changed) {
		// db.put(entries.values());
		// }

		return submit;

	}
	
	public Date getTimeSubmitted() {
		return timeSubmited;
	}

	public boolean getSubmitted() {
		return submitted;
	}

	public CourseStudent getCourseStudent() {
		return d_cstudent;
	}

	public CourseStudent loadCourseStudent() {
		if (d_cstudent == null)
			d_cstudent = DAO.db().find(cstudent);
		return d_cstudent;
	}

	public void justSubmitted() {
		submitted = true;
		timeSubmited = new Date();
	}

	public static List<Submit> listByEvaluation(long evId) {
		return DAO.db().query(Submit.class).filter("evaluation",
			Key.create(Evaluation.class, evId)).list();
	}

	public String getSecret() {
		return secret;
	}

	public static Submit get(long submitId) {
		return DAO.db().find(Key.create(Submit.class, submitId));
	}

	public List<SubmitEntry> loadEntries() {
		if (d_entries == null)
			d_entries = SubmitEntry.bySubmit(this.id);
		return d_entries;
	}
	
	public List<SubmitEntry> getEntries() {
		return d_entries;
	}

	public Key<CourseStudent> getKeyCourseStudent() {
		return cstudent; 
	}

}
