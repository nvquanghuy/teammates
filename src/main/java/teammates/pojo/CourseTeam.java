package teammates.pojo;

import java.util.Hashtable;
import java.util.List;

import javax.persistence.PrePersist;

import teammates.dev.DAO;
import teammates.exception.ValidationError;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.Query;
import com.googlecode.objectify.annotation.Cached;

@Cached
public class CourseTeam extends DatastoreObject {
	String name;
	Key<Course> course;

	CourseTeam() {
	}

	@PrePersist
	private void prepersist() {
		if (course == null || name.isEmpty()) {
			throw new ValidationError("Cannot persist CourseTeam. Missing information");
		}
	}

	public CourseTeam(String name, long courseId) {
		this.name = name;
		this.course = new Key<Course>(Course.class, courseId);
	}

	public String getName() {
		return name;
	}

	public static CourseTeam foundOrCreateByNameAndCourse(String name, long courseId) {
		Query<CourseTeam> q = DAO.db().query(CourseTeam.class)
			.filter("name", name)
			.filter("course", Key.create(Course.class, courseId));

		if (q.count() == 1) {
			return q.get();
		} else if (q.count() == 0) {
			// Create new Courseteam
			CourseTeam ct = new CourseTeam(name, courseId);
			DAO.db().put(ct);
			return ct;
		}
		else {
			// TODO: Weird thing happens, log error!
			return null;
		}
	}

	/**
	 * Number of students in this team
	 * 
	 * @return
	 */
	public int getNumStudents() {
		return DAO.db().query(CourseStudent.class).filter("team",
			new Key<CourseTeam>(CourseTeam.class, this.id)).count();
	}

	public long getCourseId() {
		return course.getId();
	}

	/**
	 * Return a hashmap <name, object> by courseid
	 * 
	 * @param id2
	 * @return
	 */
	public static Hashtable<String, CourseTeam> listByCourseWithKeyName(long courseId) {

		List<CourseTeam> teams = DAO.db().query(CourseTeam.class).filter(
			"course", new Key<Course>(Course.class, courseId)).list();

		Hashtable<String, CourseTeam> ans = new Hashtable<String, CourseTeam>();
		for (CourseTeam team : teams) {
			ans.put(team.name, team);
		}

		return ans;
	}

	public static List<CourseTeam> listByCourse(Key<Course> key) {
		return DAO.db().query(CourseTeam.class)
			.filter("course", key)
			.list();
	}

	public static List<CourseTeam> listByCourse(long courseId) {
		return listByCourse(new Key<Course>(Course.class, courseId));
	}

	/**
	 * Remove teams with zero
	 * 
	 * @param courseId
	 */
	public static void cleanupTeams(long courseId) {
		Objectify db = DAO.db();

		List<CourseTeam> teams = listByCourse(courseId);

		for (CourseTeam team : teams) {
			if (team.getNumStudents() == 0) {
				db.delete(team);
			}
		}
	}
}
