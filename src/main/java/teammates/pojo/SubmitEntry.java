package teammates.pojo;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.persistence.PrePersist;

import teammates.dev.DAO;
import teammates.exception.ValidationError;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.annotation.Unindexed;

public class SubmitEntry extends DatastoreObject {

	Key<Submit> submit;
	Key<CourseStudent> fromStudent;
	Key<CourseStudent> toStudent;
	Key<Evaluation> evaluation; // Denormalized
	String fromStudentName; // Denormalized
	String toStudentName; // Denormalized

	@Unindexed
	String content = "";

	@PrePersist
	private void prepersist() {
		// Validadtion check before persisting
		if (submit == null) {
			throw new ValidationError("Key submit is not set");
		}
		if (evaluation == null)
			throw new ValidationError("Key evaluation is not set");

		if (fromStudent == null)
			throw new ValidationError("Key fromStudent is not set");

		if (toStudent == null)
			throw new ValidationError("Key toStudent is not set");

	}

	public String getContent() {
		return content;
	}

	public String getToStudentName() {
		return toStudentName;
	}
	

	public static List<SubmitEntry> bySubmit(long submitId) {
		return DAO.db().query(SubmitEntry.class).filter("submit",
			new Key<Submit>(Submit.class, submitId)).list();
	}

	public static Map<Long, SubmitEntry>
		bySubmitMapToCourseStudent(Long submitId) {
		List<SubmitEntry> entries = bySubmit(submitId);
		Map<Long, SubmitEntry> ans = new HashMap<Long, SubmitEntry>();
		for (SubmitEntry entry : entries) {
			ans.put(entry.toStudent.getId(), entry);
		}
		return ans;
	}

	public long getToStudentId() {
		return toStudent.getId();
	}

	public void setContent(String content2) {
		content = content2;
	}

	public long getSubmitId() {
		return submit.getId();
	}

	public String getFromStudentName() {
		return fromStudentName;
	}

	/**
	 * Retrieve entries to display to individual student (all feedback given to
	 * him)
	 * 
	 * @param evid
	 * @param csId
	 * @return
	 */
	public static List<SubmitEntry> listStudentResultsEntries(long evid, long csId) {
		Objectify db = DAO.db();

		List<SubmitEntry> entries = db.query(SubmitEntry.class)
			.filter("evaluation", Key.create(Evaluation.class, evid))
			.filter("toStudent", Key.create(CourseStudent.class, csId))
			.list();

		// Remove entry he writes to himself
		for (SubmitEntry e : entries) {
			if (e.fromStudent.getId() == e.toStudent.getId()) {
				entries.remove(e);
				break;
			}
		}

		// Randomize the orders in a consistent manner. 
		// To do so we fix the seed to evid
		Collections.shuffle(entries, new Random(evid));

		return entries;
	}

}
