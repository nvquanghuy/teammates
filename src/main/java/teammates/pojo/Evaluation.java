package teammates.pojo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

import javax.persistence.PrePersist;

import teammates.dev.DAO;
import teammates.dev.Emails;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.NotSaved;

@Cached
public class Evaluation extends DatastoreObject {

	public static final int AWAITING = 0;
	public static final int OPEN = 1;
	public static final int CLOSED = 2;
	public static final int PUBLISHED = 3;

	Key<Coordinator> coordinator;
	Key<Course> course;

	String name;
	String instructions;
	Date deadline;
	Date start;
	int gracePeriod;

	@NotSaved
	Course d_course = null;

	float timeZone = 8.0f;

	/**
	 * 0: Awaiting 1: Open 2: Closed 3: Published
	 */
	int status = AWAITING;

	@PrePersist
	private void prePersist() {
		// Temporary
		if (this.start == null) {
			this.start = new Date();
		}
	}

	public Evaluation() {
	}

	public long getCourseId() {
		if (course != null) {
			return course.getId();
		}
		return -1;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	/**
	 * Create the evaluation and dependent data
	 * 
	 */
	public static Evaluation create(Evaluation e) {
		Objectify db = DAO.db();
		ObjectifyFactory dbf = new ObjectifyFactory();
		db.put(e);

		// Create all the submit and submitentry objects
		List<CourseStudent> cstudents = CourseStudent.listFromCourse(e.course.getId());
		List<Submit> submits = new ArrayList<Submit>();
		List<SubmitEntry> entries = new ArrayList<SubmitEntry>();

		// Loop through every pair (i, j) of cstudents. If they are on the same
		// team, create the SubmitEntry for them.
		for (int i = 0; i < cstudents.size(); i++) {
			CourseStudent csFrom = cstudents.get(i);

			// First create Submit object
			Submit submit = new Submit();
			submit.evaluation = Key.create(Evaluation.class, e.id);
			submit.cstudent = Key.create(CourseStudent.class, csFrom.id);
			submit.id = dbf.allocateId(Submit.class);
			submits.add(submit);

			for (int j = 0; j < cstudents.size(); j++) {
				CourseStudent csTo = cstudents.get(j);

				// Only pick pair in the same team
				if (csFrom.team.getId() != csTo.team.getId())
					continue;

				SubmitEntry entry = new SubmitEntry();
				entry.submit = Key.create(Submit.class, submit.id);
				entry.fromStudent = Key.create(CourseStudent.class, csFrom.id);
				entry.fromStudentName = csFrom.name;
				entry.toStudent = Key.create(CourseStudent.class, csTo.id);
				entry.toStudentName = csTo.name;
				entry.evaluation = submit.evaluation;
				entries.add(entry);
			}
		}

		// Batch puts
		db.put(submits);
		db.put(entries);

		return e;
	}

	public static List<Evaluation> listByCourse(long courseId) {
		return DAO.db().query(Evaluation.class).filter("course",
			new Key<Course>(Course.class, courseId)).list();
	}

	public static List<Evaluation> listByCoordinator(Coordinator coord) {
		return DAO.db().query(Evaluation.class)
			.filter("coordinator", new Key<Coordinator>(Coordinator.class, coord.id))
			.list();
	}

	public static Evaluation get(long evId) {
		return DAO.db().find(new Key<Evaluation>(Evaluation.class, evId));
	}

	public float getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(float timeZone) {
		this.timeZone = timeZone;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInstructions() {
		return instructions;
	}

	public void setInstructions(String instructions) {
		this.instructions = instructions;
	}

	public int getGracePeriod() {
		return gracePeriod;
	}

	public void setGracePeriod(int gracePeriod) {
		this.gracePeriod = gracePeriod;
	}

	public Date getDeadline() {
		return deadline;
	}

	public void setDeadline(Date deadline) {
		this.deadline = deadline;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Key<Course> getCourseKey() {
		return course;
	}

	public void setCourseKey(Key<Course> course) {
		this.course = course;
	}

	public void setCoordinatorKey(Key<Coordinator> key) {
		this.coordinator = key;
	}

	public boolean publish() {
		// if (this.status != Evaluation.CLOSED)
		// return false;

		if (this.id == null)
			return false;

		this.status = Evaluation.PUBLISHED;
		DAO.db().put(this);

		// Email all students about this
		List<CourseStudent> cstudents = CourseStudent.listFromCourse(this.course.getId());
		Emails.publishEvaluation(this, cstudents);

		return true;
	}

	public boolean unpublish() {
		if (this.status != Evaluation.PUBLISHED)
			return false;

		if (this.id == null)
			return false;

		this.status = Evaluation.CLOSED;
		DAO.db().put(this);

		return true;
	}

	/**
	 * Go through list of evaluations and see if there's an evaluation needed to
	 * be activated, activate that and return this list
	 * 
	 * @return
	 */
	/*public static List<Evaluation> openEvaluations() {
		// huy: Can be optimized further
		Objectify db = DAO.db();

		List<Evaluation> evals = db.query(Evaluation.class).filter("status",
			Evaluation.AWAITING).list();

		List<Evaluation> justActivated = new ArrayList<Evaluation>();

		// Calendar c1 = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
		// Calendar c2 = Calendar.getInstance(TimeZone.getTimeZone("GMT"));

		for (Evaluation e : evals) {
			Calendar c1 = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
			// Fix the time zone accordingly
			c1.add(Calendar.MILLISECOND, (int) (60 * 60 * 1000 * e.getTimeZone()));

			Calendar c2 = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
			c2.setTime(e.getStart());

			if (c1.after(c2) || c1.equals(c2)) {
				e.open();
				justActivated.add(e);

			}
			// Revert time zone change
			// c1.add(Calendar.MILLISECOND, (int) (-60 * 60 * 1000 *
			// e.getTimeZone()));
		}

		return justActivated;
	}*/

	/**
	 * Open this evaluation
	 */
	public boolean open() {
		if (this.id == null)
			return false;

		this.status = Evaluation.OPEN;
		DAO.db().put(this);

		Course course = Course.get(this.getCourseId());
		List<CourseStudent> cstudents = CourseStudent.listFromCourse(course.getId());

		Emails.informStudentsEvaluation(this, cstudents);

		return true;
	}

	public boolean close() {
		if (this.id == null)
			return false;
		this.status = Evaluation.CLOSED;
		DAO.db().put(this);

		return true;
	}

	/**
	 * Close all OPEN evaluations that pass the <deadline + graceperiod>
	 * 
	 */
	public static List<Evaluation> closeEvaluations() {
		Objectify db = DAO.db();

		List<Evaluation> evals = db.query(Evaluation.class).filter("status",
			Evaluation.OPEN).list();

		List<Evaluation> justClosed = new ArrayList<Evaluation>();

		// Calendar c1 = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
		// Calendar c2 = Calendar.getInstance(TimeZone.getTimeZone("GMT"));

		for (Evaluation e : evals) {
			Calendar current = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
			// Fix the time zone accordingly
			current.add(Calendar.MILLISECOND, (int) (60 * 60 * 1000 * e.getTimeZone()));

			// deadline
			Calendar dl = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
			dl.setTime(e.getDeadline());
			dl.add(Calendar.MINUTE, e.gracePeriod);

			if (current.after(dl) || current.equals(dl)) {
				e.close();
				justClosed.add(e);
			}
		}

		return justClosed;
	}

	/**
	 * Remind un-submitted students about their open evaluation
	 */
	public boolean remind() {
		if (this.id == null)
			return false;

		Objectify db = DAO.db();

		// Get list of students who haven't submitted
		Iterable<Submit> submits = db.query(Submit.class).filter("evaluation",
			Key.create(Evaluation.class, this.id)).filter("submitted", false).fetch();

		List<Key<CourseStudent>> cstudentKeys = new ArrayList<Key<CourseStudent>>();
		for (Iterator<Submit> it = submits.iterator(); it.hasNext();) {
			cstudentKeys.add(it.next().cstudent);
		}

		List<CourseStudent> cstudents = new ArrayList<CourseStudent>(db.get(
			cstudentKeys).values());

		Emails.remindEvaluation(this, cstudents);

		return true;
	}

	public static void delete(long evid) {
		Objectify db = DAO.db();

		Key<Evaluation> evalKey = Key.create(Evaluation.class, evid);

		// Extract list of submit keys
		List<Key<Submit>> submitKeys = db.query(Submit.class).filter("evaluation",
			evalKey).listKeys();

		if (submitKeys.size() > 0) {
			Iterable<Key<SubmitEntry>> entryKeys = db.query(SubmitEntry.class).filter(
				"submit in", submitKeys).fetchKeys();
			db.delete(entryKeys);
			db.delete(submitKeys);
		}
		db.delete(evalKey);
	}

	/**
	 * Number of students who have submitted
	 * 
	 * @return
	 */
	public int getNumSubmitted() {
		Objectify db = DAO.db();
		return db.query(Submit.class).filter("evaluation",
			Key.create(Evaluation.class, id)).filter("submitted", true).count();
	}

	public int getNumStudents() {
		Objectify db = DAO.db();
		return db.query(Submit.class).filter("evaluation",
			Key.create(Evaluation.class, id)).count();
	}

	public Course loadCourse() {
		if (d_course == null) {
			d_course = DAO.db().find(this.course);
		}
		return d_course;
	}

	public Course getCourse() {
		return d_course;
	}

}
