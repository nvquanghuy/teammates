package teammates.pojo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import teammates.dev.DAO;

/**
 * Rendering object for Evaluation
 * 
 */
public class EvaluationR extends Evaluation {

	String startDate;
	int startTime; // 1 to 24, 24 being 23:59
	
	String deadlineDate;
	int deadlineTime;

	public String getStartDate() {
		return startDate;
	}

	public int getStartTime() {
		return startTime;
	}
	

	public String getDeadlineDate() {
		return deadlineDate;
	}

	public int getDeadlineTime() {
		return deadlineTime;
	}

	public EvaluationR(Evaluation ev) {
		this.coordinator = ev.coordinator;
		this.course = ev.course;
		this.deadline = ev.deadline;
		this.gracePeriod = ev.gracePeriod;
		this.id = ev.id;
		this.instructions = ev.instructions;
		this.name = ev.name;
		this.start = ev.start;
		this.timeZone = ev.timeZone;
		this.status = ev.status;

//		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
//		this.startDate = formatter.format(this.start);
//		
//		formatter = new SimpleDateFormat("k");
//		int hour = Integer.parseInt(formatter.format(this.start));
//		int minutes = Integer.parseInt(new SimpleDateFormat("m").format(this.start));
//		if (hour == 23 && minutes != 0) hour++;
//		this.startTime = hour;
	
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		this.deadlineDate = formatter.format(this.deadline);
		
		formatter = new SimpleDateFormat("k");
		int hour = Integer.parseInt(formatter.format(this.deadline));
		int minutes = Integer.parseInt(new SimpleDateFormat("m").format(this.deadline));
		if (hour == 23 && minutes != 0) hour++;
		this.deadlineTime = hour;
		
	}
	
	public String getStartR() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		return formatter.format(this.start);
	}

	public String getDeadlineR() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		return formatter.format(this.deadline);
	}

	public static List<EvaluationR> fromList(List<Evaluation> ls) {
		List<EvaluationR> ans = new ArrayList<EvaluationR>(ls.size());
		
		for (Evaluation e : ls) {
			ans.add(new EvaluationR(e));
		}
		
		return ans;
		
	}
	
	/**
	 * 
	 * @return
	 */
	public String getCourseDisplay() {
		Course c = DAO.db().get(this.course);
		return c.code + " - " + c.name;
	}

}
