package teammates.pojo;

import javax.persistence.PrePersist;

import teammates.dev.DAO;
import teammates.exception.ValidationError;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;

/**
 * Coordinator/Student are not subclasses of Account
 * 
 * 
 */
public class Account extends DatastoreObject {
	boolean isCoordinator = false;

	/**
	 * Use email instead of googleId to support other services later on as well.
	 * In case of Google Auth this is the gmail
	 */
	String email = "";

	Account() {
	}

	@PrePersist
	private void prepersist() {
		if (email.isEmpty()) {
			throw new ValidationError("Account.email must not be empty");
		}
	}

	public Account(String email, boolean isCoordinator) {
		this.email = email;
		this.isCoordinator = isCoordinator;
	}

	public boolean isCoordinator() {
		return this.isCoordinator;
	}

	public String getEmail() {
		return email;
	}

	public static Account findByEmail(String email) {
		Account found = DAO.db().query(Account.class).filter("email", email).limit(
			1).get();
		return found;
	}

	/**
	 * Give an account coordinator.
	 * 
	 * Create the account if not already done so.
	 * 
	 */
	public static void makeCoordinator(String email2) {
		Objectify db = DAO.db();
		Account acc = Account.findByEmail(email2);
		if (acc != null) {
			acc.isCoordinator = true;
		} else {
			// Proceed to create the account
			acc = new Account(email2, true);
		}
		db.put(acc);

		Coordinator c = Coordinator.fromAccount(acc);
		if (c == null) {
			c = new Coordinator();
			c.account = Key.create(Account.class, acc.id);
			db.put(c);
		}

	}

	public static void removeCoordinator(long accountId) {
		Account acc = Account.get(accountId);
		acc.isCoordinator = false;

		// We don't remove the Coordinator instance because it might link to other
		// courses that the person created.

		DAO.db().put(acc);
	}

	public static Account get(long accountId) {
		return DAO.db().find(Key.create(Account.class, accountId));
	}

}
