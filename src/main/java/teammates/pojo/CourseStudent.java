package teammates.pojo;

import java.util.List;

import javax.persistence.PrePersist;

import teammates.dev.DAO;
import teammates.exception.ValidationError;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.NotSaved;

@Cached
public class CourseStudent extends DatastoreObject {

	/**
	 * Could be empty, empty when there's no student linking to the
	 * course_student.
	 */
	Key<Student> student;
	Key<Course> course;
	Key<CourseTeam> team;
	/**
	 * The university-specific ID of this student NUS: matriculation number
	 */
	String studentNo;
	String name;
	String email;

	// load on demand
	@NotSaved
	Course d_course = null;
	@NotSaved
	CourseTeam d_team = null;

	// Same for comments
	public String comments = "";

	@PrePersist
	private void prepersist() {
		if (course == null || team == null) {
			throw new ValidationError("CourseStudent: One of keys are not set.");
		}
		if (name.isEmpty() || email.isEmpty()) {
			throw new ValidationError("CourseStudent: Student info is required");
		}
	}

	public CourseStudent() {
	}

	public void setStudentNo(String no) {
		studentNo = no;
	}

	public String getStudentNo() {
		return studentNo;
	}

	public void setStudentId(long id) {
		this.student = new Key<Student>(Student.class, id);
	}

	public String getComments() {
		return comments;
	}

	public Course loadCourse() {
		d_course = DAO.db().find(course);
		return d_course;
	}

	public Course getCourse() {
		return d_course;
	}

	public String getEmail() {
		return email;
	}

	public String getName() {
		return name;
	}

	public CourseTeam getTeam() {
		return d_team;
	}

	/**
	 * Return a list of CourseStudents from courseId
	 * 
	 * @param courseId
	 * @return
	 */
	public static List<CourseStudent> listFromCourse(long courseId) {
		Objectify objf = DAO.db();

		return objf.query(CourseStudent.class).filter(
			"course", new Key<Course>(Course.class, courseId))
			.list();

	}

	/**
	 * Return an object by courseId and email
	 * 
	 * @param id2
	 * @param email
	 * @return null if not found
	 */
	public static CourseStudent fromCourseAndEmail(Long courseId, String email) {
		return DAO.db().query(CourseStudent.class).filter("course",
			new Key<Course>(Course.class, courseId)).filter("email", email).get();
	}

	public static CourseStudent fromCourseAndStudentId(Long courseId,
		Long studentId) {
		return DAO.db().query(CourseStudent.class).filter("course",
			new Key<Course>(Course.class, courseId)).filter("student",
			new Key<Student>(Student.class, studentId)).get();
	}

	public static CourseStudent get(long csId) {
		return DAO.db().find(new Key<CourseStudent>(CourseStudent.class, csId));
	}

	public static void delete(long csId) {
		DAO.db().delete(new Key<CourseStudent>(CourseStudent.class, csId));
	}

	public static void deleteCourseStudents(long courseId) {
		Objectify db = DAO.db();
		Iterable<Key<CourseStudent>> keys = db.query(CourseStudent.class).filter(
			"course", new Key<Course>(Course.class, courseId)).fetchKeys();
		db.delete(keys);
	}

	public Key<Course> getCourseKey() {
		return this.course;
	}

	public static List<CourseStudent> listByStudent(Long studentId) {
		Objectify db = DAO.db();
		return db.query(CourseStudent.class).filter("student",
			new Key<Student>(Student.class, studentId)).list();
	}

	public static CourseStudent byRegKey(String k) {
		return DAO.db().query(CourseStudent.class).filter("regKey", k).get();
	}

	public Key<Student> getStudentKey() {
		return student;
	}

	public void setStudentKey(Key<Student> key) {
		student = key;
	}

	public static List<CourseStudent> listByTeam(long teamId) {
		return DAO.db().query(CourseStudent.class).filter("team",
			new Key<CourseTeam>(CourseTeam.class, teamId)).list();
	}

	public static CourseStudent byCourseAndStudent(long courseId, long studentId) {
		return DAO.db().query(CourseStudent.class).filter("course",
			new Key<Course>(Course.class, courseId)).filter("student",
			new Key<Student>(Student.class, studentId)).limit(1).get();
	}

	public CourseTeam loadTeam() {
		d_team = DAO.db().find(team);
		return d_team;
	}

	public static CourseStudent get(Key<CourseStudent> keyCourseStudent) {
		return DAO.db().find(keyCourseStudent);
	}

	public long getTeamId() {
		return team.getId();
	}

	public long getCourseId() {
		return course.getId();
	}

	public void setTeamKey(Key<CourseTeam> key) {
		team = key;
	}
	
	public void setCourseKey(Key<Course> key) {
		course = key;
	}

	public void setName(String param) {
		name = param;
	}
	public void setEmail(String e) {
		email = e;
	}
}
