package teammates.template;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

import com.google.gson.Gson;

@SuppressWarnings("serial")
public class ViewMap extends HashMap<String, Object> {

	public static ViewMap of(String k1, Object v1) {
		ViewMap vm = new ViewMap();
		_put(vm, k1, v1);
		return vm;
	}

	private static void _put(ViewMap vm, String k, Object v) {
		if (v instanceof Long) {
			vm.put(k, v.toString());
		} else {
			vm.put(k, v);
		}
	}

	public static ViewMap of(String k1, Object v1, String k2, Object v2) {
		ViewMap vm = new ViewMap();
		_put(vm, k1, v1);
		_put(vm, k2, v2);
		return vm;
	}

	public static ViewMap of(String k1, Object v1, String k2, Object v2,
		String k3, Object v3) {
		ViewMap vm = new ViewMap();
		_put(vm, k1, v1);
		_put(vm, k2, v2);
		_put(vm, k3, v3);
		return vm;
	}

	public static ViewMap of(String k1, Object v1, String k2, Object v2,
		String k3, Object v3, String k4, Object v4) {
		ViewMap vm = new ViewMap();
		_put(vm, k1, v1);
		_put(vm, k2, v2);
		_put(vm, k3, v3);
		_put(vm, k4, v4);
		return vm;
	}

	@Deprecated
	public static <T> ViewMap from(T obj, String... keys) {
		ViewMap vm = new ViewMap();
		for (String prop : keys) {
			Object r = ViewMap.runMethod(obj, prop);
			if (r instanceof Long) {
				vm.put(prop, ((Long) r).toString());
			} else {
				vm.put(prop, r);
			}
		}
		return vm;
	}

	private static boolean methodExists(Object obj, String methodName) {
		try {
			obj.getClass().getMethod(methodName);
			return true;
		} catch (SecurityException e) {
			return false;
		} catch (NoSuchMethodException e) {
			return false;
		}
	}

	/**
	 * @throws NoSuchMethodException
	 *           always
	 * @return
	 */
	private static Object runMethod(Object obj, String prop) {
		String methodName = "get" + Character.toUpperCase(prop.charAt(0))
			+ prop.substring(1);
		if (!methodExists(obj, methodName)) {
			methodName = "is" + Character.toUpperCase(prop.charAt(0))
				+ prop.substring(1);
			if (!methodExists(obj, methodName)) {
				System.err.println("Method " + prop + " not exists.");
				return null;
			}
		}

		try {
			Method method = obj.getClass().getMethod(methodName);
			Object ret = method.invoke(obj);
			return ret;
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		return null;
	}

	public String toJson() {
		Gson gson = new Gson();
		return gson.toJson(this);
	}

}
