package teammates.template;

import java.lang.reflect.InvocationTargetException;	
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.List;

import teammates.pojo.DatastoreObject;

import com.google.template.soy.data.SoyData;
import com.google.template.soy.data.SoyListData;
import com.google.template.soy.data.SoyMapData;
import com.google.template.soy.data.restricted.BooleanData;
import com.google.template.soy.data.restricted.FloatData;
import com.google.template.soy.data.restricted.IntegerData;
import com.google.template.soy.data.restricted.NullData;
import com.google.template.soy.data.restricted.StringData;

public class Soy {

	@SuppressWarnings({ "rawtypes" })
	public static SoyData make(Object obj) {
		if (obj == null) {
			return NullData.INSTANCE;
		} else if (obj instanceof Boolean) {
			return BooleanData.createFromExistingData(obj);
		} else if (obj instanceof Float || obj instanceof Double) {
			return FloatData.createFromExistingData(obj);
		} else if (obj instanceof Integer || obj instanceof Short || obj instanceof Long) {
			Integer ans = Integer.parseInt(obj.toString());
			return IntegerData.createFromExistingData(ans);
		} else if (obj instanceof String) {
			return StringData.createFromExistingData(obj);
		} else if (obj instanceof List) {
			SoyListData ans = new SoyListData();
			List objlist = (List) obj;
			for (int i = 0; i < objlist.size(); i++) {
				ans.add(make(objlist.get(i)));
			}
			return ans;
		} else if (obj instanceof DatastoreObject) {
			
			SoyMapData map = new SoyMapData();

			// Loop through its getters.
			Method[] methods = obj.getClass().getMethods();
			for (Method m : methods) {
				if (isGetter(m)) {
					// getFirstName -> firstName
					String key = m.getName().substring(3, 4).toLowerCase() +
						m.getName().substring(4);
					Object val = null;
					try {
						val = m.invoke(obj);
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						e.printStackTrace();
					}
					//System.out.print(key + " ");
					map.put(key, make(val));
				}
			}
			return map;

			/*
			 * try { Map<String, Object> bean = BeanUtils.describe(obj); for (String k
			 * : bean.keySet()) {
			 * 
			 * if (!k.equals("class")) { System.out.println(k + " " +
			 * bean.get(k).getClass().toString()); map.put(k,
			 * objectToSoyData(bean.get(k))); } } return map; } catch
			 * (IllegalAccessException e) { e.printStackTrace(); } catch
			 * (InvocationTargetException e) { e.printStackTrace(); } catch
			 * (NoSuchMethodException e) { e.printStackTrace(); } catch
			 * (IntrospectionException e) { // TODO Auto-generated catch block
			 * e.printStackTrace(); }
			 */
		} else {
			return StringData.forValue(obj.toString());
		}
	}

	static boolean isGetter(Method m) {
		// We only get public non-static method
		if (Modifier.isStatic(m.getModifiers()))
			return false;
		if (!Modifier.isPublic(m.getModifiers()))
			return false;
		if (!m.getName().startsWith("get"))
			return false;
		if (m.getName().length() <= 3)
			return false;

		if (m.getName().equals("getClass")) {
			return false;
		}

		return true;
	}



}
