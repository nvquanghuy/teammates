package teammates.template;

public class MasterTemplate {
	public static final String COORDINATOR = "master_coordinator";
	public static final String STUDENT = "master_student";
	public static final String ADMIN = "master_admin";
	public static final String DEFAULT = "master_default";
	public static final String NONE = "master_none";
	
}
