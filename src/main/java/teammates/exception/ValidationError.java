package teammates.exception;

public class ValidationError extends RuntimeException {
	public ValidationError(String m) {
		super(
			m);
	}
}
