package teammates.exception;

public class NoPermissionException extends RuntimeException {
	private static final long serialVersionUID = -1797368705024066177L;

	public NoPermissionException(String s) {
		super(
			s);
	}

}
