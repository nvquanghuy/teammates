package teammates.config;

public class Config {
	// TeamMates-related configuration
	public static final String TEAMMATES_APP_EMAIL = "teammates2@gmail.com";
	public static String TEAMMATES_APP_URL = "http://teammateshuy.appspot.com";
	
	public static boolean IS_PRODUCTION = true;
	static {
		// From:
		// http://stackoverflow.com/questions/1015442/how-to-get-the-current-server-url-of-appengine-app
		String env = System.getProperty("com.google.appengine.runtime.environment");
		if (env.equals("Development")) {
			IS_PRODUCTION = false;
			TEAMMATES_APP_URL = "http://localhost:8888";
		}
	}
}