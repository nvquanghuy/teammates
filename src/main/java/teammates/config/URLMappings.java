package teammates.config;

import java.util.HashMap;
import java.util.Map;

import teammates.controller.AccountController;
import teammates.controller.AdminController;
import teammates.controller.CoordinatorController;
import teammates.controller.CronController;
import teammates.controller.HomeController;
import teammates.controller.StudentController;
import teammates.mvc.Controller;

/**
 * Contains mapping from URL to the corresponding Controller class
 * 
 * 
 */
public class URLMappings {
	
	public static Map<String, Class<? extends Controller>> MAP = new HashMap<String, Class<? extends Controller>>();
	
	static {
		MAP.put("/app/admin", AdminController.class);
		MAP.put("/app/account", AccountController.class);
		MAP.put("/app/s", StudentController.class);
		MAP.put("/app/c", CoordinatorController.class);
		MAP.put("/app/home", HomeController.class);
		MAP.put("/app/cron", CronController.class);
	}

}
